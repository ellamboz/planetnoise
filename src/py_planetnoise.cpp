#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <noiseutils.h>
#include <memory>
#include <texturebuilder/texturebuilder.h>
#include <texturebuilder/heightmapdescriptor.h>
#include <texturebuilder/imagedescriptor.h>
#include <texturebuilder/moduledescriptor.h>
#include <texturebuilder/rendererdescriptor.h>
#include <texturebuilder/textureworkflowcreator.h>


namespace py = pybind11;
using namespace pybind11::literals;
using namespace std;



PYBIND11_MODULE(planetnoise,m) {
    m.doc() = R"DOC(
        What is planetnoise?
        --------------------

        **Planetnoise** is a Python module that encapsulates a C++ library I wrote some times ago
        around the wonderful Libnoise_ library to encapsulate its classes and ease world texture building.

        Basically it was born out of the necessity to create JSON representations of world maps that could
        be read and written by any given application. Thus, I made a shell around Libnoise that used in my
        own apps. 

        However, while JSON representation is nice for apps, it is also hard to code for humans without
        expressly made programs; so, I set out to create a VHLL layer around it. I tried Lua, but 
        while it is very easy to embed Lua as a language and export C++ classes to it, it never grew on 
        me. I would have liked to make a python extensions, but I lacked the time and the will to undertake
        this project.

        Enter PyBind_ .

        PyBind makes writing python modules around c++ classes **a breeze**. I wrote the bulk of the 
        extension in around one day, barring some minor corrections and some refactoring to make the library
        *a little* more pythonic. 

        The result is a **fast** python library, natively compiled, that allows you to:

        - Generate worldmap textures, equirectangular projection, using your favorite language ;-) 
        - export and import them in json format
        - all with fast render times (1024x512 less than 1sec on my i7 6th gen running linux, with HD620)
        - You can create and combine multiple textures and render multiple images with a single TextureBuilder
        - Say goodbye to height maps posing as seen-from-space maps. A tutorial will guide you to build plausible maps!
        - You will also learn how to build barren, rocky planets, gas giants and weird, campy worlds.

        .. code-block:: python

            import planetnoise
            from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
            from planetnoise import RendererDescriptor as RD
            from planetnoise import NoiseMapBuilderDescriptor as NMB


            # let's build the perlin noise descriptor
            # seed = 0 means "totally random"
            perlin = MD.Perlin( name="Perlin", seed=101, freq=1.2,lac=1.8, pers=0.6,oct=8)

            # let's build another Perlin Descriptor for sea:
            # take care to use a different name from other modules!
            perlin_sea = MD.Perlin( name="PerlinSea", seed=201, freq=1.2,lac=1.8, pers=0.6,oct=8)

            # Let's also build a Ridged Multifractal descriptor. Its jagged appearance will be useful 
            # to create mountain ranges. 
            rmf_mountains = MD.RidgedMulti( name="Ridged", seed=301, freq=3.9, lac=1.9, oct=8)

            # then the image descriptor, which will hold rendered image data
            # we STILL need one image. 
            image = ID.create("Image")

            # then the heightmap, which will hold heightmap data
            heightmap = HMD.create("Heightmap")

            # we need an heightmap for sea, too. 
            heightmapSea = HMD.create("HeightmapSea")

            #and one for the mountains
            heightmapRange = HMD.create("HeightmapRange")

            # connect the noise module with heightmap
            heightmapBuilder = NMB.create("hmbuilder", module=perlin, heightMap=heightmap)

            # we also need a hm builder for sea. 
            heightmapBuilderSea = NMB.create("hmbuilderSea", module=perlin_sea, heightMap=heightmapSea)

            # ... and one for mountain ranges
            heightmapBuilderRanges = NMB.create("hmbuilderRanges", module=rmf_mountains, heightMap=heightmapRange)


            # this is a simple gradient we'll use to render land areas.
            # we remove ice, since we'll use rmf_mountains ridged multifractal
            # noise as a basis for our mountain ranges. 
            land_gradient = [                
                ( -1.0,255,255,192,255),
                ( 0.21,  0,128,  0,255),
                ( 1.0,100, 50,  0,255)
            ]

            # this is a sea gradient. It's sea blue until 55%, then there's a little sand strip
            # that becomes transparent. We'll use it to generate a blue map with a cutout area that makes for 
            # the continents' profiles. 
            sea_gradient = [
                (-1.0  , 20, 20, 80,255),    
                (-0.1  , 30, 30,128,255),
                ( 0.250  , 40, 40,144,255),
                ( 0.251,255,255,192,255),
                ( 0.410,255,255,192,  0),
                ( 1.0  ,255,255,192,  0),
            ]

            # this is the gradient we're using for ice. Only higest peak will be white
            ice_gradient = [
                (-1.0  ,255,255,255,   0),
                ( 0.4  ,255,255,255,   0),
                ( 0.42 ,255,255,255,  0),
                ( 0.78 ,255,255,255, 48),
                ( 0.85 ,255,255,255, 128),
                ( 0.95 ,255,255,255, 192),
                ( 1.00 ,255,255,255, 225),
            ]

            # let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
            # we're using a "land-only gradient" 
            renderer  = RD.create(name="renderer", 
                heightMap=heightmap,       # name of the heightmap you want to render
                gradientInfo = land_gradient,
                bumpMap = heightmapRange,   # we're using another heightmap as a bumpmap
                destImage=image,
                lightContrast = 1.0,
                enabledLight= True)

            # after the land, snow
            renderer_mnt  = RD.create(name="renderer_mnt", 
                heightMap=heightmapRange,       # name of the heightmap you want to render
                gradientInfo = ice_gradient,
                destImage=image,
                backgroundImage= image,
                lightContrast = 1.4,
                enabledLight= True)


            # then sea covers all: we'll use the sea gradient to create a "sea overlay" on the map. 
            # this will make our world map slightly more natural, since land and sea have their own generator.
            renderer_sea  = RD.create(name="renderer_sea", 
                heightMap=heightmapSea,       # name of the heightmap you want to render
                gradientInfo = sea_gradient,
                destImage=image,
                backgroundImage= image,
                lightContrast = 2.0,
                enabledLight= False)



            # then let's assemble everything together in the TextureBuilder
            texture = planetnoise.TextureBuilder()
            texture.setSize(2400,1200)
            # texture.setBounds(0,10,35,45)

            texture.appendModuleDescriptor([perlin,  perlin_sea, rmf_mountains])
            texture.appendImageDescriptor(image)           # add images to builder
            texture.appendHeightMapDescriptor([heightmap, heightmapSea, heightmapRange])
            texture.appendNoiseMapBuilderDescriptor([heightmapBuilder,heightmapBuilderSea, heightmapBuilderRanges]) # add heightmapbuilder

            # very important, renderers perform their job according to their insertion order. 
            texture.appendRendererDescriptor([renderer, renderer_mnt, renderer_sea])     # finally, lets add the renderer

            jsonTexture = texture.jsonString()
            with open ("Planet_03.texjson","w") as f:
                f.write(jsonTexture)

            texture.outputFileName = "Planet_03"
            texture.buildTextureFromJsonString(texture.jsonString(), ".")

        Enjoy!

        .. _Libnoise: http://libnoise.sourceforge.net/
        .. _PyBind: https://github.com/pybind/pybind11
    )DOC";
    m.attr("moduleList") = NoiseModules::moduleList;

    py::class_<ModuleDescriptor, std::shared_ptr<ModuleDescriptor>>(m, "ModuleDescriptor", R"DOC(
        **ModuleDescriptor** is a descriptor class that encapsulates a libnoise modules
        around a common interface.

        You usually don't create this class directly (even if you can). Instead, you 
        create an instance by invoking one of its **Static Constructor**, like this:

        .. code-block:: python

            perlin = MD.Perlin( name="Perlin", seed=101, freq=1.2,lac=1.8, pers=0.6,oct=8)

        Like all descriptor classes, it must have an unique name: when added to TextureBuilder o
        instance, no ModuleDescriptor can share the same name. 

        )DOC")
        .def_property("name",
            &ModuleDescriptor::name, &ModuleDescriptor::setName, "gets or sets ModuleDescriptor's name")
        .def_property("moduleType",
            &ModuleDescriptor::moduleType, &ModuleDescriptor::setModuleType, "gets or sets ModuleDescriptor type")
        .def_property("seed",
            &ModuleDescriptor::seed, &ModuleDescriptor::setSeed, "gets or sets the seed of random number generator: 0 means the seed is randomized.")
        .def_property("actualSeed",
            &ModuleDescriptor::actualSeed, &ModuleDescriptor::setActualSeed, "gets or sets the seed used in cased **seed** property is zero")
        .def_property("freq",
            &ModuleDescriptor::freq, &ModuleDescriptor::setFreq, "gets or sets the frequency of the module")
        .def_property("lac",
            &ModuleDescriptor::lac, &ModuleDescriptor::setLac, "gets or sets the lacunarity of the module")
        .def_property("pers",
            &ModuleDescriptor::pers, &ModuleDescriptor::setPers, "gets or sets the persistence of the module")
        .def_property("offset",
            &ModuleDescriptor::offset, &ModuleDescriptor::setOffset, "gets or sets the offset of the module")
        .def_property("gain",
            &ModuleDescriptor::gain, &ModuleDescriptor::setGain, "gets or sets the gain of the module")
        .def_property("oct",
            &ModuleDescriptor::oct, &ModuleDescriptor::setOct, "gets or sets the octave of the module")
        .def_property("displ",
            &ModuleDescriptor::displ, &ModuleDescriptor::setDispl, "gets or sets the displacement of the module")
        .def_property("enableDist",
            &ModuleDescriptor::enableDist, &ModuleDescriptor::setEnabledist, "enables or disables displacement on Voronoi")
        .def_property("src1",
            &ModuleDescriptor::src1, &ModuleDescriptor::setSrc1, "gets or sets a source module. ")
        .def_property("src2",
            &ModuleDescriptor::src2, &ModuleDescriptor::setSrc2, "gets or sets a source module. ")
        .def_property("src3",
            &ModuleDescriptor::src3, &ModuleDescriptor::setSrc3, "gets or sets a source module. ")
        .def_property("src4",
            &ModuleDescriptor::src4, &ModuleDescriptor::setSrc4, "gets or sets a source module. ")
        .def_property("ctl",
            &ModuleDescriptor::ctl, &ModuleDescriptor::setCtl, "gets or sets a control module. ")
        .def_property("lbound",
            &ModuleDescriptor::lBound, &ModuleDescriptor::setLbound, "gets or sets the Lower bound. ")
        .def_property("ubound",
            &ModuleDescriptor::uBound, &ModuleDescriptor::setUbound, "gets or sets the Upper bound. ")
        .def_property("exp",
            &ModuleDescriptor::exp, &ModuleDescriptor::setExp, "gets or sets the exponent value. ")
        .def_property("bias",
            &ModuleDescriptor::bias, &ModuleDescriptor::setBias, "gets or sets the bias value ")
        .def_property("scale",
            &ModuleDescriptor::scale, &ModuleDescriptor::setScale, "gets or sets the scale value. ")
        .def_property("ubound",
            &ModuleDescriptor::invert, &ModuleDescriptor::invert, "enables or disable the invertion of module height value")
        .def_property("x",
            &ModuleDescriptor::x, &ModuleDescriptor::setX, "gets or sets X value ")
        .def_property("y",
            &ModuleDescriptor::y, &ModuleDescriptor::setY, "gets or sets Y value ")
        .def_property("z",
            &ModuleDescriptor::z, &ModuleDescriptor::setZ, "gets or sets Z value ")
        .def_property("pow",
            &ModuleDescriptor::pow, &ModuleDescriptor::setPow, "gets or sets pow value")
        .def_property("rough",
            &ModuleDescriptor::rough, &ModuleDescriptor::setRough, "gets or sets roughness value module. ")
        .def_property("value",
            &ModuleDescriptor::value, &ModuleDescriptor::setValue, "gets or sets a value property. ")
        /*
        .def("debugLine", &ModuleDescriptor::debugLine)
        .def("dumpModule", &ModuleDescriptor::dumpModule)
        .def("appendToProperties", &ModuleDescriptor::appendToProperties)
        .def("propertiesToExport", &ModuleDescriptor::propertiesToExport)
        .def("setupPropertiesToExport", &ModuleDescriptor::setupPropertiesToExport)
        .def("propContains", &ModuleDescriptor::propContains)
        .def("moduleContains", &ModuleDescriptor::moduleContains)
        .def("mustConnect", &ModuleDescriptor::mustConnect)
        .def("connectModules", &ModuleDescriptor::connectModules)
        .def_static("CreateBillow", &ModuleDescriptor::CreateBillow)
        .def_static("CreatePerlin", &ModuleDescriptor::CreatePerlin)
        .def_static("CreateRidgedMulti", &ModuleDescriptor::CreateRidgedMulti)
        .def_static("CreateExp", &ModuleDescriptor::CreateExp)
        .def_static("CreateTurbulence2", &ModuleDescriptor::CreateTurbulence2)
        .def_static("CreateTurbulenceBillow", &ModuleDescriptor::CreateTurbulenceBillow)
        .def_static("CreateTurbulenceRidged", &ModuleDescriptor::CreateTurbulenceRidged)
        .def_static("CreateTurbulence", py::overload_cast<std::string, std::string>(&ModuleDescriptor::CreateTurbulence))
        .def_static("CreateTurbulence", py::overload_cast<std::string, std::string, double, double, double>(&ModuleDescriptor::CreateTurbulence))
        */
        .def_static("Abs", &ModuleDescriptor::newAbs,"name"_a, "src1"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Abs** Module.

            The **Abs** module outputs the absolute value of the output value from a source module.

            Parameters
            ----------
        )DOC")
        .def_static("Add", &ModuleDescriptor::newAdd,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Add** Modulel.
            
            The **Add** module outputs addition of two source modules. 

            Parameters
            ----------
        )DOC")
        .def_static("Avg", &ModuleDescriptor::newAvg,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Avg** Module.
            
            The **Avg** module outputs the average value of the output of two source modules.

            Parameters
            ----------
        )DOC")
        .def_static("Avg4", &ModuleDescriptor::newAvg4,"name"_a, "src1"_a, "src2"_a, "src3"_a, "src4"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Avg4** Module.
            
            The **Avg4** module outputs the average value of the output of four source modules.

            Parameters
            ----------
        )DOC")
        .def_static("Billow", &ModuleDescriptor::newBillow, "name"_a,"seed"_a=0,  "freq"_a=1.0 , "lac"_a=2.0 , "pers"_a=0.5 ,  "oct"_a=8  ,  "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Billow** Module.

            The **Billow** module is similar to Perlin; however, its output is more suited to render
            rocks or clouds. 
            
            Parameters
            ----------
        )DOC")
        .def_static("Blend", &ModuleDescriptor::newBlend,"name"_a, "src1"_a, "src2"_a, "ctl"_a,"enableRandom"_a = false)
        .def_static("Clamp", &ModuleDescriptor::newClamp, "name"_a, "src1"_a, "uBound"_a,"lBound"_a, "enableRandom"_a = false)
        .def_static("Const", &ModuleDescriptor::newConst, "name"_a, "value"_a, "enableRandom"_a=false)
        .def_static("Cache", &ModuleDescriptor::newCache, "name"_a, "src1"_a)
        .def_static("Cos", &ModuleDescriptor::newCos, "name"_a, "src1"_a, "freq"_a, "exp"_a, "value"_a, "enableRandom"_a=false)
        .def_static("Curve", &ModuleDescriptor::newCurve,"name"_a, "src1"_a, "cPoints"_a, "enableRandom"_a=false)
        .def_static("Curve", &ModuleDescriptor::newCurve,"name"_a, "src1"_a, "cpoints"_a, "enableRandom"_a=false)
        .def_static("Cylinder", &ModuleDescriptor::newCylinders, "name"_a, "freq"_a, "enableRandom"_a=false)
        .def_static("Cylinders", &ModuleDescriptor::newCylinders, "name"_a, "freq"_a, "enableRandom"_a=false)
        .def_static("Displace", &ModuleDescriptor::newDisplace,"name"_a, "src1"_a, "src2"_a, "src3"_a, "src4"_a, "ctl"_a, "enableRandom"_a=false)
        .def_static("Exponent", &ModuleDescriptor::newExponent,  "name"_a, "src1"_a, "exp"_a=1.0,  "enableRandom"_a=false)
        .def_static("Exp", &ModuleDescriptor::newExponent,  "name"_a, "src1"_a, "exp"_a=1.0,  "enableRandom"_a=false)
        .def_static("Invert", &ModuleDescriptor::newInvert,"name"_a, "src1"_a, "enableRandom"_a = false)
        .def_static("Max", &ModuleDescriptor::newMax,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("Min", &ModuleDescriptor::newMin,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("Multiply", &ModuleDescriptor::newMultiply,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("Perlin", &ModuleDescriptor::newPerlin, "name"_a,"seed"_a=0,  "freq"_a =4.58, "lac"_a =1.25, "pers"_a =0.31,  "oct"_a = 8 ,  "enableRandom"_a = false)
        .def_static("Power", &ModuleDescriptor::newPower,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("RidgedMulti", &ModuleDescriptor::newRidgedMulti, "name"_a,"seed"_a=0,  "freq"_a=1.0 , "lac"_a=2.0 , "oct"_a=7  ,  "enableRandom"_a = false)
        .def_static("RidgedMulti2", &ModuleDescriptor::newRidgedMulti2, "name"_a,"seed"_a=0,  "freq"_a=1.0 , "lac"_a=2.0 , "gain"_a=1.0 ,  "exp"_a=1.0 ,  "oct"_a=7  , "offset"_a=0, "enableRandom"_a = false)
        .def_static("RotatePoint", &ModuleDescriptor::newRotatePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def_static("ScaleBias", &ModuleDescriptor::newScaleBias , "name"_a, "src1"_a, "scale"_a, "bias"_a, "enableRandom"_a = false)
        .def_static("ScalePoint", &ModuleDescriptor::newScalePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def_static("Select", &ModuleDescriptor::newSelect, "name"_a, "src1"_a, "src2"_a, "ctl"_a, "uBound"_a , "lBound"_a ,"value"_a, "enableRandom"_a = false)
        .def_static("Sin", &ModuleDescriptor::newSin, "name"_a, "src1"_a, "freq"_a, "exp"_a, "value"_a, "enableRandom"_a = false)
        .def_static("Spheres", &ModuleDescriptor::newSpheres, "name"_a, "freq"_a, "enableRandom"_a = false)
        .def_static("Terrace", &ModuleDescriptor::newTerrace, "name"_a, "src1"_a, "cpoints"_a, "invert"_a = false, "enableRandom"_a = false)
        .def_static("TranslatePoint", &ModuleDescriptor::newTranslatePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def_static("Turbulence", &ModuleDescriptor::newTurbulence, "name"_a, "src1"_a, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("Turbulence2", &ModuleDescriptor::newTurbulence2, "name"_a, "src1"_a, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("TurbulenceBillow", &ModuleDescriptor::newTurbulenceBillow, "name"_a, "src1"_a=0, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("TurbulenceRidged", &ModuleDescriptor::newTurbulenceRidged, "name"_a, "src1"_a=0, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("Voronoi", &ModuleDescriptor::newVoronoi, "name"_a, "seed"_a=0, "freq"_a=5.0, "disp"_a=0.0, "enableDispl"_a=false, "enableRandom"_a = false)
        .def_static("Voronoi", &ModuleDescriptor::newVoronoi, "name"_a, "seed"_a=0, "freq"_a=5.0, "disp"_a=0.0, "enableDist"_a=false, "enableRandom"_a = false)
        ////
        .def_static("AbsN", &ModuleDescriptor::newAbs,"name"_a, "src1"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Abs** Module.

            The **Abs** module outputs the absolute value of the output value from a source module.

            Parameters
            ----------
        )DOC")
        .def_static("AddN", &ModuleDescriptor::newAdd,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Add** Modulel.
            
            The **Add** module outputs addition of two source modules. 

            Parameters
            ----------
        )DOC")
        .def_static("AvgN", &ModuleDescriptor::newAvg,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Avg** Module.
            
            The **Avg** module outputs the average value of the output of two source modules.

            Parameters
            ----------
        )DOC")
        .def_static("Avg4N", &ModuleDescriptor::newAvg4,"name"_a, "src1"_a, "src3"_a, "src1"_a, "src4"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Avg4** Module.
            
            The **Avg4** module outputs the average value of the output of four source modules.

            Parameters
            ----------
        )DOC")
        .def_static("BlendN", &ModuleDescriptor::newBlend,"name"_a, "src1"_a, "src2"_a, "ctl"_a,"enableRandom"_a = false)
        .def_static("ClampN", &ModuleDescriptor::newClamp, "name"_a, "src1"_a, "uBound"_a,"lBound"_a, "enableRandom"_a = false)
        .def_static("CosN", &ModuleDescriptor::newCos, "name"_a, "src1"_a, "freq"_a, "exp"_a, "value"_a, "enableRandom"_a=false)
        .def_static("CurveN", &ModuleDescriptor::newCurve,"name"_a, "src1"_a, "cPoints"_a, "enableRandom"_a=false)
        .def_static("DisplaceN", &ModuleDescriptor::newDisplace,"name"_a, "src1"_a, "src2"_a, "src3"_a, "src4"_a, "ctl"_a, "enableRandom"_a=false)
        .def_static("ExponentN", &ModuleDescriptor::newExponent,  "name"_a, "src1"_a, "exp"_a,  "enableRandom"_a=false)
        .def_static("InvertN", &ModuleDescriptor::newInvert,"name"_a, "src1"_a, "enableRandom"_a = false)
        .def_static("MaxN", &ModuleDescriptor::newMax,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("MinN", &ModuleDescriptor::newMin,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("MultiplyN", &ModuleDescriptor::newMultiply,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("PowerN", &ModuleDescriptor::newPower,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def_static("RotatePointN", &ModuleDescriptor::newRotatePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def_static("ScaleBiasN", &ModuleDescriptor::newScaleBias , "name"_a, "src1"_a, "scale"_a, "bias"_a, "enableRandom"_a = false)
        .def_static("ScalePointN", &ModuleDescriptor::newScalePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def_static("SelectN", &ModuleDescriptor::newSelect, "name"_a, "src1"_a, "src2"_a, "ctl"_a, "uBound"_a , "lBound"_a ,"value"_a, "enableRandom"_a = false)
        .def_static("SinN", &ModuleDescriptor::newSin, "name"_a, "src1"_a, "freq"_a, "exp"_a, "value"_a, "enableRandom"_a = false)
        .def_static("SpheresN", &ModuleDescriptor::newSpheres, "name"_a, "freq"_a, "enableRandom"_a = false)
        .def_static("TerraceN", &ModuleDescriptor::newTerrace, "name"_a, "src1"_a, "cpoints"_a, "invert"_a = false, "enableRandom"_a = false)
        .def_static("TranslatePointN", &ModuleDescriptor::newTranslatePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def_static("TurbulenceN", &ModuleDescriptor::newTurbulence, "name"_a, "src1"_a, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("Turbulence2N", &ModuleDescriptor::newTurbulence2, "name"_a, "src1"_a, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("TurbulenceBillowN", &ModuleDescriptor::newTurbulenceBillow, "name"_a, "src1"_a=0, "seed"_a, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def_static("TurbulenceRidgedN", &ModuleDescriptor::newTurbulenceRidged, "name"_a, "src1"_a=0, "seed"_a, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false);

    py::class_<RendererDescriptor, std::shared_ptr<RendererDescriptor>>(m, "RendererDescriptor")
        .def(py::init<>())
        .def_static("create", 
            /*
            (std::shared_ptr<RendererDescriptor> (RendererDescriptor::*)
            (const std::string&, const std::string&, std::vector<GradientInfo>&,
            const std::string&, const std::string&, const std::string&, const std::string&, 
            double, double, bool, bool)
            */
            &RendererDescriptor::create,
            "name"_a, "heightMap"_a,"gradientInfo"_a, "destImage"_a,
            "backgroundImage"_a=nullptr,"alphaImage"_a=nullptr, "bumpMap"_a=nullptr, "lightBrightness"_a=2.0,
            "lightContrast"_a=1.5,"enabledLight"_a=false, "randomGradient"_a=false)
        .def_property("name",
            &RendererDescriptor::name, 
            &RendererDescriptor::setName)
        .def_property("destImage",
            &RendererDescriptor::destImage, 
            &RendererDescriptor::setDestImage)
        .def_property("backgroundImage",
            &RendererDescriptor::backgroundImage, 
            &RendererDescriptor::setBackgroundImage)            
        .def_property("alphaImage",
            &RendererDescriptor::alphaImage, 
            &RendererDescriptor::setAlphaImage)            
        .def_property("heightMap",
            &RendererDescriptor::heightMap, 
            &RendererDescriptor::setHeightmap)            
        .def_property("bumpMap",
            &RendererDescriptor::bumpMap, 
            &RendererDescriptor::setBumpMap)            
        .def_property("enabledLight",
            &RendererDescriptor::enabledLight, 
            &RendererDescriptor::setEnabledlight)            
        .def_property("lightContrast",
            &RendererDescriptor::lightContrast, 
            &RendererDescriptor::setLightcontrast)            
        .def_property("lightBrightness",
            &RendererDescriptor::lightBrightness, 
            &RendererDescriptor::setLightbrightness)            
        .def_property("randomGradient",
            &RendererDescriptor::randomGradient, 
            &RendererDescriptor::setRandomGradient)            
        .def("rndHue",
            &RendererDescriptor::rndHue)            
        .def("rndSaturation",
            &RendererDescriptor::rndSaturation)            
        .def("rndValue",
            &RendererDescriptor::rndValue)
        .def("setRandomFactor", &RendererDescriptor::setRandomFactor)
        .def_property("noiseMaps",
            &RendererDescriptor::noiseMaps, 
            &RendererDescriptor::setNoiseMaps)
        .def_property("images",
            &RendererDescriptor::images, 
            &RendererDescriptor::setImages)
        .def_property("rndValue",
            &RendererDescriptor::renderers, 
            &RendererDescriptor::setRenderers)
        .def("gradientInfo", &RendererDescriptor::gradientInfo)            
        .def("clearGradientInfo", &RendererDescriptor::clearGradientInfo)            
        .def("addGradientInfo", &RendererDescriptor::addGradientInfo, "gradientInfo"_a)            
        .def("eraseGradientInfo", &RendererDescriptor::eraseGradientInfo)            
        .def("replaceGradient", &RendererDescriptor::replaceGradient, "newGradient"_a)
        .def("connectImagesAndMap", &RendererDescriptor::connectImagesAndMap)
        .def("randomizeGradientInfo", &RendererDescriptor::randomizeGradientInfo)
        .def("randomPositionFactor", &RendererDescriptor::randomPositionFactor,
            py::arg("rndPos")=0.0);



    py::class_<HeightMapDescriptor, std::shared_ptr<HeightMapDescriptor>>(m, "HeightMapDescriptor")
        .def(py::init<>())
        .def_static("create",&HeightMapDescriptor::create, "name"_a)
        .def_property("name",
            &HeightMapDescriptor::name, 
            &HeightMapDescriptor::setName);

    py::class_<ImageDescriptor, std::shared_ptr<ImageDescriptor>>(m, "ImageDescriptor")
        .def(py::init<>())
        .def_static("create",&ImageDescriptor::create, "name"_a)
        .def_property("name",
            &ImageDescriptor::name, 
            &ImageDescriptor::setName);

    py::class_<NoiseMapBuilderDescriptor, std::shared_ptr<NoiseMapBuilderDescriptor>>(m, "NoiseMapBuilderDescriptor")
        .def(py::init<>())
        .def("connectSrcModule", &NoiseMapBuilderDescriptor::connectSrcModule)
        .def_property("name",
            &NoiseMapBuilderDescriptor::name, 
            &NoiseMapBuilderDescriptor::setName)
        .def_property("dest",
            &NoiseMapBuilderDescriptor::dest, 
            &NoiseMapBuilderDescriptor::setDest)
        .def_property("noiseMaps",
            &NoiseMapBuilderDescriptor::noiseMaps, 
            &NoiseMapBuilderDescriptor::setNoiseMaps)
        .def_property("size",
            &NoiseMapBuilderDescriptor::size, 
            &NoiseMapBuilderDescriptor::setSize)            
        .def_property("bounds",
            &NoiseMapBuilderDescriptor::bounds, 
            &NoiseMapBuilderDescriptor::setBounds)            
        .def_property("sourceModule",
            &NoiseMapBuilderDescriptor::sourceModule, 
            &NoiseMapBuilderDescriptor::setSourceModule)            
        .def_property("seamless",
            &NoiseMapBuilderDescriptor::seamless, 
            &NoiseMapBuilderDescriptor::setSeamless)            
         .def_property("noiseMapBuilders",
            &NoiseMapBuilderDescriptor::noiseMapBuilders, 
            &NoiseMapBuilderDescriptor::setNoiseMapBuilders)            
        .def("getSizeX",&NoiseMapBuilderDescriptor::getSizeX)
        .def("getSizeY",&NoiseMapBuilderDescriptor::getSizeY)
        .def_static("create", &NoiseMapBuilderDescriptor::create,
            "name"_a, "module"_a, "heightMap"_a);



    py::class_<TextureBuilder, std::shared_ptr<TextureBuilder>>(m, "TextureBuilder")
        .def(py::init<>(), "Initialize the texture builder")
        .def_static("create", &TextureBuilder::create,           
            "Creates a shared instance of a TextureBuilder with a random planetary texture data ready to render",
            py::arg("earthlike")=false,
            py::arg("texture1")="",
            py::arg("texture2")="",
            py::arg("seaLevel")=0.1,
            py::arg("iceLevel")=0.5             
        )
        .def("initialize", &TextureBuilder::initialize,
            "Initializes  a TextureBuilder with a random planetary texture data ready to render",
            py::arg("earthlike")=false,
            py::arg("texture1")="",
            py::arg("texture2")="",
            py::arg("seaLevel")=0.1,
            py::arg("iceLevel")=0.5
        )
        .def_property("cloudMap",
            &TextureBuilder::cloudMap, 
            &TextureBuilder::setCloudMap   
            )       
        .def_property("colorMap",
            &TextureBuilder::colorMap, 
            &TextureBuilder::setColorMap
            )
        .def_property("reflectionMap",
            &TextureBuilder::reflectionMap, 
            &TextureBuilder::setReflectionMap
            )
        .def_property("bumpMap",
            &TextureBuilder::bumpMap, 
            &TextureBuilder::setBumpMap
            )
        .def_property("outputFileName",
            &TextureBuilder::outputFileName, 
            &TextureBuilder::setOutputFileName
            )
        .def("randomFactors",&TextureBuilder::randomFactors)
        .def("useRandomFactors",&TextureBuilder::useRandomFactors)
        .def("pickRandomFactor",&TextureBuilder::pickRandomFactor)
        .def("variateDouble",&TextureBuilder::variateDouble)
        .def("size",&TextureBuilder::size, "Returns a tuple with map size")
        .def("setSize",&TextureBuilder::setSize, "Set map size")
        .def("bounds",&TextureBuilder::bounds, "Returns a tuple with bounds (SNWE)")
        .def("setBounds", &TextureBuilder::setBounds, "Set Bounds (SNWE)",
            py::arg("south")=-90.0,
            py::arg("north")=90.0,
            py::arg("west")=180.0,
            py::arg("east")=-180.0)
        .def_property("builderType",
            &TextureBuilder::builderType, 
            &TextureBuilder::setBuilderType)
        .def("applyRandomFactor",&TextureBuilder::applyRandomFactor, "Apply custom random factor")
        .def_property("outputFolder",
            &TextureBuilder::outputFolder, 
            &TextureBuilder::setOutputFolder,
            "Gets or sets the folder where to write json and images")
        .def_property("destinationImagePath",
            &TextureBuilder::destinationImagePath, 
            &TextureBuilder::setDestinationImagePath)     
        .def("jsonString",&TextureBuilder::toJsonString)                            
        .def("noiseMapBuilders",&TextureBuilder::noiseMapBuilders, "Readonly list of libnoise MapBuilders objects")                           
        .def("heightMaps",&TextureBuilder::heightMaps,"Readonly list of libnoise HeightMaps objects")                            
        .def("images",&TextureBuilder::images, "Readonly list of libnoise Images objects")                            
        .def("renderers",&TextureBuilder::renderers,"Readonly list of libnoise renderers objects")                           
        .def("modules",&TextureBuilder::modules, "Readonly list of libnoise Modules objects")
        .def("hmDesc",&TextureBuilder::hmDesc, "Readonly dictionary of HeightMapDescriptors")    
        .def("getImageDescriptor", &TextureBuilder::getImageDescriptor, "Retrieve the ImageDescriptor for a given key")                       
        .def("appendImageDescriptor", (void (TextureBuilder::*)(const std::string&, std::shared_ptr<ImageDescriptor>)) &TextureBuilder::appendImageDescriptor, "Appends an ImageDescriptor with a given key")                       
        .def("appendImageDescriptor", (void (TextureBuilder::*)(std::shared_ptr<ImageDescriptor>)) &TextureBuilder::appendImageDescriptor, "Appends an ImageDescriptor")                       
        .def("appendImageDescriptor", (void (TextureBuilder::*)(std::vector<std::shared_ptr<ImageDescriptor>>)) &TextureBuilder::appendImageDescriptor, "Appends a list of image descriptors")                       
        .def("removeImageDescriptor", &TextureBuilder::removeImageDescriptor, "Rempves the ImageDescriptor for a given key")                       
        .def("imDesc",&TextureBuilder::imDesc, "Readoly dictionary of ImageDescriptor")                            
        .def("getHeightMapDescriptor", &TextureBuilder::getHeightMapDescriptor)                       
        .def("removeHeightMapDescriptor", &TextureBuilder::removeHeightMapDescriptor)                       
        .def("appendHeightMapDescriptor", (void (TextureBuilder::*)(const std::string&, std::shared_ptr<HeightMapDescriptor>)) &TextureBuilder::appendHeightMapDescriptor, "Appends an HeightMapDescriptor with a given key")                       
        .def("appendHeightMapDescriptor", (void (TextureBuilder::*)(std::shared_ptr<HeightMapDescriptor>)) &TextureBuilder::appendHeightMapDescriptor, "Appends an HeightMapDescriptor")                       
        .def("appendHeightMapDescriptor", (void (TextureBuilder::*)(std::vector<std::shared_ptr<HeightMapDescriptor>>)) &TextureBuilder::appendHeightMapDescriptor, "Appends a list of HeightMapDescriptor")                       
        .def("modDesc", &TextureBuilder::modDesc)                            
        .def("getModuleDescriptor", &TextureBuilder::getModuleDescriptor)                       
        .def("appendModuleDescriptor", (void (TextureBuilder::*)(const std::string&, std::shared_ptr<ModuleDescriptor>)) &TextureBuilder::appendModuleDescriptor, "Appends an appendModuleDescriptor with a given key")                       
        .def("appendModuleDescriptor", (void (TextureBuilder::*)(std::shared_ptr<ModuleDescriptor>)) &TextureBuilder::appendModuleDescriptor, "Appends an HeightMapDescriptor")                       
        .def("appendModuleDescriptor", (void (TextureBuilder::*)(std::vector<std::shared_ptr<ModuleDescriptor>>)) &TextureBuilder::appendModuleDescriptor, "Appends a list of HeightMapDescriptor")                       
        .def("removeModuleDescriptor", &TextureBuilder::removeModuleDescriptor)                       
        .def("nmbDesc",&TextureBuilder::nmbDesc)                            
        .def("getNoiseMapBuilderDescriptor", &TextureBuilder::getNoiseMapBuilderDescriptor)                       
        .def("appendNoiseMapBuilderDescriptor", (void (TextureBuilder::*)(const std::string&, std::shared_ptr<NoiseMapBuilderDescriptor>)) &TextureBuilder::appendNoiseMapBuilderDescriptor, "Appends an NoiseMapBuilderDescriptor with a given key")                       
        .def("appendNoiseMapBuilderDescriptor", (void (TextureBuilder::*)(std::shared_ptr<NoiseMapBuilderDescriptor>)) &TextureBuilder::appendNoiseMapBuilderDescriptor, "Appends an NoiseMapBuilderDescriptor")                       
        .def("appendNoiseMapBuilderDescriptor", (void (TextureBuilder::*)(std::vector<std::shared_ptr<NoiseMapBuilderDescriptor>>)) &TextureBuilder::appendNoiseMapBuilderDescriptor, "Appends a list of NoiseMapBuilderDescriptor")                       
        .def("removeNoiseMapBuilderDescriptor", &TextureBuilder::removeNoiseMapBuilderDescriptor)                       
        .def("rndDesc",&TextureBuilder::rndDesc)      
        .def("getRendererDescriptor", &TextureBuilder::getRendererDescriptor)                       
        .def("appendRendererDescriptor", (void (TextureBuilder::*)(const std::string&, std::shared_ptr<RendererDescriptor>)) &TextureBuilder::appendRendererDescriptor, "Appends an RendererDescriptor with a given key")                       
        .def("appendRendererDescriptor", (void (TextureBuilder::*)(std::shared_ptr<RendererDescriptor>)) &TextureBuilder::appendRendererDescriptor, "Appends an RendererDescriptor")                       
        .def("appendRendererDescriptor", (void (TextureBuilder::*)(std::vector<std::shared_ptr<RendererDescriptor>>)) &TextureBuilder::appendRendererDescriptor, "Appends a list of RendererDescriptor")                       
        .def("removeRendererDescriptor", &TextureBuilder::removeRendererDescriptor)                       
        .def("saveImageDescriptorToFile",(void (TextureBuilder::*)(std::string, std::string)) &TextureBuilder::saveImageDescriptorToFile,  
            "image_name"_a, "filename"_a, R"DOC(
            Saves the Image descriptor specified by its `image_name` to file specified by `filename`. 

            The texture must be prepared in advance by invoking `prepareTexureForRendering` method. 
            Regardless of the file extensions, images are generated using PNG format. 

            Parameters
            ----------
        )DOC")
        .def("saveImageDescriptorToFile",(void (TextureBuilder::*)(std::shared_ptr<ImageDescriptor>, std::string)) &TextureBuilder::saveImageDescriptorToFile,
            "image_name"_a, "filename"_a, R"DOC(
            Saves the specified Image descriptor  to the file specified by `filename`. 

            The texture must be prepared in advance by invoking `prepareTexureForRendering` method. 
            Regardless of the file extensions, images are generated using PNG format. 

            Parameters
            ----------
        )DOC")
        .def("saveAllImageDescriptors",&TextureBuilder::saveAllImageDescriptors, "path"_a="",R"DOC(
            Saves all images descriptors on specified path using `outputFileName`.  

            The texture must be prepared in advance by invoking `prepareTexureForRendering` method. 
            Regardless of the file extensions, images are generated using PNG format. 

            Parameters
            ----------
        )DOC")
        .def("rndNames",&TextureBuilder::rndNames)                                        
        .def("createModules",&TextureBuilder::createModules)
        .def("createHeightMaps",&TextureBuilder::createHeightMaps)
        .def("createNoiseMapBuilders",&TextureBuilder::createNoiseMapBuilders)
        .def("createRenderers",&TextureBuilder::createRenderers)
        .def("createImages",&TextureBuilder::createImages)
        .def("createAll",&TextureBuilder::createAll)
        .def("connectModules",&TextureBuilder::connectModules)
        .def("connectRenderers",&TextureBuilder::connectRenderers)
        .def("connectNoiseMapBuilders",&TextureBuilder::connectNoiseMapBuilders)
        .def("connectAll",&TextureBuilder::connectAll)
        .def("buildNoiseMaps",&TextureBuilder::buildNoiseMaps)
        .def("renderRenderers",&TextureBuilder::renderRenderers)
        .def("buildImages",&TextureBuilder::buildImages)
        .def("renderRenderers",&TextureBuilder::renderRenderers)
        .def("buildTexture",&TextureBuilder::buildTexture, "path"_a="", R"DOC(
            Builds and renders all textures to image files using the specified path and 
            prefixing filenames with `outputFileName` property

            Parameters
            ----------
        )DOC")
        .def("buildTextureFromJsonString",&TextureBuilder::buildTextureFromJsonString,
            "jsonData"_a, "path"_a="", R"DOC(
            Builds textures from JSON data and renders them
            to image files using the specified path and 
            prefixing filenames with `outputFileName` property

            Parameters
            ----------
        )DOC")
        .def("buildTextureFromJson",&TextureBuilder::buildTextureFromJson,
            "filename"_a, "path"_a="" , R"DOC(
            Builds textures from JSON data stored in `filename` and renders them
            to image files using the specified path and 
            prefixing filenames with `outputFileName` property

            Parameters
            ----------
        )DOC")
        .def("loadTextureFromJsonString",&TextureBuilder::loadTextureFromJsonString)
        .def("loadTextureFromJson",&TextureBuilder::loadTextureFromJson)
        .def("writeTextureToPath",&TextureBuilder::writeTextureToPath)
        .def("generatedMaps",&TextureBuilder::generatedMaps)
        .def("prepareObjectFromJsonFile",&TextureBuilder::prepareObjectFromJsonFile)
        .def("prepareObjectFromJsonString",&TextureBuilder::prepareObjectFromJsonString)
        .def("saveRenderedImageToFile",&TextureBuilder::saveRenderedImageToFile)
        .def("textureSanityCheck",&TextureBuilder::textureSanityCheck)
        .def("hasColorMap",&TextureBuilder::hasColorMap)
        .def("hasBumpMap",&TextureBuilder::hasBumpMap)
        .def("hasCloudMap",&TextureBuilder::hasCloudMap)
        .def("hasReflectionMap",&TextureBuilder::hasReflectionMap)
        .def("raiseRenderer",&TextureBuilder::raiseRenderer)
        .def("lowerRenderer",&TextureBuilder::lowerRenderer)
        .def("assignSizeInfoToNMBDesc",&TextureBuilder::assignSizeInfoToNMBDesc)
        .def("addRendererDescriptor",&TextureBuilder::addRendererDescriptor)
        .def("deleteRendererDescriptor",&TextureBuilder::deleteRendererDescriptor)
        .def("clearAllContainers", &TextureBuilder::clearAllContainers)
        .def("clearRandomFactors", &TextureBuilder::clearRandomFactors)
        .def("addRandomFactors",&TextureBuilder::addRandomFactors)
        .def("prepareTexureForRendering", &TextureBuilder::prepareTextureForRendering, R"DOC(            
                Prepares the textures for rendering, but does not write image data
                on .png files            

        )DOC")
        .def("connectDescriptorsAndPrepare", &TextureBuilder::connectDescriptorsAndPrepare)
        .def("Abs", &TextureBuilder::newAbs,"name"_a, "src1"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Abs** Module.

            The **Abs** module outputs the absolute value of the output value from a source module.

            Parameters
            ----------
        )DOC")
        .def("Add", &TextureBuilder::newAdd,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Add** Modulel.
            
            The **Add** module outputs addition of two source modules. 

            Parameters
            ----------
        )DOC")
        .def("Avg", &TextureBuilder::newAvg,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Avg** Module.
            
            The **Avg** module outputs the average value of the output of two source modules.

            Parameters
            ----------
        )DOC")
        .def("Avg4", &TextureBuilder::newAvg4,"name"_a, "src1"_a, "src2"_a, "src3"_a, "src4"_a, "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Avg4** Module.
            
            The **Avg4** module outputs the average value of the output of four source modules.

            Parameters
            ----------
        )DOC")
        .def("Billow", &TextureBuilder::newBillow, "name"_a,"seed"_a=0,  "freq"_a=1.0 , "lac"_a=2.0 , "pers"_a=0.5 ,  "oct"_a=8  ,  "enableRandom"_a = false, R"DOC(
            Creates an instance of the **Billow** Module.

            The **Billow** module is similar to Perlin; however, its output is more suited to render
            rocks or clouds. 
            
            Parameters
            ----------
        )DOC")
        .def("Blend", &TextureBuilder::newBlend,"name"_a, "src1"_a, "src2"_a, "ctl"_a,"enableRandom"_a = false)
        .def("Clamp", &TextureBuilder::newClamp, "name"_a, "src1"_a, "uBound"_a,"lBound"_a, "enableRandom"_a = false)
        .def("Const", &TextureBuilder::newConst, "name"_a, "value"_a, "enableRandom"_a=false)
        .def("Cos", &TextureBuilder::newCos, "name"_a, "src1"_a, "freq"_a, "exp"_a, "value"_a, "enableRandom"_a=false)
        .def("Cache", &TextureBuilder::newCache, "name"_a, "src1"_a)
        .def("Curve", &TextureBuilder::newCurve,"name"_a, "src1"_a, "cPoints"_a, "enableRandom"_a=false)
        .def("Curve", &TextureBuilder::newCurve,"name"_a, "src1"_a, "cpoints"_a, "enableRandom"_a=false)
        .def("Cylinder", &TextureBuilder::newCylinders, "name"_a, "freq"_a, "enableRandom"_a=false)
        .def("Cylinders", &TextureBuilder::newCylinders, "name"_a, "freq"_a, "enableRandom"_a=false)
        .def("Displace", &TextureBuilder::newDisplace,"name"_a, "src1"_a, "src2"_a, "src3"_a, "src4"_a, "ctl"_a, "enableRandom"_a=false)
        .def("Exponent", &TextureBuilder::newExponent,  "name"_a, "src1"_a, "exp"_a=1.0,  "enableRandom"_a=false)
        .def("Exp", &TextureBuilder::newExponent,  "name"_a, "src1"_a, "exp"_a=1.0,  "enableRandom"_a=false)
        .def("Invert", &TextureBuilder::newInvert,"name"_a, "src1"_a, "enableRandom"_a = false)
        .def("Max", &TextureBuilder::newMax,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def("Min", &TextureBuilder::newMin,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def("Multiply", &TextureBuilder::newMultiply,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def("Perlin", &TextureBuilder::newPerlin, "name"_a,"seed"_a=0,  "freq"_a =4.58, "lac"_a =1.25, "pers"_a =0.31,  "oct"_a = 8 ,  "enableRandom"_a = false)
        .def("Power", &TextureBuilder::newPower,"name"_a, "src1"_a, "src2"_a, "enableRandom"_a = false)
        .def("RidgedMulti", &TextureBuilder::newRidgedMulti, "name"_a,"seed"_a=0,  "freq"_a=1.0 , "lac"_a=2.0 , "oct"_a=7  ,  "enableRandom"_a = false)
        .def("RidgedMulti2", &TextureBuilder::newRidgedMulti2, "name"_a,"seed"_a=0,  "freq"_a=1.0 , "lac"_a=2.0 , "gain"_a=1.0 ,  "exp"_a=1.0 ,  "oct"_a=7  , "offset"_a=0, "enableRandom"_a = false)
        .def("RotatePoint", &TextureBuilder::newRotatePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def("ScaleBias", &TextureBuilder::newScaleBias , "name"_a, "src1"_a, "scale"_a, "bias"_a, "enableRandom"_a = false)
        .def("ScalePoint", &TextureBuilder::newScalePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def("Select", &TextureBuilder::newSelect, "name"_a, "src1"_a, "src2"_a, "ctl"_a, "uBound"_a , "lBound"_a ,"value"_a, "enableRandom"_a = false)
        .def("Sin", &TextureBuilder::newSin, "name"_a, "src1"_a, "freq"_a, "exp"_a, "value"_a, "enableRandom"_a = false)
        .def("Spheres", &TextureBuilder::newSpheres, "name"_a, "freq"_a, "enableRandom"_a = false)
        .def("Terrace", &TextureBuilder::newTerrace, "name"_a, "src1"_a, "cpoints"_a, "invert"_a = false, "enableRandom"_a = false)
        .def("TranslatePoint", &TextureBuilder::newTranslatePoint, "name"_a, "src1"_a, "x"_a, "y"_a, "z"_a, "enableRandom"_a = false)
        .def("Turbulence", &TextureBuilder::newTurbulence, "name"_a, "src1"_a, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def("Turbulence2", &TextureBuilder::newTurbulence2, "name"_a, "src1"_a, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def("TurbulenceBillow", &TextureBuilder::newTurbulenceBillow, "name"_a, "src1"_a=0, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def("TurbulenceRidged", &TextureBuilder::newTurbulenceRidged, "name"_a, "src1"_a=0, "seed"_a=0, "freq"_a, "pow"_a, "rough"_a, "enableRandom"_a = false)
        .def("Voronoi", &TextureBuilder::newVoronoi, "name"_a, "seed"_a=0, "freq"_a=5.0, "disp"_a=0.0, "enableDispl"_a=false, "enableRandom"_a = false)
        .def("Voronoi", &TextureBuilder::newVoronoi, "name"_a, "seed"_a=0, "freq"_a=5.0, "disp"_a=0.0, "enableDist"_a=false, "enableRandom"_a = false)
        .def("Renderer", 
            /*
            (std::shared_ptr<RendererDescriptor> (RendererDescriptor::*)
            (const std::string&, const std::string&, std::vector<GradientInfo>&,
            const std::string&, const std::string&, const std::string&, const std::string&, 
            double, double, bool, bool)
            */
            &TextureBuilder::createRendererDescriptor,
            "name"_a, "heightMap"_a,"gradientInfo"_a, "destImage"_a,
            "backgroundImage"_a=nullptr,"alphaImage"_a=nullptr, "bumpMap"_a=nullptr, "lightBrightness"_a=2.0,
            "lightContrast"_a=1.5,"enabledLight"_a=false, "randomGradient"_a=false, R"DOC(
            Creates, adds to the `TextureBuilder` instance and returns a `RendererDescriptor` instance.

            Parameters
            ----------
        )DOC" )
        .def("Image",&TextureBuilder::createImageDescriptor, "name"_a, R"DOC(
            Creates, adds to the `TextureBuilder` instance and returns an `ImageDescriptor` instance.

            
            Parameters
            ----------
        )DOC")
        .def("HeightMap",&TextureBuilder::createHeightMapDescriptor, "name"_a, R"DOC(
            Creates, adds to the `TextureBuilder` instance and returns an `HeightMapDescriptor` instance.

            
            Parameters
            ----------
        )DOC")
        .def("NoiseMapBuilder", &TextureBuilder::createNoiseMapBuilderDescriptor,
            "name"_a, "module"_a, "heightMap"_a, R"DOC(
            Creates, adds to the `TextureBuilder` instance and returns an `NoiseMapBuilderDescriptor` instance.

            
            Parameters
            ----------
        )DOC");        


}   
