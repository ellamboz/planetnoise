/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/


#ifndef TEXTUREBUILDER_H
#define TEXTUREBUILDER_H

#include <libnoise-helpers_global.h>
#include <texturebuilder/heightmapdescriptor.h>
#include <texturebuilder/imagedescriptor.h>
#include <texturebuilder/moduledescriptor.h>
#include <texturebuilder/noisemapbuilderdescriptor.h>
#include <texturebuilder/rendererdescriptor.h>
#include <vector>
#include <map>
#include <memory>
#include "ssg_structures.h"
#include <algorithm>
#include <json.hpp>
#include <ctime>
#include <cstdlib>

using namespace nlohmann;
using namespace std;

typedef map<string, shared_ptr<HeightMapDescriptor>> MapHeightMapDescriptors;
typedef map<string, shared_ptr<ImageDescriptor>> MapImageDescriptors;
typedef map<string, shared_ptr<ModuleDescriptor>> MapModuleDescriptors;
typedef map<string, shared_ptr<NoiseMapBuilderDescriptor>> MapNoiseMapBuilderDescriptors;
typedef map<string, shared_ptr<RendererDescriptor>> MapRendererDescriptors;

typedef map<string, shared_ptr<utils::NoiseMap>> MapHeightMaps;
typedef map<string, shared_ptr<utils::Image>> MapImages;
typedef map<string, shared_ptr<noise::module::Module>> MapModules;
typedef map<string, shared_ptr<utils::NoiseMapBuilder>> MapNoiseMapBuilders;
typedef map<string, shared_ptr<utils::RendererImage>> MapRenderers;

typedef vector<shared_ptr<utils::RendererImage>> ListRenderers;

typedef vector<double> RandomFactors;

class LIBNOISEHELPERSSHARED_EXPORT TextureBuilder
{

    MapHeightMapDescriptors _hmDesc;
    MapImageDescriptors __imDesc;
    MapModuleDescriptors _modDesc;
    MapNoiseMapBuilderDescriptors _nmbDesc;
    MapRendererDescriptors _rndDesc;
    vector<string> _rndNames;

    MapHeightMaps  _heightMaps;
    MapImages _images; 
    MapModules _modules;
    MapNoiseMapBuilders _noiseMapBuilders;
    MapRenderers _renderers;

    ListRenderers _lstRenderers;

    vector<string> _generatedMaps;

    string _cloudMap;
    string _colorMap;
    string _bumpMap;
    string _reflectionMap;
    RandomFactors _randomFactors;

    std::tuple<int,int> _size = std::tuple<int,int>(1024,512);
    std::tuple<double,double,double,double> _bounds = std::tuple<double,double,double,double>(-90.0,90.0,-180.0,180.0);
    string _builderType = "sphere";

    string _outputFolder;
    string _destinationImagePath = "";
    string _textureFile;
    string _outputFileName;


public:
    TextureBuilder() :     
        _cloudMap(""),
        _bumpMap(""),
        _outputFolder("."),
        _colorMap("image1"),
        _reflectionMap(""){ 
            srand(time(0)); 
        }
        
    static TextureBuilder create(bool earthlike = false, string texture1="", string texture2="", double seaLevel = 0.4, double iceLevel=0.85) {
        TextureBuilder b;
        b.initialize(earthlike,texture1,texture2, seaLevel, iceLevel);
        return b;
    }
    void initialize (bool earthlike = false, string texture1="", string texture2="", double seaLevel = 0.4, double iceLevel=0.85);

    void clearAllContainers() {
        _heightMaps.clear();
        _images.clear();
        _modules.clear();
        _noiseMapBuilders.clear();
        _renderers.clear();
        _lstRenderers.clear();
        _generatedMaps.clear();
        _hmDesc.clear();
        __imDesc.clear();
        _modDesc.clear();
        _nmbDesc.clear();
        _rndDesc.clear();
        _rndNames.clear();
        _randomFactors.clear();
        srand(time(0)); 
    }

    void clearRandomFactors() { _randomFactors.clear(); }    
    void addRandomFactors(std::vector<double>& factors) {
        _randomFactors.clear();
        for (const auto& f : factors) 
            _randomFactors.push_back(f);
    }

    inline const string& cloudMap() { return _cloudMap; }
    inline const string& colorMap() { return _colorMap; }
    inline const string& reflectionMap() { return _reflectionMap; }
    inline const string& bumpMap() { return _bumpMap; }
    inline const string& outputFileName() { return _outputFileName; }

    inline void setBumpMap(const string& m) { _bumpMap = m; }
    inline void setColorMap(const string& m) { _colorMap = m; }
    inline void setReflectionMap(const string& m) { _reflectionMap = m; }
    inline void setCloudMap(const string& m) { _cloudMap = m; }
    inline void setOutputFileName(const string& m) { _outputFileName = m;}

    inline RandomFactors& randomFactors() { return _randomFactors; }
    inline bool useRandomFactors() {
        bool bRes = _randomFactors.size() > 0 && _randomFactors[0] > 0.0;
       return bRes;
    }
    inline double pickRandomFactor() {
        return useRandomFactors() ?
                    _randomFactors[ SSGX::dx( _randomFactors.size() ) ] : 0.0;
    }

    double variateDouble(double origin, double rndFactor) {
        double d1 = SSGX::floatRand() * (2*rndFactor) - rndFactor;
        return origin + d1;
    }

    string getLua();


    std::tuple<int,int>& size() { return _size; }
    void setSize (int x, int y ) { _size = std::tuple<int,int>(x,y); }

    std::tuple<double,double,double,double> bounds() { return _bounds; }
    void setBounds (double south = -90.0, double north = 90.0, double west = -180.0, double east = 180.0) {
        _bounds = std::tuple<double,double,double,double>(south,north,west,east);
    }

    string& builderType() { return _builderType; }
    void setBuilderType(const string& b) { _builderType = b; }

    inline double applyRandomFactor (double v) {
        return useRandomFactors() ?
                    variateDouble(v, pickRandomFactor()) : v; }

    inline const string& outputFolder() { return _outputFolder; }
    inline void setOutputFolder(const string& f) { _outputFolder = f; }

    #ifdef JSON
    void fromJson(const QJsonObject& json);
    void toJson(QJsonObject& json);
    #else
    void fromJson(const json& j);
    void toJson(json& j);

    bool jsonDefinedNotNull(const json& j, const std::string& key) {
        auto it  = j.find(key);
        if (it != j.end()) {
            return !j[key].is_null();
        }
        else {
            return false;
        }
    }
    #endif // JSON

    // these routines create libnoise's objects and store them into
    // appropriate maps, to be referenced where needed.

    void createModules();
    void createHeightMaps();
    void createNoiseMapBuilders();
    void createRenderers();
    void createImages();

    const MapNoiseMapBuilders& noiseMapBuilders() { return _noiseMapBuilders; }
    const MapHeightMaps& heightMaps() { return _heightMaps; }
    const MapImages& images() { return _images; }
    const MapRenderers& renderers() { return _renderers; }
    const MapModules& modules() { return _modules; }

    MapHeightMapDescriptors& hmDesc() { return _hmDesc; }
    std::shared_ptr<HeightMapDescriptor> getHeightMapDescriptor(const string& k) { return _hmDesc[k]; }
    void appendHeightMapDescriptor(const string& k, shared_ptr<HeightMapDescriptor> p) { 
        _hmDesc.insert(std::pair< string, shared_ptr<HeightMapDescriptor>>(k,p));
    }
    void appendHeightMapDescriptor(shared_ptr<HeightMapDescriptor> p) { 
        _hmDesc.insert(std::pair< string, shared_ptr<HeightMapDescriptor>>(p->name(),p));
    }
    void appendHeightMapDescriptor(std::vector<shared_ptr<HeightMapDescriptor>> list) { 
        for(const auto& p : list) {
            _hmDesc.insert(std::pair< string, shared_ptr<HeightMapDescriptor>>(p->name(),p));
        }
    }

    void removeHeightMapDescriptor(const string& k) { 
        _hmDesc.erase(k);
    }

    MapImageDescriptors& imDesc() { return __imDesc;}
    std::shared_ptr<ImageDescriptor> getImageDescriptor(const string& k) { return __imDesc[k]; }
    void appendImageDescriptor(const string& k, shared_ptr<ImageDescriptor> p) { 
        __imDesc.insert(std::pair< string, shared_ptr<ImageDescriptor>>(k,p));
    }
    void appendImageDescriptor(shared_ptr<ImageDescriptor> p) { 
        __imDesc.insert(std::pair< string, shared_ptr<ImageDescriptor>>(p->name(),p));
    }
    void appendImageDescriptor(std::vector<shared_ptr<ImageDescriptor>> list) { 
        for(const auto& p : list) {
            __imDesc.insert(std::pair< string, shared_ptr<ImageDescriptor>>(p->name(),p));
        }
    }

    void removeImageDescriptor(const string& k) { 
        __imDesc.erase(k);
    }

    
    MapModuleDescriptors& modDesc() { return _modDesc;  }
    std::shared_ptr<ModuleDescriptor> getModuleDescriptor(const string& k) { return _modDesc[k]; }
    void appendModuleDescriptor(const string& k, shared_ptr<ModuleDescriptor> p) { 
        _modDesc.insert(std::pair< string, shared_ptr<ModuleDescriptor>>(k,p));
    }
    void appendModuleDescriptor(shared_ptr<ModuleDescriptor> p) { 
        _modDesc.insert(std::pair< string, shared_ptr<ModuleDescriptor>>(p->name(),p));
    }
    void appendModuleDescriptor(std::vector<shared_ptr<ModuleDescriptor>> list) { 
        for(const auto& p : list) {
            _modDesc.insert(std::pair< string, shared_ptr<ModuleDescriptor>>(p->name(),p));
        }
    }    
    void removeModuleDescriptor(const string& k) { 
        _modDesc.erase(k);
    }


    MapNoiseMapBuilderDescriptors& nmbDesc() { return _nmbDesc; }
    std::shared_ptr<NoiseMapBuilderDescriptor> getNoiseMapBuilderDescriptor(const string& k) { return _nmbDesc[k]; }
    void appendNoiseMapBuilderDescriptor(const string& k, shared_ptr<NoiseMapBuilderDescriptor> p) { 
        _nmbDesc.insert(std::pair< string, shared_ptr<NoiseMapBuilderDescriptor>>(k,p));
    }
    void appendNoiseMapBuilderDescriptor(shared_ptr<NoiseMapBuilderDescriptor> p) { 
        _nmbDesc.insert(std::pair< string, shared_ptr<NoiseMapBuilderDescriptor>>(p->name(),p));
    }
    void appendNoiseMapBuilderDescriptor(std::vector<shared_ptr<NoiseMapBuilderDescriptor>> list) { 
        for(const auto& p : list) {
            _nmbDesc.insert(std::pair< string, shared_ptr<NoiseMapBuilderDescriptor>>(p->name(),p));
        }
    }        
    void removeNoiseMapBuilderDescriptor(const string& k) { 
        _nmbDesc.erase(k);
    }


    const MapRendererDescriptors& rndDesc() { return _rndDesc; }
    std::shared_ptr<RendererDescriptor> getRendererDescriptor(const string& k) { return _rndDesc[k]; }
    void appendRendererDescriptor(const string& k, shared_ptr<RendererDescriptor> p) { 
        _rndDesc.insert(std::pair< string, shared_ptr<RendererDescriptor>>(k,p));
        _rndNames.push_back(k);
    }
    void appendRendererDescriptor(shared_ptr<RendererDescriptor> p) { 
        _rndDesc.insert(std::pair< string, shared_ptr<RendererDescriptor>>(p->name(),p));
        _rndNames.push_back(p->name());
    }
    void appendRendererDescriptor(std::vector<shared_ptr<RendererDescriptor>> list) { 
        for(const auto& p : list) {
            _rndDesc.insert(std::pair< string, shared_ptr<RendererDescriptor>>(p->name(),p));
            _rndNames.push_back(p->name());
        }
    }          
    void removeRendererDescriptor(const string& k) { 
        _rndDesc.erase(k);
        auto itr = std::find(_rndNames.begin(), _rndNames.end(), k);
        if (itr != _rndNames.end()) 
            _rndNames.erase(itr);
    }

    vector<string>& rndNames() { return _rndNames; }

    void createAll() {
        // qDebug() << "Creating modules";
        createModules();
        // qDebug() << "Creating height maps";
        createHeightMaps();
        // qDebug() << "Creating map builders";
        createNoiseMapBuilders();
        // qDebug() << "Creating images";
        createImages();
        // qDebug() << "Creating renderers";
        createRenderers();
    }

    void connectModules();
    void connectRenderers();
    void connectNoiseMapBuilders();

    void connectAll() {
        cerr << "Connecting modules... "  << endl;
        connectModules();
        cerr << "Connecting renderers... "  << endl;
        connectRenderers();
        cerr << "Connecting map builders... "  << endl;
        connectNoiseMapBuilders();
        cerr << "Building noisemaps... "  << endl;
        buildNoiseMaps();
        cerr << "All connected!... "  << endl;
    }

    void buildNoiseMaps();
    void renderRenderers();

    shared_ptr<utils::Image> buildImages() {
        renderRenderers();
        return _images.begin()->second;
        //return _images[0];
    }


    void buildTextureFromJsonString(const string& jsonData,string path="");
    void buildTextureFromJson(const string& filename,string path="");
    void buildTexture(string path="") {
        auto j = this->toJsonString();
        this->buildTextureFromJsonString(j, path);
    }

    void loadTextureFromJsonString(const string& jsonData);
    void loadTextureFromJson(const string& filename);
    void connectDescriptorsAndPrepare();

    void writeTextureToPath(string path = "");
    void saveImageDescriptorToFile(shared_ptr<ImageDescriptor> image, string filename);
    void saveImageDescriptorToFile(string image_name, string filename);
    void saveAllImageDescriptors(string path="") {
        writeTextureToPath(path);
    }

    const vector<string>& generatedMaps() {
        return _generatedMaps;
    }

    void prepareTextureForRendering();
    void prepareObjectFromJsonFile (const string& filename);
    void prepareObjectFromJsonString(const string& jsonData);
    void saveRenderedImageToFile (const string& imageName, const string& destFileName);
    void textureSanityCheck();

    bool hasColorMap() { return _colorMap != ""; }
    bool hasBumpMap() { return _bumpMap != ""; }
    bool hasCloudMap() { return _cloudMap != ""; }
    bool hasReflectionMap() { return _reflectionMap != ""; }

    void assignSizeInfoToNMBDesc(NoiseMapBuilderDescriptor *d) {
        d->setBuilderType(NoiseMapBuilderType::SPHERE);
        d->setSize(std::get<0>(_size), std::get<1>(_size));
        d->setBounds(std::get<0>(_bounds),std::get<1>(_bounds),std::get<2>(_bounds),std::get<3>(_bounds));

    }

    void addRendererDescriptor(const string& key, shared_ptr<RendererDescriptor> rnd) {
        if (_rndDesc.count(key) == 0) {
            _rndDesc[key] = rnd;
            _rndNames.push_back(key);
        }
        else {
            string err = "Renderer " + key + " already present, cannot add renderer";
            throw err;
        }
    }

    void deleteRendererDescriptor(const string& key) {
        if (_rndDesc.count(key) != 0) {
            _rndDesc.erase(key);
            auto it = std::find(_rndNames.begin(),_rndNames.end(),key);
            _rndNames.erase(it);
        }
        else {
            string err = "Renderer " + key + " not present, cannot remove item";
            throw err;
        }
    }

    const string& destinationImagePath() { return _destinationImagePath; }
    void setDestinationImagePath(const string& img) { _destinationImagePath = img; }

    void ciccio();

    void raiseRenderer(const string& rendererName);
    void lowerRenderer(const string& rendererName);

    std::string toJsonString() {
        nlohmann::json j;
        this->toJson(j);
        return j.dump(4);
    }

    //helper modules 
    shared_ptr<ModuleDescriptor> newAbs (string name, 
        shared_ptr<ModuleDescriptor>  src1, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Abs");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newAdd (string name, 
        shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Add");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newAvg (string name, 
        shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Avg");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newAvg4 (string name, 
        shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2,  
        shared_ptr<ModuleDescriptor>  src3, 
        shared_ptr<ModuleDescriptor>  src4, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Avg4");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setSrc3(src3->name());
        md->setSrc4(src4->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newBillow (string name, int seed,  double freq , double lac , double pers ,  int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Billow");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setPers(pers);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newBlend (string name, shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2, 
        shared_ptr<ModuleDescriptor>  ctl, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Blend");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setCtl(ctl->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newClamp (string name, shared_ptr<ModuleDescriptor>  src1, double uBound, double lBound, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Clamp");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setUbound(uBound);
        md->setLbound(lBound);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newConst (string name, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Const");
        md->setName(name);
        md->setValue(value);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newCos (string name, shared_ptr<ModuleDescriptor>  src1, double freq, double exp, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cos");
        md->setName(name);
        md->setValue(value);
        md->setSrc1(src1->name());
        md->setFreq(freq);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newCache (string name, shared_ptr<ModuleDescriptor>  src1) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cos");
        md->setSrc1(src1->name());
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }    
    shared_ptr<ModuleDescriptor> newCurve (string name, shared_ptr<ModuleDescriptor>  src1, std::vector<std::tuple<double,double>>& cPoints,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Curve");
        md->setName(name);
        md->setSrc1(src1->name());
        md->cPoints().clear();
        for (auto& cp : cPoints) {
            md->cPoints().push_back(cp);
        }
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newCylinders (string name, double freq, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cylinders");
        md->setName(name);
        md->setFreq(freq);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newDisplace (string name, shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2,  
        shared_ptr<ModuleDescriptor>  src3, 
        shared_ptr<ModuleDescriptor> src4, 
        shared_ptr<ModuleDescriptor>  ctl, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Displace");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setSrc3(src3->name());
        md->setSrc4(src4->name());
        md->setCtl(ctl->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newExponent (string name, shared_ptr<ModuleDescriptor>  src1, double exp, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Exponent");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newInvert (string name, shared_ptr<ModuleDescriptor>  src1, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Invert");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newMax (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Max");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newMin (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Min");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newMultiply (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Multiply");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newPower (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Power");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newPerlin (string name, int seed,  double freq , double lac , double pers , 
        int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Perlin");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setPers(pers);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newRidgedMulti (string name, int seed,  double freq , double lac ,  
        int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RidgedMulti");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newRidgedMulti2 (string name, int seed,  double freq , double lac ,  
        double gain, double exp, int oct,double offset, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RidgedMulti2");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setOct(oct);
        md->setExp(exp);
        md->setGain(gain);    
        md->setOffset(offset) ;  
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newRotatePoint (string name, shared_ptr<ModuleDescriptor>  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RotatePoint");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newScaleBias (string name, shared_ptr<ModuleDescriptor>  src1, 
        double scale, double bias, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("ScaleBias");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setScale(scale);
        md->setBias(bias);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newScalePoint (string name, shared_ptr<ModuleDescriptor>  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("ScalePoint");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newSelect (string name, 
        shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, 
        shared_ptr<ModuleDescriptor>  ctl, double uBound, double lBound, double value, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Select");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setCtl(ctl->name());
        md->setUbound(uBound);
        md->setLbound(lBound);
        md->setValue(value);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newSin (string name, shared_ptr<ModuleDescriptor>  src1, double freq, double exp, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Sin");
        md->setName(name);
        md->setValue(value);
        md->setSrc1(src1->name());
        md->setFreq(freq);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newSpheres (string name,  double freq, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Spheres");
        md->setName(name);
        md->setFreq(freq);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newTranslatePoint (string name, shared_ptr<ModuleDescriptor>  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TranslatePoint");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }
    shared_ptr<ModuleDescriptor> newTurbulence (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Turbulence");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }

    shared_ptr<ModuleDescriptor> newTurbulence2 (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Turbulence2");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }

    shared_ptr<ModuleDescriptor> newTurbulenceBillow (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TurbulenceBillow");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }

    shared_ptr<ModuleDescriptor> newTurbulenceRidged (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TurbulenceRidged");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }    

    shared_ptr<ModuleDescriptor> newVoronoi (string name, 
        int seed , double freq , double displ , bool enableDispl , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Voronoi");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setDispl(displ);
        md->setEnabledist(enableDispl);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }    
    shared_ptr<ModuleDescriptor> newTerrace (string name, shared_ptr<ModuleDescriptor>  src1, 
        std::vector<std::tuple<double,double>>& cPoints, bool invert, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Terrace");
        md->setName(name);
        md->setSrc1(src1->name());
        md->cPoints().clear();
        for (auto& cp : cPoints) {
            md->cPoints().push_back(cp);
        }
        md->setInvert(invert);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        this->appendModuleDescriptor(md);
        return md;
    }

    std::shared_ptr<HeightMapDescriptor> createHeightMapDescriptor(std::string name) {
        auto p = std::make_shared<HeightMapDescriptor>();
        p->setName(name);
        this->appendHeightMapDescriptor(p);
        return p;
    }

    
    std::shared_ptr<ImageDescriptor> createImageDescriptor(std::string name) {
        auto p = std::make_shared<ImageDescriptor>();
        p->setName(name);
        this->appendImageDescriptor(p);
        return p;
    }

    shared_ptr<NoiseMapBuilderDescriptor> createNoiseMapBuilderDescriptor(string name, shared_ptr<ModuleDescriptor>  module, shared_ptr<HeightMapDescriptor>  heightMap) {
        auto desc = make_shared<NoiseMapBuilderDescriptor>();
        desc->setName(name);
        desc->setSourceModule(module->name());
        desc->setDest(heightMap->name());
        this->appendNoiseMapBuilderDescriptor(desc);
        return desc;
    }    

    std::shared_ptr<RendererDescriptor> createRendererDescriptor (
        const string& name, 
        std::shared_ptr<HeightMapDescriptor> heightMap , 
        vector<GradientInfo>& gradientInfo, 
        std::shared_ptr<ImageDescriptor> destImage,
        std::shared_ptr<ImageDescriptor> backgroundImage = nullptr, 
        std::shared_ptr<ImageDescriptor> alphaImage = nullptr,
        std::shared_ptr<HeightMapDescriptor> bumpMap = nullptr,
        double lightBrightness = 2.0,
        double lightContrast = 1.5,
        bool enabledLight = false, 
        bool randomGradient = false
    ) {
        auto p = make_shared<RendererDescriptor>();
        p->setName(name);
        p->setHeightmap(heightMap->name());
        p->setDestImage(destImage->name());
        p->replaceGradient(gradientInfo);
        if (backgroundImage != nullptr) p->setBackgroundImage(backgroundImage->name());
        if (bumpMap != nullptr) p->setBumpMap(bumpMap->name());
        if (alphaImage != nullptr) p->setAlphaImage(alphaImage->name());
        p->setLightbrightness(lightBrightness);
        p->setLightcontrast(lightContrast);
        p->setEnabledlight(enabledLight);
        p->setRandomGradient(randomGradient);
        
        this->appendRendererDescriptor(p);

        return p;
    }    
};




#endif // TEXTUREBUILDER_H
