/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/



#ifndef MODULEDESCRIPTOR_H
#define MODULEDESCRIPTOR_H

#include <tuple>
#include <noiseutils.h>
#include <ssg_structures.h>
#include "libnoise-helpers_global.h"
#include "noise/module/cos.h"
#include <vector>
#include <memory>
#include <map>
#include <algorithm>
#include <json.hpp>
#include <iostream>

using namespace std;
using namespace nlohmann;

namespace NoiseModules {
    static vector<std::string> moduleList = {
        "Abs",
        "Add",
        "Avg",
        "Avg4",
        "Billow",
        "Blend",
        "Clamp",
        "Const",
        "Cos",
        "Curve",
        "Cylinders",
        "Displace",
        "Exponent",
        "Invert",
        "Max",
        "Min",
        "Multiply",
        "Perlin",
        "Power",
        "RidgedMulti",
        "RidgedMulti2",
        "RotatePoint",
        "ScaleBias",
        "ScalePoint",
        "Select",
        "Sin",
        "Spheres",
        "Terrace",
        "TranslatePoint",
        "Turbulence",
        "Turbulence2",
        "TurbulenceBillow",
        "TurbulenceRidged",
        "Voronoi",
        "Cache"
    };
}

using namespace noise;
using namespace noise::module;

class LIBNOISEHELPERSSHARED_EXPORT ModuleDescriptor
{

    std::string _moduleType = "Perlin";
    std::string _name = "perlin1";
    std::string _src1 = "";
    std::string _src2 = "";
    std::string _src3 = "";
    std::string _src4 = "";
    std::string _ctl;


    int _seed = 0;
    int _actualSeed = 0;
    int _oct = 6;
    double _freq = 2.50;
    double _lac = 3.20;
    double _pers = 0.30 ;
    double _offset = 0.0;
    double _gain = 2.0 ;
    double _disp = 0.0;
    double _lBound = 0.0;
    double _uBound = 0.0;
    double _exp = 2.0;
    double _bias = 1.0;
    double _scale = 1.0;
    double _x = 1.0;
    double _y = 1.0;
    double _z = 1.0;
    double _pow = 1.0;
    double _rough = 1.0;
    double _value = 1.0;
    bool _enableRandom = false;
    bool _invert = false;
    bool _enableDist = false;

    vector<std::tuple<double,double>> _cPoints;

    //reference to a Map containing loaded modules
    map<std::string, shared_ptr<Module>> _modules;

    //properties to export to json
    vector<std::string> _propertiesToExport;

public:
    //getters
    std::string& moduleType() { return _moduleType; }
    std::string& name() { return _name; }
    int seed() { return _seed; }
    int actualSeed() { return _actualSeed; }
    double freq() { return _freq; }
    double lac() { return _lac; }
    double pers() { return _pers; }
    double offset() { return _offset; }
    double gain() { return _gain; }
    int oct() { return _oct; }
    double displ() { return _disp; }
    bool enableDist() { return _enableDist; }
    std::string& src1() { return _src1; }
    std::string& src2() { return _src2; }
    std::string& src3() { return _src3; }
    std::string& src4() { return _src4; }
    std::string& ctl() { return _ctl; }
    double lBound() { return _lBound; }
    double uBound() { return _uBound; }
    vector<std::tuple<double,double>>& cPoints() { return _cPoints; }
    double exp() { return _exp; }
    double bias() { return _bias; }
    double scale() { return _scale; }
    bool invert() { return _invert; }
    double x() { return _x; }
    double y() { return _y; }
    double z() { return _z; }
    double pow() { return _pow; }
    double rough() { return _rough; }
    double value() { return _value; }
    bool enableRandom() { return _enableRandom; }

    std::string luaDeclaration() { return _name + "=" + _moduleType + ".new()"; }
    std::string luaInitialization() ;

    void debugLine() {
        cerr << "Module:" << _name << ", "
             << "Type:" << _moduleType << ", "
             << "Ctl:" << _ctl << ", "
             << "Src1:" << _src1 << ", "
             << "Src2:" << _src2 << ", "
             << "Src3:" << _src3 << ", "
             << "Src4:" << _src4 << endl;
        cerr << "oct:" << _oct << ", "
             << "freq:" << _freq << ", "
             << "lac:" << _lac << ", "
             << "pers:" << _pers << ", "
             << "offset:" << _offset << ", "
             << "gain:" << _gain << ", "
             << "disp:" << _disp << endl;
        cerr << "lbound:" << _lBound << ", "
             << "ubound:" << _uBound << ", "
             << "exp:" << _exp << ", "
             << "bias:" << _bias << ", "
             << "scale:" << _scale << ", "
             << "pow:" << _pow   << ", "
             << "rough:" << _rough << ", "
             << "value:" << _disp << endl;
    }

    void appendToProperties (const std::vector<std::string>& data ) {
        _propertiesToExport.insert(
                                   _propertiesToExport.end(),
                                   data.begin(),
                                   data.end()
                                   );
    }

    inline void dumpModule() {
        /*
        qDebug() << "ModuleType:" << _moduleType << " Name:" << _name << " Seed:" << _seed << " ActualSeed:" << _actualSeed
                 << " Freq:" << _freq << " Lac:" << _lac << " Pers:" << _pers << " Oct:" << _oct
                 << " Displ:" << _disp << " EnableDist:" << _enableDist << " Src1:" << _src1 << " Src2:" << _src2
                 << " Src3:" << _src3 << " Src4:" << _src4 << " Ctl:" << _ctl << " Ubound:" << _uBound
                 << " Lbound:" << _lBound << " Exp:" << _exp << " Bias:" << _bias << " Scale:" << _scale << " Invert:" << _invert
                 << " x:" << _x << " y:" << _y << " z:" << _z << " Pow:" << _pow << " Rough:" << _rough << " value:" << _value
                 << " EnableRandom" << _enableRandom; // << " CPoints:" << _cPoints;
        */
    }

    const map<std::string, shared_ptr<Module>>& modules() { return _modules; }
    vector<std::string>& propertiesToExport() { return _propertiesToExport; }

    //setters
    void setModuleType (std::string v) { _moduleType = v; this->setupPropertiesToExport(v); }
    void setName(std::string v) { _name = v ; }
    void setSeed(int v) { _seed = v ; }
    void setActualSeed(int v) { _actualSeed = v ; }
    void setFreq(double v) { _freq = v ; }
    void setLac(double v) { _lac = v ; }
    void setPers(double v) { _pers = v ; }
    void setOffset(double v) { _offset = v ; }
    void setGain(double v) { _gain = v ; }
    void setOct(int v) { _oct = v ; }
    void setDispl(double v) { _disp = v ; }
    void setEnabledist(bool v) { _enableDist = v ; }
    void setSrc1(const std::string& v) { _src1 = v ; }
    void setSrc2(const std::string& v) { _src2 = v ; }
    void setSrc3(const std::string& v) { _src3 = v ; }
    void setSrc4(const std::string& v) { _src4 = v ; }
    void setCtl(const std::string& v) { _ctl = v ; }
    void setLbound(double v) { _lBound = v ; }
    void setUbound(double v) { _uBound = v ; }
    void setCpoints(const vector<std::tuple<double,double>>& v) { _cPoints = v ; }
    void setExp(double v) { _exp = v ; }
    void setBias(double v) { _bias = v ; }
    void setScale(double v) { _scale = v ; }
    void setInvert(bool v) { _invert = v ; }
    void setX(double v) { _x = v ; }
    void setY(double v) { _y = v ; }
    void setZ(double v) { _z = v ; }
    void setPow(double v) { _pow = v ; }
    void setRough(double v) { _rough = v ; }
    void setValue(double v) { _value = v ; }

    void setModules(const map<std::string, shared_ptr<Module>>& m) {_modules = m; }
    void setEnableRandom(bool b) { _enableRandom = b; }


    #ifdef JSON
    void fromJson(const QJsonObject& json);
    void toJson(QJsonObject& json);
    #else


    void fromJson(const nlohmann::json& j);
    void toJson(nlohmann::json& j);
    #endif // JSON

    void setupPropertiesToExport(std::string& _m_moduleType);

    inline bool propContains(const std::string& item) {
        return std::find(_propertiesToExport.begin(), _propertiesToExport.end(), item) != _propertiesToExport.end();
    }

    inline bool moduleContains (const std::string& key) {
        return _modules.find(key) != _modules.end();
    }

    ModuleDescriptor();

    //static ModuleDescriptor createRandom();

    bool mustConnect() { return _src1 != "" && _src2 != "" && _src3 != "" && _src4 != ""; }

    ModuleDescriptor& connectModules();
    shared_ptr<Module> makeModule() ;
    //{ "module":"Billow" , "mame": "mod_name" , "seed": 0.0 , "freq": 0.0 , "lac": 0.0 , "pers": 0.0 ,
    shared_ptr<Module> makeBillow();
    //{ "module":"Checkerboard" , "name": "mod_name" },
    shared_ptr<Module> makeCheckerboard();
    //{ "module":"Const" , "name": "mod_name", "value" : 0.0 },
    shared_ptr<Module> makeConst();
    //{ "module":"Cylinders" , "name": "mod_name" , "Freq": 0.0 },
    shared_ptr<Module> makeCylinders();
    //{ "Module":"Perlin" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Lac": 0.0 , "Pers": 0.0 , "Oct": 0.0 },
    shared_ptr<Module> makePerlin();
    //{ "Module":"RidgedMulti" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Lac": 0.0 , "Oct": 0.0 },
    shared_ptr<Module> makeRidgedMulti();
    //{ "Module":"RidgedMulti" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Lac": 0.0 , "Oct": 0.0 },
    shared_ptr<Module> makeRidgedMulti2();
    //{ "Module":"Spheres" , "Name": "mod_name" , "Freq": 0.0 },
    shared_ptr<Module> makeSpheres();
    //{ "Module":"Voronoi" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Displ": 0.0 , "EnableDist": true },
    shared_ptr<Module> makeVoronoi();
    //{ "Module":"Abs" , "Name": "mod_name" , "Src1": "mod_name" },
    shared_ptr<Module> makeAbs();
    //{ "Module":"Clamp" , "Name": "mod_name" , "Src1": "mod_name" , "Lbound": 0.0 , "Ubound": 0.0 },
    shared_ptr<Module> makeClamp();
    //{ "Module":"Curve" , "Name": "mod_name" , "Src1": "mod_name" , "Cpoints": [[0.0,1.0],[1.0,1.1]] },
    shared_ptr<Module> makeCurve();
    //{ "Module":"Exponent" , "Name": "mod_name" , "Src1": "mod_name" , "Exp": 0.0 },
    shared_ptr<Module> makeExponent();
    //{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
    shared_ptr<Module> makeInvert();
    //{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
    shared_ptr<Module> makeCos();
    //{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
    shared_ptr<Module> makeSin();
    //{ "Module":"ScaleBias" , "Name": "mod_name" , "Src1": "mod_name" , "Bias": 0.0 , "Scale": 0.0 },
    shared_ptr<Module> makeScaleBias();
    //{ "Module":"Terrace" , "Name": "mod_name" , "Src1": "mod_name" , "Cpoints": [[0.0,1.0],[1.0,1.1]] , "Invert": true },
    shared_ptr<Module> makeTerrace();

    //{ "Module":"Turbulence" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Src1": "mod_name" , "Pow": 0.0 , "Rough": 0.0 }]
    shared_ptr<Module> makeTurbulence();
    shared_ptr<Module> makeTurbulence2();
    shared_ptr<Module> makeTurbulenceBillow();
    shared_ptr<Module> makeTurbulenceRidged();

    //{ "Module":"Add" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    shared_ptr<Module> makeAdd ();

    //{ "Module":"Max" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    shared_ptr<Module> makeMax ();

    //{ "Module":"Min" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    shared_ptr<Module> makeMin ();

    //{ "Module":"Avg" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    shared_ptr<Module> makeAvg ();

    //{ "Module":"Avg" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    shared_ptr<Module> makeAvg4 ();

    //{ "Module":"Multiply" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    //{ "Module":"Power" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" },
    //{ "Module":"Blend" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" , "Src3": "mod_name" },
    //{ "Module":"Select" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" , "Src3": "mod_name" , "Lbound": 0.0 , "Ubound": 0.0 },
    shared_ptr<Module> makeMultiply ();
    shared_ptr<Module> makePower ();
    shared_ptr<Module> makeBlend ();
    shared_ptr<Module> makeSelect ();


    //{ "Module":"Displace" , "Name": "mod_name" , "Src1": "mod_name" , "Src2": "mod_name" , "Src3": "mod_name" , "Src4": "mod_name" },
    //{ "Module":"Rotate" , "Name": "mod_name" , "Src1": "mod_name" , "x": 0.0 , "y": 0.0 , "z": 0.0 },
    //{ "Module":"ScalePoint" , "Name": "mod_name" , "Src1": "mod_name" , "x": 0.0 , "y": 0.0 , "z": 0.0 },
    //{ "Module":"TranslatePoint" , "Name": "mod_name" , "Src1": "mod_name" , "x": 0.0 , "y": 0.0 , "z": 0.0 },

    shared_ptr<Module> makeDisplace ();
    shared_ptr<Module> makeRotatePoint ();
    shared_ptr<Module> makeScalePoint ();
    shared_ptr<Module> makeTranslatePoint ();
    shared_ptr<Module> makeCache();

    //
    //    if (_m_moduleType=="Abs") appendToProperties( { "name" ,  "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Add") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    //    if (_m_moduleType=="Avg") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    //    if (_m_moduleType=="Billow")  appendToProperties( { "name" , "seed" , "freq" , "lac" , "pers" , "oct"  , "enableRandom"} );
    inline static ModuleDescriptor CreateBillow(string _name) {
        ModuleDescriptor _m;
        _m._freq = 1.50 + 4.0*SSGX::floatRand();
        _m._lac  = 0.50 + 5.0*SSGX::floatRand();
        _m._enableRandom = true;
        _m._name = _name;
        _m._moduleType = "Billow";
        _m._oct = 8;
        return _m;
    }
    //    if (_m_moduleType=="Blend") appendToProperties( { "name" ,  "src1" , "src2" , "ctl" , "enableRandom"} );
    //    if (_m_moduleType=="Clamp") appendToProperties( { "name" , "lBound" , "uBound" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Const") appendToProperties( { "name" , "value" , "enableRandom"} );
    //    if (_m_moduleType=="Cos") appendToProperties( { "name" ,  "freq" , "exp" , "value" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Curve") appendToProperties( { "name" , "controlPoints" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Cylinders")  appendToProperties( { "name" , "freq" , "enableRandom"} );
    //    if (_m_moduleType=="Displace") appendToProperties( { "name" ,    "src1" , "src2" , "src3" , "src4" , "ctl" , "enableRandom"} );
    //    if (_m_moduleType=="Exponent") appendToProperties( { "name" , "exp" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Invert") appendToProperties( { "name" ,  "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Max") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    //    if (_m_moduleType=="Min") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    //    if (_m_moduleType=="Multiply") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    //    if (_m_moduleType=="Perlin")  appendToProperties( { "name" , "seed" , "freq" , "lac" , "pers" , "oct" , "enableRandom"} );
    inline static ModuleDescriptor CreatePerlin(string _name) {
        ModuleDescriptor _m;
        _m._freq = 1.50 + 4.0*SSGX::floatRand();
        _m._lac  = 0.50 + 5.0*SSGX::floatRand();
        _m._pers = 0.15 + 1.0*SSGX::floatRand();
        _m._enableRandom = true;
        _m._oct = 8;
        _m._name = _name;
        _m._moduleType = "Perlin";
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }

    //    if (_m_moduleType=="Power") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    //    if (_m_moduleType=="RidgedMulti")  appendToProperties( { "name" "seed" , "freq" , "lac" , "oct" , "enableRandom"} );
    inline static ModuleDescriptor CreateRidgedMulti(string _name) {
        ModuleDescriptor _m;
        _m._freq = 1.50 + 4.0*SSGX::floatRand();
        _m._lac  = 0.50 + 5.0*SSGX::floatRand();
        _m._pers = 0.15 + 1.0*SSGX::floatRand();
        _m._enableRandom = true;
        _m._oct = 8;
        _m._name = _name;
        _m._moduleType = "RidgedMulti";
        _m.setupPropertiesToExport(_m._moduleType);

        return _m;
    }

    //    if (_m_moduleType=="RidgedMulti2")  appendToProperties( { "name" , "offset" , "gain" , "exp" , "seed" , "freq" , "lac" , "oct" , "enableRandom"} );
    //    if (_m_moduleType=="RotatePoint") appendToProperties( { "name" , "src1" , "x" , "y" , "z" , "enableRandom"} );
    //    if (_m_moduleType=="ScaleBias") appendToProperties( { "name" , "bias" , "scale" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="ScalePoint") appendToProperties( { "name" , "src1" , "x" , "y" , "z" , "enableRandom"} );
    //    if (_m_moduleType=="Select") appendToProperties( { "name" ,  "src1" , "src2" , "ctl" , "uBound" , "lBound" , "value" , "enableRandom"} );
    //    if (_m_moduleType=="Sin") appendToProperties( { "name" ,  "freq" , "exp" , "value" ,  "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Spheres") appendToProperties( { "name" , "freq" , "enableRandom"} );
    //    if (_m_moduleType=="Terrace") appendToProperties( { "name" , "controlPoints" , "invert" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="TranslatePoint") appendToProperties( { "name" , "src1" , "x" , "y" , "z" , "enableRandom"} );
    //    if (_m_moduleType=="Turbulence")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    inline static ModuleDescriptor CreateTurbulence(string _name, string _src1) {
        ModuleDescriptor _m;
        _m.setFreq(1.50 + 7.0*SSGX::floatRand());
        _m.setPow(0.02 + 0.5*SSGX::floatRand());
        _m.setRough(1.0 + 4.0*SSGX::floatRand());
        _m.setName(_name);
        _m.setSrc1(_src1);
        _m.setModuleType("Turbulence");
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }

    inline static ModuleDescriptor CreateTurbulence(string _name, string _src1, double freq, double pow, double rough) {
        ModuleDescriptor _m;
        _m.setFreq(freq);
        _m.setPow(pow);
        _m.setRough(rough);
        _m.setName(_name);
        _m.setSrc1(_src1);
        _m.setModuleType("Turbulence");
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }    

    inline static ModuleDescriptor CreateExp(string _name, string _src1, double d_exp = 1.25) 
    {
        ModuleDescriptor _m;
        _m.setModuleType("Exponent");
        _m.setExp(d_exp);
        _m.setName(_name);
        _m.setSrc1(_src1);
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }

    //    if (_m_moduleType=="Turbulence2")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    inline static ModuleDescriptor CreateTurbulence2(string _name, string _src1) {
        ModuleDescriptor _m;
        _m._freq = 1.50 + 7.0*SSGX::floatRand();
        _m._pow  = 0.02 + 0.4*SSGX::floatRand();
        _m._rough = 1.0 + 4.0*SSGX::floatRand();
        _m._name = _name;
        _m._src1 = _src1;
        _m.setModuleType("Turbulence2");
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }

    inline static ModuleDescriptor CreateTurbulenceBillow(string _name, string _src1) {
        ModuleDescriptor _m;
        _m._freq = 1.50 + 7.0*SSGX::floatRand();
        _m._pow  = 0.02 + 0.4*SSGX::floatRand();
        _m._rough = 1.0 + 4.0*SSGX::floatRand();
        _m._name = _name;
        _m._src1 = _src1;
        _m.setModuleType("TurbulenceBillow");
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }

    inline static ModuleDescriptor CreateTurbulenceRidged(string _name, string _src1) {
        ModuleDescriptor _m;
        _m._freq = 1.50 + 7.0*SSGX::floatRand();
        _m._pow  = 0.02 + 0.4*SSGX::floatRand();
        _m._rough = 1.0 + 4.0*SSGX::floatRand();
        _m._name = _name;
        _m._src1 = _src1;
        _m.setModuleType("TurbulenceRidged");
        cerr << "Created TurbulenceRidged linked to " << _m._src1 << endl;
        _m.setupPropertiesToExport(_m._moduleType);
        return _m;
    }

    //    if (_m_moduleType=="TurbulenceBillow")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="TurbulenceRidged")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    //    if (_m_moduleType=="Voronoi") appendToProperties( { "name" , "freq" , "displ" , "seed" , "enableDist" , "enableRandom"} );

    static shared_ptr<ModuleDescriptor> newAbs (string name, 
        shared_ptr<ModuleDescriptor>  src1, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Abs");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newAdd (string name, 
        shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Add");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newAvg (string name, 
        shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Avg");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newAvg4 (string name, 
        shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2,  
        shared_ptr<ModuleDescriptor>  src3, 
        shared_ptr<ModuleDescriptor>  src4, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Avg4");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setSrc3(src3->name());
        md->setSrc4(src4->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newBillow (string name, int seed,  double freq , double lac , double pers ,  int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Billow");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setPers(pers);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newBlend (string name, shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2, 
        shared_ptr<ModuleDescriptor>  ctl, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Blend");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setCtl(ctl->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newClamp (string name, shared_ptr<ModuleDescriptor>  src1, double uBound, double lBound, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Clamp");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setUbound(uBound);
        md->setLbound(lBound);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newConst (string name, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Const");
        md->setName(name);
        md->setValue(value);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newCos (string name, shared_ptr<ModuleDescriptor>  src1, double freq, double exp, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cos");
        md->setName(name);
        md->setValue(value);
        md->setSrc1(src1->name());
        md->setFreq(freq);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newCurve (string name, shared_ptr<ModuleDescriptor>  src1, std::vector<std::tuple<double,double>>& cPoints,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Curve");
        md->setName(name);
        md->setSrc1(src1->name());
        md->_cPoints.clear();
        for (auto& cp : cPoints) {
            md->_cPoints.push_back(cp);
        }
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newCylinders (string name, double freq, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cylinders");
        md->setName(name);
        md->setFreq(freq);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newDisplace (string name, shared_ptr<ModuleDescriptor>  src1, 
        shared_ptr<ModuleDescriptor>  src2,  
        shared_ptr<ModuleDescriptor>  src3, 
        shared_ptr<ModuleDescriptor> src4, 
        shared_ptr<ModuleDescriptor>  ctl, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Displace");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setSrc3(src3->name());
        md->setSrc4(src4->name());
        md->setCtl(ctl->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newExponent (string name, shared_ptr<ModuleDescriptor>  src1, double exp, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Exponent");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newInvert (string name, shared_ptr<ModuleDescriptor>  src1, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Invert");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newMax (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Max");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newMin (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Min");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newMultiply (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Multiply");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newPower (string name, shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Power");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newPerlin (string name, int seed,  double freq , double lac , double pers , 
        int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Perlin");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setPers(pers);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newRidgedMulti (string name, int seed,  double freq , double lac ,  
        int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RidgedMulti");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newRidgedMulti2 (string name, int seed,  double freq , double lac ,  
        double gain, double exp, int oct,double offset, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RidgedMulti2");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setOct(oct);
        md->setExp(exp);
        md->setGain(gain);    
        md->setOffset(offset) ;  
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newRotatePoint (string name, shared_ptr<ModuleDescriptor>  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RotatePoint");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newScaleBias (string name, shared_ptr<ModuleDescriptor>  src1, 
        double scale, double bias, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("ScaleBias");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setScale(scale);
        md->setBias(bias);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newScalePoint (string name, shared_ptr<ModuleDescriptor>  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("ScalePoint");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newSelect (string name, 
        shared_ptr<ModuleDescriptor>  src1, shared_ptr<ModuleDescriptor>  src2, 
        shared_ptr<ModuleDescriptor>  ctl, double uBound, double lBound, double value, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Select");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSrc2(src2->name());
        md->setCtl(ctl->name());
        md->setUbound(uBound);
        md->setLbound(lBound);
        md->setValue(value);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newSin (string name, shared_ptr<ModuleDescriptor>  src1, double freq, double exp, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Sin");
        md->setName(name);
        md->setValue(value);
        md->setSrc1(src1->name());
        md->setFreq(freq);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newSpheres (string name,  double freq, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Spheres");
        md->setName(name);
        md->setFreq(freq);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newTranslatePoint (string name, shared_ptr<ModuleDescriptor>  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TranslatePoint");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newTurbulence (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Turbulence");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor> newTurbulence2 (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Turbulence2");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor> newTurbulenceBillow (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TurbulenceBillow");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor> newTurbulenceRidged (string name, shared_ptr<ModuleDescriptor>  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TurbulenceRidged");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }    

    static shared_ptr<ModuleDescriptor> newVoronoi (string name, 
        int seed , double freq , double displ , bool enableDispl , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Voronoi");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setDispl(displ);
        md->setEnabledist(enableDispl);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }    
    static shared_ptr<ModuleDescriptor> newTerrace (string name, shared_ptr<ModuleDescriptor>  src1, 
        std::vector<std::tuple<double,double>>& cPoints, bool invert, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Terrace");
        md->setName(name);
        md->setSrc1(src1->name());
        md->_cPoints.clear();
        for (auto& cp : cPoints) {
            md->_cPoints.push_back(cp);
        }
        md->setInvert(invert);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor> newCache (string name, shared_ptr<ModuleDescriptor>  src1 ) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cache");
        md->setName(name);
        md->setSrc1(src1->name());
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    //////////////////////////////////////////////////////////


    static shared_ptr<ModuleDescriptor> newAbsName (string name, 
        string src1, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Abs");
        md->setName(name);
        md->setSrc1(src1);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newAddName (string name, 
        string src1, 
        string src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Add");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newAvgName (string name, 
        string src1, 
        string src2, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Avg");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newAvg4Name (string name, 
        string  src1, 
        string  src2,  
        string  src3, 
        string  src4, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Avg4");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setSrc3(src3);
        md->setSrc4(src4);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newBillowName (string name, int seed,  double freq , double lac , double pers ,  int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Billow");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setPers(pers);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newBlendOldName (string name, 
        string src1, 
        string src2, 
        string ctl, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Blend");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setCtl(ctl);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newClampName (string name, 
        string src1, 
        double uBound, double lBound, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Clamp");
        md->setName(name);
        md->setSrc1(src1);
        md->setUbound(uBound);
        md->setLbound(lBound);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newConstName (string name, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Const");
        md->setName(name);
        md->setValue(value);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newCosName (string name, 
        string src1, double freq, double exp, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cos");
        md->setName(name);
        md->setValue(value);
        md->setSrc1(src1);
        md->setFreq(freq);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newCurveName (string name, 
        string src1, std::vector<std::tuple<double,double>>& cPoints,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Curve");
        md->setName(name);
        md->setSrc1(src1);
        md->_cPoints.clear();
        for (auto& cp : cPoints) {
            md->_cPoints.push_back(cp);
        }
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newCylindersName (string name, double freq, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cylinders");
        md->setName(name);
        md->setFreq(freq);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor> newDisplaceName (string name, 
        string src1, 
        string src2,  
        string src3, 
        string src4, 
        string ctl, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Displace");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setSrc3(src3);
        md->setSrc4(src4);
        md->setCtl(ctl);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newExponentName (string name, string  src1, double exp, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Exponent");
        md->setName(name);
        md->setSrc1(src1);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newInvertName (string name, string  src1, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Invert");
        md->setName(name);
        md->setSrc1(src1);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newMaxName (string name, string  src1, string  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Max");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newMinName (string name, string  src1, string  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Min");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newMultiplyName (string name, string  src1, string  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Multiply");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newPowerName (string name, string  src1, string  src2, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Power");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newPerlinName (string name, int seed,  double freq , double lac , double pers , 
        int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Perlin");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setPers(pers);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newRidgedMultiName (string name, int seed,  double freq , double lac ,  
        int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RidgedMulti");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setOct(oct);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newRidgedMulti2Name (string name, int seed,  double freq , double lac ,  
        double gain, double exp, int oct  ,  bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RidgedMulti2");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setLac(lac);
        md->setOct(oct);
        md->setExp(exp);
        md->setGain(gain);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newRotatePointName (string name, string  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("RotatePoint");
        md->setName(name);
        md->setSrc1(src1);
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newScaleBiasName(string name, string  src1, 
        double scale, double bias, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("ScaleBias");
        md->setName(name);
        md->setSrc1(src1);
        md->setScale(scale);
        md->setBias(bias);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newScalePointName (string name, string  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("ScalePoint");
        md->setName(name);
        md->setSrc1(src1);
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newSelectName (string name, 
        string  src1, string  src2, 
        string  ctl, double uBound, double lBound, double value, 
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Select");
        md->setName(name);
        md->setSrc1(src1);
        md->setSrc2(src2);
        md->setCtl(ctl);
        md->setUbound(uBound);
        md->setLbound(lBound);
        md->setValue(value);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newSinName (string name, string  src1, double freq, double exp, double value,
        bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Sin");
        md->setName(name);
        md->setValue(value);
        md->setSrc1(src1);
        md->setFreq(freq);
        md->setExp(exp);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newSpheresName (string name,  double freq, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Spheres");
        md->setName(name);
        md->setFreq(freq);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newTranslatePointName (string name, string  src1, 
        double x, double y, double z, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TranslatePoint");
        md->setName(name);
        md->setSrc1(src1);
        md->setX(x);
        md->setY(y);
        md->setZ(z);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newTurbulenceName (string name, string  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Turbulence");
        md->setName(name);
        md->setSrc1(src1);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor>  newTurbulence2Name (string name, string  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Turbulence2");
        md->setName(name);
        md->setSrc1(src1);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor>  newTurbulenceBillowName (string name, string  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TurbulenceBillow");
        md->setName(name);
        md->setSrc1(src1);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }

    static shared_ptr<ModuleDescriptor>  newTurbulenceRidgedName (string name, string  src1, 
        int seed , double freq , double pow , double rough , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("TurbulenceRidged");
        md->setName(name);
        md->setSrc1(src1);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setPow(pow);
        md->setRough(rough);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }    

    static shared_ptr<ModuleDescriptor>  newVoronoiName (string name, 
        int seed , double freq , double displ , bool enableDispl , bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Voronoi");
        md->setName(name);
        md->setSeed(seed);
        md->setFreq(freq);
        md->setDispl(displ);
        md->setEnabledist(enableDispl);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }    
    static shared_ptr<ModuleDescriptor>  newTerraceName (string name, string  src1, 
        std::vector<std::tuple<double,double>>& cPoints, bool invert, bool enableRandom = false) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Terrace");
        md->setName(name);
        md->setSrc1(src1);
        md->_cPoints.clear();
        for (auto& cp : cPoints) {
            md->_cPoints.push_back(cp);
        }
        md->setInvert(invert);
        md->setEnableRandom(enableRandom);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
    static shared_ptr<ModuleDescriptor>  newCacheName (string name, string  src1) {
        auto  md = make_shared<ModuleDescriptor>();
        md->setModuleType("Cache");
        md->setSrc1(src1);
        md->setupPropertiesToExport(md->moduleType());
        return md;
    }
};




#endif // MODULEDESCRIPTOR_H
