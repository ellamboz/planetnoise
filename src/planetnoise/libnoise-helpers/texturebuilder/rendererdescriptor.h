/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#ifndef RENDERERDESCRIPTOR_H
#define RENDERERDESCRIPTOR_H

#include <tuple>
#include <noiseutils.h>
#include <ssg_structures.h>
#include "libnoise-helpers_global.h"
#include <memory>
#include <vector>
#include <map>
#include <json.hpp>
#include "imagedescriptor.h"
#include "heightmapdescriptor.h"

using namespace utils;
using namespace std;
using namespace nlohmann;

//position, r, g, b, a
typedef std::tuple<double,int,int,int,int> GradientInfo;
typedef std::tuple<int, utils::Color> RandomGradientRow;


class LIBNOISEHELPERSSHARED_EXPORT RendererDescriptor
{

    std::string _name = "renderer0001";
    std::string _heightMap = "heightmap1";
    std::string _bumpMap = "";
    std::string _backgroundImage = "";
    std::string _alphaImage = "";
    std::string _destImage = "";
    bool _enabledLight = false;
    double _lightContrast = 1.0;
    double _lightBrightness = 1.0;
    bool _randomGradient = false;

    int _rndHue        = 0;
    int _rndSaturation = 0;
    int _rndValue      = 0;
    bool applyRandomFactor() { return _rndHue != 0 || _rndSaturation != 0 || _rndValue != 0; }


    vector<GradientInfo> _gradientInfo;
    map<std::string, shared_ptr<utils::NoiseMap>> _noiseMaps;
    map<std::string, shared_ptr<utils::Image>> _images;
    map<std::string, shared_ptr<utils::RendererImage>> _renderers;

public:
    RendererDescriptor();
    static std::shared_ptr<RendererDescriptor> create (
        const string& name, 
        std::shared_ptr<HeightMapDescriptor> heightMap , 
        vector<GradientInfo>& gradientInfo, 
        std::shared_ptr<ImageDescriptor> destImage,
        std::shared_ptr<ImageDescriptor> backgroundImage = nullptr, 
        std::shared_ptr<ImageDescriptor> alphaImage = nullptr,
        std::shared_ptr<HeightMapDescriptor> bumpMap = nullptr,
        double lightBrightness = 2.0,
        double lightContrast = 1.5,
        bool enabledLight = false, 
        bool randomGradient = false
    ) {
        auto p = make_shared<RendererDescriptor>();
        p->setName(name);
        p->setHeightmap(heightMap->name());
        p->setDestImage(destImage->name());
        p->replaceGradient(gradientInfo);
        if (backgroundImage != nullptr) p->setBackgroundImage(backgroundImage->name());
        if (bumpMap != nullptr) p->setBumpMap(bumpMap->name());
        if (alphaImage != nullptr) p->setAlphaImage(alphaImage->name());
        p->setLightbrightness(lightBrightness);
        p->setLightcontrast(lightContrast);
        p->setEnabledlight(enabledLight);
        p->setRandomGradient(randomGradient);
        
        return p;
    }

    /*
    static std::shared_ptr<RendererDescriptor> create (
        const string& name, 
        string heightMap, 
        vector<GradientInfo>& gradientInfo, 
        const string&string destImage,
        const string&string backgroundImage = "", 
        const string&string alphaImage = "",
        const string&string bumpMap = "",
        double lightBrightness = 2.0,
        double lightContrast = 1.5,
        bool enabledLight = false, 
        bool randomGradient = false        
    ) {
        auto p = make_shared<RendererDescriptor>();
        p->setName(name);
        p->setHeightmap(heightMap);
        p->setDestImage(destImage);
        p->replaceGradient(gradientInfo);
        p->setBackgroundImage(backgroundImage);
        p->setBumpMap(bumpMap);
        p->setAlphaImage(alphaImage);
        p->setLightbrightness(lightBrightness);
        p->setLightcontrast(lightContrast);
        p->setEnabledlight(enabledLight);
        p->setRandomGradient(randomGradient);
        return p;
    }    
    */

    std::string luaDeclaration() { return _name + "=RendererImage.new()"; }
    std::string luaInitialization() ;

    std::string& name() { return _name; }
    std::string& destImage() { return _destImage; }
    std::string& backgroundImage() { return _backgroundImage; }
    std::string& alphaImage() { return _alphaImage; }
    std::string& heightMap() { return _heightMap; }
    std::string& bumpMap() { return _bumpMap; }
    bool enabledLight() { return _enabledLight; }
    double lightContrast() { return _lightContrast; }
    double lightBrightness() { return _lightBrightness; }
    bool randomGradient() { return _randomGradient; }
    int rndHue() { return _rndHue; }
    int rndSaturation() { return _rndSaturation; }
    int rndValue() { return _rndValue; }
    void setRandomFactor (int h, int s, int v )  { _rndHue = h; _rndSaturation = s; _rndValue = v; }


    const map<std::string, shared_ptr<utils::NoiseMap>>& noiseMaps() { return _noiseMaps; }
    const map<std::string, shared_ptr<utils::Image>>& images() { return _images; }
    const map<std::string, shared_ptr<utils::RendererImage>>& renderers() { return _renderers; }

    void setName(const std::string& v) { _name = v ; }
    void setDestImage(const std::string& v) { _destImage = v ; }
    void setBackgroundImage(const std::string& v) { _backgroundImage = v ; }
    void setAlphaImage(const std::string& v) { _alphaImage = v ; }
    void setHeightmap(const std::string& v) { _heightMap = v ; }
    void setBumpMap(const std::string& v) { _bumpMap = v ; }
    void setEnabledlight(bool v) { _enabledLight = v ; }
    void setLightcontrast(double v) { _lightContrast = v ; }
    void setLightbrightness(double v) { _lightBrightness = v ; }
    void setRandomGradient (bool b ) { _randomGradient = b; }

    void setNoiseMaps(const map<std::string, shared_ptr<utils::NoiseMap>>& v) { _noiseMaps = v; }
    void setImages(const map<std::string, shared_ptr<utils::Image>>& v) { _images = v; }
    void setRenderers(const map<std::string, shared_ptr<utils::RendererImage>>& v) { _renderers = v; }

    shared_ptr<utils::RendererImage> makeRenderer();

    RendererDescriptor& connectImagesAndMap();

    #ifdef JSON
    void toJson(QJsonObject& json);
    void fromJson(const QJsonObject& json);
    #else
    void toJson(json& j);
    void fromJson(const json& j);

    #endif

    vector<GradientInfo>& gradientInfo() { return _gradientInfo; }
    void clearGradientInfo() { _gradientInfo.clear(); }
    void addGradientInfo(GradientInfo g) { _gradientInfo.push_back(g); }
    void replaceGradient(std::vector<GradientInfo>& newGradient) {
        std::cerr << "Gradient size is " << newGradient.size() << std::endl;
        _gradientInfo.clear();
        for (const auto& gi : newGradient) {
            _gradientInfo.push_back(gi);
        }
    }
    void eraseGradientInfo(int i) { _gradientInfo.erase(_gradientInfo.begin() + i); }

    #ifdef JSON
    void readGradientInfo(const QJsonObject& json);
    #else
    void readGradientInfo(const json& j);
    #endif // JSON

    void randomizeGradientInfo();

    void randomPositionFactor(double rndPos = 0.0);
};

#endif // RENDERERDESCRIPTOR_H
