/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#ifndef IMAGEDESCRIPTOR_H
#define IMAGEDESCRIPTOR_H

#include <tuple>
#include <noiseutils.h>
#include <ssg_structures.h>
#include "libnoise-helpers_global.h"
#include <memory>

using namespace std;


class LIBNOISEHELPERSSHARED_EXPORT ImageDescriptor
{
    std::string _name = "image1";
public:
    std::string& name() { return _name; }
    void setName(const std::string& v) { _name = v; }
    ImageDescriptor();

    std::string pythonDeclaration() {
        return _name + "=Image.new()\n" + _name +"Bmp=WriterBMP.new()";
    }

    static std::shared_ptr<ImageDescriptor> create(std::string name) {
        auto p = std::make_shared<ImageDescriptor>();
        p->setName(name);
        return p;
    }

    //from descriptor to actual object
    shared_ptr<utils::Image> makeImage() {
        return make_shared<utils::Image>();
    }
};

#endif // IMAGEDESCRIPTOR_H
