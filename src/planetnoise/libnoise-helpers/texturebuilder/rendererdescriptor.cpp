/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#include "rendererdescriptor.h"

using namespace std;
using namespace nlohmann;

RendererDescriptor::RendererDescriptor()
{

}

std::string RendererDescriptor::luaInitialization() {
    std::string res="";
    res = res + _name + ":ClearGradient()\n" +
            _name + ":SetDestImage(" + _destImage + ")\n" +
            _name + ":SetSourceNoiseMap(" + _heightMap + ")\n" +
            _name + ":SetBackgroundColor(Color.new(0,0,0,0))\n";
    if (_bumpMap != "") {
        res = res + _name + ":SetBumpNoiseMap(" + _bumpMap + ")\n";
    }
    if (_backgroundImage != "") {
        res = res + _name + ":SetBackgroundImage(" + _backgroundImage + ")\n";
    }
    if (_alphaImage != "") {
        res = res + _name + ":SetAlphaImage(" + _alphaImage + ")\n";
    }
    if (_enabledLight) {
        res = res + _name + ":EnableLight()\n"+
                _name + ":SetLightContrast(" + std::to_string(_lightContrast)+")\n" +
                _name + ":SetLightBrightness(" + std::to_string(_lightBrightness)+")\n";
    }
    // control points
    // TODO: fix on a later date
    /*
    for (auto i = _gradientInfo.begin(); i != _gradientInfo.end(); ++i) {
        res = res + _name+ std::string(":AddGradientPoint(%1, Color.new(%2,%3,%4,%5))\n")
                .arg(std::get<0>(*i))
                .arg(std::get<1>(*i))
                .arg(std::get<2>(*i))
                .arg(std::get<3>(*i))
                .arg(std::get<4>(*i));
    }
    */
    res = res + _name + ":Render()\n\n";
    return res;
}

shared_ptr<utils::RendererImage> RendererDescriptor::makeRenderer() {
    utils::RendererImage* p = new utils::RendererImage();

    p->SetBackgroundColor(utils::Color(0,0,0,0)); //transparency, quoi
    p->EnableLight(this->_enabledLight);
    p->SetLightBrightness(this->lightBrightness());
    p->SetLightContrast(this->lightContrast());

    //and now, gradient info
    p->ClearGradient();
    for (auto gi : _gradientInfo) {
        p->AddGradientPoint(std::get<0>(gi), utils::Color(std::get<1>(gi),std::get<2>(gi),std::get<3>(gi),std::get<4>(gi)));
    }

    shared_ptr<utils::RendererImage> sp; sp.reset(p);
    return sp;
}

RendererDescriptor& RendererDescriptor::connectImagesAndMap() {
    utils::RendererImage* p = _renderers[this->name()].get();
    if (_backgroundImage != "") {
        if (!(_images.count(_backgroundImage) != 0) )
            throw "Renderer " + _name + " is referencing background image " +
                _backgroundImage + ", which is not defined in images sections";
        auto pImg = _images[_backgroundImage].get(); //pointer to utils::Image
        p->SetBackgroundImage(*pImg);                 //reference to stored data
    }
    if (_alphaImage != "") {
        if (!(_images.count(_alphaImage) != 0 ))
            throw "Renderer " + _name + " is referencing alpha image " +
                _alphaImage + ", which is not defined in images sections";
        auto pImg = _images[_alphaImage].get(); //pointer to utils::Image
        p->SetAlphaImage(*pImg);                 //reference to stored data
    }
    if (_destImage != "") {
        if (!(_images.count(_destImage) != 0 ))
            throw "Renderer " + _name + " is referencing destination image " +
                _backgroundImage + ", which is not defined in images sections";
        auto pImg = _images[_destImage].get(); //pointer to utils::Image
        p->SetDestImage(*pImg);                 //reference to stored data
    }
    else
        throw "Undefined destination image in " + _name;
    auto theMap = _noiseMaps[this->_heightMap].get();
    //qDebug() << "x: " << theMap->GetWidth() << ", y: " <<  theMap->GetHeight();
    p->SetSourceNoiseMap(*theMap);
    if (_noiseMaps.find(this->_bumpMap) != _noiseMaps.end() ) {
        auto bmpMap = _noiseMaps[this->_bumpMap].get();
        p->SetBumpNoiseMap(*bmpMap);
    }
    return *this;
}

void RendererDescriptor::toJson(json& j) {

    j["name"] = _name;
    j["heightMap"] = _heightMap;
    j["bumpMap"] = _bumpMap;
    j["enabledLight"] = _enabledLight;
    j["lightContrast"] = _lightContrast;
    j["lightBrightness"] = _lightBrightness;
    j["backgroundImage"] = _backgroundImage;
    j["alphaImage"] = _alphaImage;
    j["destImage"] = _destImage;
    j["randomGradient"] = _randomGradient;
    if (_rndHue != 0 && _rndSaturation != 0 && _rndValue != 0) {
        auto a = json::array();
        a.push_back(_rndHue);
        a.push_back (_rndSaturation);
        a.push_back(_rndValue);
        j["randomFactor"] = a;
    }
    auto giItems = json::array();
    for (auto& gi :  _gradientInfo) {
        auto a = json::array();
        a.push_back(std::get<0>(gi));
        a.push_back(std::get<1>(gi));
        a.push_back(std::get<2>(gi));
        a.push_back(std::get<3>(gi));
        a.push_back(std::get<4>(gi));
        giItems.push_back(a);
    }
    j["gradientInfo"] = giItems;
}

void RendererDescriptor::readGradientInfo(const json& j)
{
    auto gi = j["gradientInfo"];
    _gradientInfo.clear();
    bool mustApplyRandomFactor = this->applyRandomFactor();
   // qDebug() << "Color array: " << gi.size();
    for (auto h = 0; h < gi.size(); h++) {
        auto i = gi[h];
        if (mustApplyRandomFactor) {
            utils::Color c(i[1].get<int>(),i[2].get<int>(),i[3].get<int>(),i[4].get<int>()) ;
            c = c.change(_rndHue, _rndSaturation, _rndValue);
            auto grad = GradientInfo(i[0].get<double>(),
                                    static_cast<int>(c.red),
                                     static_cast<int>(c.green),
                                     static_cast<int>(c.blue),
                                     static_cast<int>(c.alpha));
            _gradientInfo.push_back(grad);
        } else {
            auto grad = GradientInfo(i[0].get<double>(),
                i[1].get<int>(),i[2].get<int>(),i[3].get<int>(),i[4].get<int>());
            _gradientInfo.push_back(grad);
        }
    }
}

void RendererDescriptor::randomizeGradientInfo()
{
    //double dRes = -1.0;
    //auto startingColor = utils::Color::RandomColor();
    //startingColor.alpha = 255;
    //bool direction = SSGX::d10() % 2 == 0;
    utils::GradientColor rndGradient = utils::GradientColor::CreateRandomGradient();
    std::vector<utils::GradientPoint> points = rndGradient.GetGradientPoints();

    for (utils::GradientPoint grad : points ) {
        auto gInfo = GradientInfo (
                    grad.pos,
                    (int)grad.color.red,
                    (int)grad.color.green,
                    (int)grad.color.blue,
                    (int)grad.color.alpha
                    );
        _gradientInfo.push_back(gInfo);
    }

}

void RendererDescriptor::randomPositionFactor(double rndPos) {

    if (rndPos != 0.0) {
        double rndFactor = rndPos;
        if (rndPos < 0.0) rndFactor = 0.01;
        if (rndPos > 1.0) rndFactor = 0.9;
        for (GradientInfo& grad : _gradientInfo) {
            double pos = 1.0+std::get<0>(grad);
            double d = rndFactor * pos;
            double dVal = Rand256::instance().doubleValue(d*2.0) - d;
            double pos2 = pos +dVal;
            if (pos2 < 0.0) pos2 = 0.0;
            if (pos2 > 2.0) pos2 = 2.0 - Rand256::instance().doubleValue(0.1);
            std::get<0>(grad) = (pos2 - 1.0);
        }
    }
}

void RendererDescriptor::fromJson(const json& j) {
    if (j.find("randomGradient") != j.end() &&  !j["randomGradient"].is_null() && j["randomGradient"].is_boolean())
        _randomGradient = j["randomGradient"].get<bool>();
    if (j.find("randomFactor") != j.end() && !j["randomFactor"].is_null() && j["randomFactor"].is_array())
    {
        auto jArray = j["randomFactor"];
        //if (jArray.count() != 3)
        //    throw noise::ExceptionInvalidParam;
        //hue, sat, v;
        _rndHue = jArray[0].get<int>() % 255;
        _rndSaturation = jArray[1].get<int>() % 255;
        _rndValue = jArray[2].get<int>() % 255;

    }

    if (j.find("destImage") != j.end() && !j["destImage"].is_null() && j["destImage"].is_string())
        _destImage = j["destImage"].get<string>();
    if (j.find("backgroundImage") != j.end() && !j["backgroundImage"].is_null() && j["backgroundImage"].is_string())
        _backgroundImage = j["backgroundImage"].get<string>();
    if (j.find("alphaImage") != j.end() && !j["alphaImage"].is_null() && j["alphaImage"].is_string())
        _alphaImage = j["alphaImage"].get<string>();
    if (j.find("name") != j.end() && !j["name"].is_null() && j["name"].is_string())
        _name = j["name"].get<string>();;
    if (j.find("noiseMap") != j.end() && !j["noiseMap"].is_null() && j["noiseMap"].is_string())
        _heightMap= j["noiseMap"].get<string>();;
    if (j.find("bumpMap") != j.end() && !j["bumpMap"].is_null() && j["bumpMap"].is_string())
        _bumpMap= j["bumpMap"].get<string>();
    if (j.find("heightMap") != j.end() && !j["heightMap"].is_null() && j["heightMap"].is_string())
        _heightMap= j["heightMap"].get<string>();;
    if (j.find("enabledLight") != j.end() && !j["enabledLight"].is_null() && j["enabledLight"].is_boolean())
        _enabledLight = j["enabledLight"].get<bool>();
    if (j.find("lightContrast") != j.end() && !j["lightContrast"].is_null() && j["lightContrast"].is_number_float())
        _lightContrast = j["lightContrast"].get<double>();;
    if (j.find("lightBrightness") != j.end() && !j["lightBrightness"].is_null() && j["lightBrightness"].is_number_float())
        _lightBrightness = j["lightBrightness"].get<double>();
    if (j.find("gradientInfo") != j.end() && !j["gradientInfo"].is_null()  && !_randomGradient) {
        readGradientInfo(j);
    }
    else {
        randomizeGradientInfo();

    }

}
