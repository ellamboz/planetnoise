/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#include "texturebuilder.h"
#include <memory>
#include <qcolorops.h>
#include "ssg_structures.h"
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "textureworkflowcreator.h"
#include <iostream>

using namespace nlohmann;
using namespace std;

///
/// \brief TextureBuilder::TextureBuilder
///
/// Creates a TextureBuilder object and initializes it with a random, three-layered texture
/// that can be uses as a starting point for some weird planet :-)
void TextureBuilder::initialize(bool earthlike, string texture1, string texture2, double sealevel, double icelevel) 
{
    cerr << "Randomizing workflow...";
    _randomFactors.push_back(0.0);

    TextureWorkflowCreator tfc (earthlike, texture1, texture2);
    tfc.setTextureBuilder(this);
    tfc.initializeRandomFactors();
    tfc.createTextureWorkflow("BeginLayer",true,"","BaseImage",false,false,texture1);
    tfc.createTextureWorkflow("MiddleLayer",false,"BaseImage","BaseImage", false,false,texture2);
    //if (true) {
    if (earthlike) {
        cerr << "---------------------\nCreating seas and pole...\n---------------------\n" << endl;
        int cntExtra = (SSGX::d10() -3) / 2;
        if (cntExtra > 0) {
            for (auto ci = 0; ci < cntExtra; ci ++) {
                string sLabel = "ExtraLayer_"+ std::to_string(ci);
                tfc.createTextureWorkflow(sLabel, false,"BaseImage","BaseImage",false,false, ci % 2 == 0 ? texture1 : texture2 );
            }
        }

        tfc.prepareBumpHeightmap();
        tfc.createSnowFromBumpmap("BaseImage");

        if (SSGX::d100() < 75) 
            tfc.createGrassAndDesert("BaseImage");

        std::cerr << "Rendered snow " << std::endl;
        auto dstep = tfc.createSeaWorkflow( "BaseImage", sealevel,0.09);
        tfc.createPoleWorkflow("BaseImage",icelevel,0.03);
        tfc.createBumpMapSea(dstep);
        tfc.createSpecMap("MiddleLayer_Module", sealevel);
        tfc.createGeoMap("MiddleLayer_Module", sealevel);
    } else {
        tfc.createTextureWorkflow("PenLayer",false,"BaseImage","BaseImage",false,false,texture1);
        int cntExtra = (SSGX::d10() -2) / 2;
        if (cntExtra > 0) {
            for (auto ci = 0; ci < cntExtra; ci ++) {
                string sLabel = "ExtraLayer_"+ std::to_string(ci);
                tfc.createTextureWorkflow(sLabel, false,"BaseImage","BaseImage",false,false, ci % 2 == 0 ? texture1 : texture2 );
            }
        }

         tfc.createTextureWorkflow("EndLayer",false,"BaseImage","BaseImage",true,false,texture2);
        int mdType = SSGX::d6();
        int quad = SSGX::d6();
        if (quad < -1) {
            switch (mdType) {
                case 0:
                    tfc.createBumpMap("BeginLayer_Module","MiddleLayer_Module");
                    break;
                case 1:
                    tfc.createBumpMap("BeginLayer_Module","PenLayer_Module");
                    break;
                case 2:
                    tfc.createBumpMap("BeginLayer_Module","EndLayer_Module");
                    break;
                case 3:
                    tfc.createBumpMap("EndLayer_Module","MiddleLayer_Module");
                    break;
                case 4:
                    tfc.createBumpMap("PenLayer_Module","MiddleLayer_Module");
                    break;
                case 5:
                    tfc.createBumpMap("EndLayer_Module","PenLayer_Module");
                    break;
                default:
                    tfc.createBumpMap("BeginLayer_Module","MiddleLayer_Module");
                    break;
                }
            } else {
                tfc.createBumpMap4("BeginLayer_Module","MiddleLayer_Module","PenLayer_Module","EndLayer_Module");
            }

        //tfc.createBumpMap("BeginLayer_Module","MiddleLayer_Module");
        tfc.createSpecMap("EndLayer_Module");
        if (SSGX::d100() < 40) {
            tfc.createPoleWorkflow("BaseImage");
        }
    }
    this->_colorMap = "BaseImage";
    if (!earthlike) {
        this->_bumpMap = "img_bump";
        this->_reflectionMap = "img_spec";
    } else {
        this->_bumpMap = "img_spec";
        this->_reflectionMap = "img_bump";
    }
}

#ifdef JSON
void TextureBuilder::fromJson(const QJsonObject &json) {

    if (!j["colorMap"].isNull() && !j["colorMap"].isUndefined())
        _colorMap = j["colorMap"].toString();
    if (!j["cloudMap"].isNull() && !j["cloudMap"].isUndefined())
        _cloudMap = j["cloudMap"].toString();
    if (!j["reflectionMap"].isNull() && !j["reflectionMap"].isUndefined())
        _reflectionMap = j["reflectionMap"].toString();
    if (!j["bumpMap"].isNull() && !j["bumpMap"].isUndefined())
        _bumpMap = j["bumpMap"].toString();

    if (!j["builderType"].isNull() && !j["builderType"].isUndefined()) {
        auto sType = j["builderType"].toString();
        if (sType.toLower() != "sphere" && sType.toLower() != "plane" && sType.toLower() != "cylinder")
            _builderType = "sphere";
        else
            _builderType = sType;
    }

    if (!j["size"].isNull() && !j["size"].isUndefined()) {
        auto a = j["size"].toArray();
        _size = std::tuple<int,int>(a[0].toInt(),a[1].toInt());
    }

    if (!j["bounds"].isNull() && !j["bounds"].isUndefined()) {
        auto a1 = j["bounds"].toArray();
        _bounds = std::tuple<double,double,double,double>(
            a1[0].toDouble(),
            a1[1].toDouble(),
            a1[2].toDouble(),
            a1[3].toDouble()
            );
    }

    if (!j["randomFactors"].isNull() && !j["randomFactors"].isUndefined()) {
        auto jsRf = j["randomFactors"];
        if (jsRf.isDouble()) {
            _randomFactors.clear();
            _randomFactors.append(jsRf.toDouble());
        }
        if (jsRf.isArray()) {
            _randomFactors.clear();
            QJsonArray arr = jsRf.toArray();
            for (auto h = 0; h < arr.size(); ++h) {
                _randomFactors.append(arr[h].toDouble());
            }
        }
    }


    QJsonArray aModules = j["modules"].toArray();
    QJsonArray aHeightMaps = j["heightMaps"].toArray();
    QJsonArray aHMBuilders = j["heightMapBuilders"].toArray();
    QJsonArray aRenderers = j["renderers"].toArray();
    QJsonArray aImages = j["images"].toArray();

    _modDesc.clear();
    _modules.clear();
    for (auto h = 0; h < aModules.size(); ++h) {
        ModuleDescriptor *m = new ModuleDescriptor();
        QJsonObject o = aModules[h].toObject();
        m->fromJson(o);

        string name = m->name();MiddleLayer
        m->setModules(_modules);
        shared_ptr<ModuleDescriptor> p; p.reset(m);
        _modDesc.insert(name,p);
    }

    _rndDesc.clear();
    _renderers.clear();
    _rndNames.clear();

    for (auto h = 0; h < aRenderers.size(); ++h) {
        RendererDescriptor *m = new RendererDescriptor();
        QJsonObject o = aRenderers[h].toObject();
        m->fromJson(o);
        string name = m->name();

        m->setImages(_images);
        m->setNoiseMaps(_heightMaps);
        m->setRenderers(_renderers);

        shared_ptr<RendererDescriptor> p; p.reset(m);
        _rndDesc.insert(name,p);
        _rndNames.append(name);
    }

    _nmbDesc.clear();
    _noiseMapBuilders.clear();
    for (auto h = 0; h < aHMBuilders.size(); ++h) {
        NoiseMapBuilderDescriptor *m = new NoiseMapBuilderDescriptor();
        QJsonObject o = aHMBuilders[h].toObject();

        m->fromJson(o);

        assignSizeInfoToNMBDesc(m);

        //m->setBuilderType(NoiseMapBuilderType::SPHERE);
        //m->setSize(std::get<0>(_size), std::get<1>(_size));
        //m->setBounds(std::get<0>(_bounds),std::get<1>(_bounds),std::get<2>(_bounds),std::get<3>(_bounds));

        m->setModules(_modules);

        string name = m->name();
        shared_ptr<NoiseMapBuilderDescriptor> p; p.reset(m);
        _nmbDesc.insert(name,p);
    }

    _hmDesc.clear();
    _heightMaps.clear();
    for (auto h = 0; h < aHeightMaps.size(); ++h) {
        HeightMapDescriptor *m = new HeightMapDescriptor();
        string o = aHeightMaps[h].toString();

        m->setName(o);

        shared_ptr<HeightMapDescriptor> p; p.reset(m);
        _hmDesc.insert(o,p);
    }

    _images.clear();
    __imDesc.clear();
    for (auto h = 0; h < aImages.size(); ++h) {
        ImageDescriptor *m = new ImageDescriptor();
        string o = aImages[h].toString();

        m->setName(o);
        if (h == 0 && _colorMap == "") _colorMap = o;
        if (h == 1 && _bumpMap == "") _bumpMap =  o;
//        if (h == 2 && _cloudMap == "") _cloudMap = o;
        if (h == 3 && _reflectionMap == "") _reflectionMap = o;

        shared_ptr<ImageDescriptor> p; p.reset(m);
        __imDesc.insert(o,p);
    }

    /*
    createAll();
    connectAll();
    */
}
#else

///
/// \brief TextureBuilder::fromJson
/// \param j
///
/// Populates a TextureBuilder instance with serialized json data
void TextureBuilder::fromJson(const json &j) {

    if (jsonDefinedNotNull(j, "colorMap"))
        _colorMap = j["colorMap"].get<string>();
    if (jsonDefinedNotNull(j, "cloudMap"))
        _cloudMap = j["cloudMap"].get<string>();
    if (jsonDefinedNotNull(j, "reflectionMap"))
        _reflectionMap = j["reflectionMap"].get<string>();
    if (jsonDefinedNotNull(j, "bumpMap"))
        _bumpMap = j["bumpMap"].get<string>();

    if (jsonDefinedNotNull(j, "builderType")) {
        string sType = j["builderType"].get<string>();
        auto sTypeLower = sType;
        std::transform(sTypeLower.begin(), sTypeLower.end(), sTypeLower.begin(), ::tolower);
        if (sTypeLower != "sphere" && sTypeLower != "plane" && sTypeLower != "cylinder")
            _builderType = "sphere";
        else
            _builderType = sType;
    }

    if (jsonDefinedNotNull(j, "size"))
    {
        auto a = j["size"];
        _size = std::tuple<int,int>(a[0].get<int>(),a[1].get<int>());
    }

    if (jsonDefinedNotNull(j, "bounds")) {
        auto a1 = j["bounds"];
        _bounds = std::tuple<double,double,double,double>(
            a1[0].get<double>(),
            a1[1].get<double>(),
            a1[2].get<double>(),
            a1[3].get<double>()
            );
    }

    if (jsonDefinedNotNull(j, "randomFactors")) {
        auto jsRf = j["randomFactors"];
        if (jsRf.is_number_float()) {
            _randomFactors.clear();
            _randomFactors.push_back(jsRf.get<double>());
        }
        if (jsRf.is_array()) {
            _randomFactors.clear();
            auto arr = jsRf;
            for (auto h = 0; h < static_cast<int>(arr.size()); ++h) {
                _randomFactors.push_back(arr[h].get<double>());
            }
        }
    }


    auto aModules = j["modules"];
    auto aHeightMaps = j["heightMaps"];
    auto aHMBuilders = j["heightMapBuilders"];
    auto aRenderers = j["renderers"];
    auto aImages = j["images"];

    _modDesc.clear();
    _modules.clear();
    for (auto h = 0; h < static_cast<int>(aModules.size()); ++h) {
        ModuleDescriptor *m = new ModuleDescriptor();
        json o = aModules[h];
        m->fromJson(o);

        string name = m->name();
        m->setModules(_modules);
        shared_ptr<ModuleDescriptor> p; p.reset(m);
        _modDesc.insert({name,p});
    }

    _rndDesc.clear();
    _renderers.clear();
    _rndNames.clear();

    for (auto h = 0; h < static_cast<int>(aRenderers.size()); ++h) {
        RendererDescriptor *m = new RendererDescriptor();
        json o = aRenderers[h];
        m->fromJson(o);
        string name = m->name();

        m->setImages(_images);
        m->setNoiseMaps(_heightMaps);
        m->setRenderers(_renderers);

        shared_ptr<RendererDescriptor> p; p.reset(m);
        _rndDesc.insert({name,p});
        _rndNames.push_back(name);
    }

    _nmbDesc.clear();
    _noiseMapBuilders.clear();
    for (auto h = 0; h < static_cast<int>(aHMBuilders.size()); ++h) {
        NoiseMapBuilderDescriptor *m = new NoiseMapBuilderDescriptor();
        json o = aHMBuilders[h];

        m->fromJson(o);

        assignSizeInfoToNMBDesc(m);

        //m->setBuilderType(NoiseMapBuilderType::SPHERE);
        //m->setSize(std::get<0>(_size), std::get<1>(_size));
        //m->setBounds(std::get<0>(_bounds),std::get<1>(_bounds),std::get<2>(_bounds),std::get<3>(_bounds));

        m->setModules(_modules);

        string name = m->name();
        shared_ptr<NoiseMapBuilderDescriptor> p; p.reset(m);
        _nmbDesc.insert({name,p});
    }

    _hmDesc.clear();
    _heightMaps.clear();
    for (auto h = 0; h < static_cast<int>(aHeightMaps.size()); ++h) {
        HeightMapDescriptor *m = new HeightMapDescriptor();
        string o = aHeightMaps[h].get<string>();

        m->setName(o);

        shared_ptr<HeightMapDescriptor> p; p.reset(m);
        _hmDesc.insert({o,p});
    }

    _images.clear();
    __imDesc.clear();
    for (auto h = 0; h < static_cast<int>(aImages.size()); ++h) {
        ImageDescriptor *m = new ImageDescriptor();
        string o = aImages[h].get<string>();

        m->setName(o);
        if (h == 0 && _colorMap == "") _colorMap = o;
        if (h == 1 && _bumpMap == "") _bumpMap =  o;
        if (h == 2 && _cloudMap == "") _cloudMap = o;
        if (h == 3 && _reflectionMap == "") _reflectionMap = o;

        shared_ptr<ImageDescriptor> p; p.reset(m);
        __imDesc.insert({o,p});
    }

    /*
    createAll();
    connectAll();
    */
}
#endif // JSON

#ifdef JSON
void TextureBuilder::toJson(QJsonObject &json) {
    j["colorMap"] = _colorMap;
    j["cloudMap"] = _cloudMap;
    j["reflectionMap"] = _reflectionMap;
    j["bumpMap"] = _bumpMap;

    j["builderType"] = _builderType;

    QJsonArray a;
    a.append(std::get<0>(_size));
    a.append(std::get<1>(_size));
    j["size"] = a;
    QJsonArray a1;
    a1.append(std::get<0>(_bounds));
    a1.append(std::get<1>(_bounds));
    a1.append(std::get<2>(_bounds));
    a1.append(std::get<3>(_bounds));
    j["bounds"] = a1;


    if (_randomFactors.count() > 0) {
        QJsonArray rf;
        for (auto it = _randomFactors.begin(); it != _randomFactors.end(); ++it) {
            auto w = (*it);
            rf.append(w);
        }
        j["randomFactors"] = rf;
    }
    QJsonArray aModules;
    for (auto it = _modDesc.begin(); it != _modDesc.end(); ++it) {
        QJsonObject o;
        it.value().get()->toJson(o);
        aModules.append(o);
    }
    j["modules"] = aModules;

    QJsonArray aHMBuilders;
    for (auto it = _nmbDesc.begin(); it != _nmbDesc.end(); ++it) {
        QJsonObject o;
        it.value().get()->toJson(o);
        aHMBuilders.append(o);
    }
    j["heightMapBuilders"] = aHMBuilders;

    QJsonArray aRenderers;
    //
    for (auto key = _rndNames.begin(); key != _rndNames.end(); ++key) {
        //for (auto it = _rndDesc.begin(); it != _rndDesc.end(); ++it) {
        QJsonObject o;
        _rndDesc[*key].get()->toJson(o);
        aRenderers.append(o);
    }

    j["renderers"] = aRenderers;

    QJsonArray aImages;
    for (auto it = __imDesc.begin(); it != __imDesc.end(); ++it) {
        aImages.append(it.value().get()->name());
    }
    j["images"] = aImages;

    QJsonArray aHeightMaps;
    for (auto it = _hmDesc.begin(); it != _hmDesc.end(); ++it) {
        aHeightMaps.append(it.value().get()->name());
    }
    j["heightMaps"] = aHeightMaps;

}
#else

///
/// \brief TextureBuilder::toJson
/// \param j
///
///  Serializes a TextureBuilder instance to json
void TextureBuilder::toJson(json &j) {
    j["colorMap"] = _colorMap;
    j["cloudMap"] = _cloudMap;
    j["reflectionMap"] = _reflectionMap;
    j["bumpMap"] = _bumpMap;

    j["builderType"] = _builderType;

    auto a = json::array();
    a.push_back(std::get<0>(_size));
    a.push_back(std::get<1>(_size));
    j["size"] = a;
    auto a1 = json::array();
    a1.push_back(std::get<0>(_bounds));
    a1.push_back(std::get<1>(_bounds));
    a1.push_back(std::get<2>(_bounds));
    a1.push_back(std::get<3>(_bounds));
    j["bounds"] = a1;


    if (_randomFactors.size() > 0) {
        auto rf = json::array();
        for (auto it = _randomFactors.begin(); it != _randomFactors.end(); ++it) {
            auto w = (*it);
            rf.push_back(w);
        }
        j["randomFactors"] = rf;
    }
    auto aModules = json::array();
    for (auto it = _modDesc.begin(); it != _modDesc.end(); ++it) {
        json o;
        it->second.get()->toJson(o);
        aModules.push_back(o);
    }
    j["modules"] = aModules;

    std::cerr << "Done Modules\n";

    auto aHMBuilders = json::array();
    for (auto it = _nmbDesc.begin(); it != _nmbDesc.end(); ++it) {
        json o;
        it->second.get()->toJson(o);
        aHMBuilders.push_back(o);
    }
    j["heightMapBuilders"] = aHMBuilders;

    std::cerr << "Done HMB\n";

    auto aRenderers = json::array();
    //
    /*
    std::cout << _rndDesc.size() << std::endl;
    for (auto it = _rndDesc.begin(); it != _rndDesc.end(); ++it) {
        json o;
        std::cout << it->first  << "," << it->second->name() << std::endl;
        it->second.get()->toJson(o);
        aRenderers.push_back(o);
    }
    */
    for (auto key = _rndNames.begin(); key != _rndNames.end(); ++key) {
        //std::cout << *key << std::endl;
        auto data = _rndDesc[*key];
        //std::cout << *key << "," << data->name() << std::endl;
        //for (auto it = _rndDesc.begin(); it != _rndDesc.end(); ++it) {
        json o;
        data->toJson(o);
        aRenderers.push_back(o);
    }

    j["renderers"] = aRenderers;

    std::cerr << "Done Renderers\n";

    auto aImages  = json::array();
    for (auto it = __imDesc.begin(); it != __imDesc.end(); ++it) {
        aImages.push_back(it->second.get()->name());
    }
    j["images"] = aImages;

    auto aHeightMaps =  json::array();
    for (auto it = _hmDesc.begin(); it != _hmDesc.end(); ++it) {
        aHeightMaps.push_back(it->second.get()->name());
    }
    j["heightMaps"] = aHeightMaps;


}
#endif // JSON

///
/// \brief TextureBuilder::createModules
///
/// Creates the modules according to the stored descriptors.
void TextureBuilder::createModules() {
    shared_ptr<ModuleDescriptor> mod;
    for (auto mod : _modDesc) {
        auto pMod = mod.second;
        auto strKey = mod.first;
        auto modPtr = pMod.get();
        auto modName =  pMod.get()->name();
        cerr << "Creating module object: " << strKey << endl;
        if (this->useRandomFactors() &&  modPtr->enableRandom() ) {            
            cerr << "Randomizing: " << modName << endl;
            //modPtr->debugLine();
            modPtr->setBias(this->applyRandomFactor(modPtr->bias()) );
            modPtr->setDispl(this->applyRandomFactor(modPtr->displ()) );
            modPtr->setExp(this->applyRandomFactor(modPtr->exp()) );
            modPtr->setFreq(this->applyRandomFactor(modPtr->freq()) );
            modPtr->setLac(this->applyRandomFactor(modPtr->lac()) );
            modPtr->setLbound(this->applyRandomFactor(modPtr->lBound()) );
            modPtr->setPers(this->applyRandomFactor(modPtr->pers()) );
            modPtr->setPow(this->applyRandomFactor(modPtr->pow()) );
            modPtr->setRough(this->applyRandomFactor(modPtr->rough()) );
            modPtr->setScale(this->applyRandomFactor(modPtr->scale()) );
            modPtr->setValue(this->applyRandomFactor(modPtr->value()) );
            modPtr->setX(this->applyRandomFactor(modPtr->x()) );
            modPtr->setY(this->applyRandomFactor(modPtr->y()) );
            modPtr->setZ(this->applyRandomFactor(modPtr->z()) );
            //cerr << "Randomizing: to..." << endl;
            //modPtr->debugLine();
        }
        pMod.get()->setModules(_modules);
        auto ptr = mod.second.get()->makeModule();
        // qDebug() << "Module " << modPtr->name() << " After module creation...";
        modPtr->dumpModule();
        _modules.insert({pMod.get()->name(), ptr});
    }
}


void TextureBuilder::createImages() {
    shared_ptr<ImageDescriptor> img;
    // qDebug() << "Creating images object for " << _textureFile;
    for (auto& img : __imDesc) {
        auto key = img.first;
        auto val = img.second;
        // qDebug() << "Creating image " << img.get()->name();
        auto ptr = val.get()->makeImage();
        _images.insert({key,ptr});
        // qDebug() << "Added to image map";
    }
}

void TextureBuilder::createRenderers() {
    shared_ptr<RendererDescriptor> hmd;
    //QMapIterator<string, shared_ptr<RendererDescriptor>>  it(_rndDesc);
    for (auto i = _rndNames.begin(); i != _rndNames.end(); ++i) {
        //while (it.hasNext()) {
        string name = (*i);
        hmd =  _rndDesc[name]; //it.next().value();
        hmd.get()->setImages(_images);
        hmd.get()->setNoiseMaps(_heightMaps);
        if (this->useRandomFactors() ) {
            double rf = this->pickRandomFactor();
            hmd.get()->randomPositionFactor(rf);
        }
        shared_ptr<utils::RendererImage> ptr
                = hmd.get()->makeRenderer();
        _renderers.insert({hmd.get()->name(), ptr});
        _lstRenderers.push_back(ptr);
    }
}

void TextureBuilder::createHeightMaps() {
    shared_ptr<HeightMapDescriptor> hmd;
    for (auto& hmd : _hmDesc) {
        auto ptr = hmd.second.get()->makeNoiseMap();
        _heightMaps.insert({hmd.second.get()->name(), ptr});
    }

}

void TextureBuilder::createNoiseMapBuilders() {
    shared_ptr<NoiseMapBuilderDescriptor> hmd;
    for (auto& hmd : _nmbDesc) {
        hmd.second.get()->setModules(_modules);
        auto ptr = hmd.second.get()->makeBuilder();
        _noiseMapBuilders.insert({hmd.second.get()->name(), ptr});
    }

}

void TextureBuilder::connectModules()
{
    map<string,shared_ptr<ModuleDescriptor>>::iterator iter = _modDesc.begin();
    while (iter != _modDesc.end())
    {
        cerr << "Connecting " << iter->first << endl;
        iter->second.get()->setModules(_modules);
        iter->second.get()->connectModules();
        cerr << "Connected."  << endl;
        iter++;
    }
}

void TextureBuilder::connectNoiseMapBuilders() {
    map<string,shared_ptr<NoiseMapBuilderDescriptor>>::iterator iter = _nmbDesc.begin();
    while (iter != _nmbDesc.end())
    {
        iter->second.get()->setModules(_modules);
        iter->second.get()->setNoiseMapBuilders(_noiseMapBuilders);
        iter->second.get()->setNoiseMaps(_heightMaps);
        iter->second.get()->connectSrcModule();
        iter++;
    }
}

void TextureBuilder::connectRenderers() {
    map<string,shared_ptr<RendererDescriptor>>::iterator iter = _rndDesc.begin();
    while (iter != _rndDesc.end())
    {
        auto ptr = iter->second.get();
        ptr->setRenderers(_renderers);
        ptr->setNoiseMaps(_heightMaps);
        ptr->connectImagesAndMap();
        iter++;
    }

}


void TextureBuilder::buildNoiseMaps()
{
    map<string,shared_ptr<utils::NoiseMapBuilder>>::iterator iter =  _noiseMapBuilders.begin();
    while(iter != _noiseMapBuilders.end()) {
        utils::NoiseMapBuilder* ptr = iter->second.get();
        ptr->Build();
        iter++;
    }

}

void TextureBuilder::renderRenderers()
{
    shared_ptr<utils::RendererImage> r;
    for (auto r : _lstRenderers) {
        std::cerr << "Rendering renderer...\n";
        try {
            r->Render();
        }
        catch (std::string &err) {
            cerr << "Error in renderRenderers";
            throw err;
        }
        catch (...) {
            cerr << "Error in renderRenderers";
            throw "Undefined error building texture ";
        }        
    }
}


void TextureBuilder::prepareObjectFromJsonFile(const string &filename) {
    try {
        this->loadTextureFromJson(filename);
        this->connectDescriptorsAndPrepare();
    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "Error in prepareObjectFromJsonFile";
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "Error in prepareObjectFromJsonFile";
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "Error in prepareObjectFromJsonFile";
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "Error in prepareObjectFromJsonFile";
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "Error in prepareObjectFromJsonFile";
        throw err;
    }
    catch (...) {
        cerr << "Error in prepareObjectFromJsonFile";
        throw "Undefined error building texture " + filename;
    }
}

/// To be called before building images.
///
/// This method checks if all images defined in json file are actually used
/// and if renderers use them correctly (that is, when an Image is referenced
/// first time by a Renderer is referenced as a destination image, not
/// as a background image and the like...
///
/// textureSanityCheck() throws a string containing the error messages if something's wrong.
void TextureBuilder::textureSanityCheck() {

    // Here, we store image usage matrix. Image name is the key,
    // while the <bool,bool> tuple means that it's been used and first
    // referenced correctly as a renderer (that is as Destination)

    map<string, std::tuple<bool,bool>> imageUsage;

    // let's assume the worst
    for (auto it = __imDesc.begin(); it != __imDesc.end(); ++it) {
        imageUsage.insert({it->first,std::tuple<bool,bool>(false,false)});
    }

    // Now that we have initialized our matrix, let's work on Renderers
    for (auto it = _rndNames.begin(); it != _rndNames.end(); ++it) {
        //for (auto it = _rndDesc.begin(); it != _rndDesc.end(); ++it) {
        auto renderer = _rndDesc[*it].get();

        //first thing first: let's see if it's got an image map defined.
        if (renderer->heightMap().empty())
            throw "Renderer " + renderer->name() + " has no heightMap defined";
        //then if it's actually in list
        auto itHm = _hmDesc.find(renderer->heightMap());
        if (itHm == _hmDesc.end())
            throw "Renderer " + renderer->name() + " references " + renderer->heightMap() + " as its heightMap but it's undefined";

        //ok, now to image. First, background. Let's check it's been defined and already used.
        //Then destination. Every renderer must define a
        //destination image
        auto back = renderer->backgroundImage();
        auto dest = renderer->destImage();

        //background can be null, let's check its existence if defined
        if (!back.empty() ) {

            //if defined background must exist
            auto it2 = __imDesc.find(back);
            if (it2 == __imDesc.end())
                throw "Renderer " + renderer->name() + " references " + back + " as its background image but it's undefined";

            //if it exists, it must have already been used as a dest image
            auto t = imageUsage[back];
            if ( !std::get<0>(t))
                throw "Image " + back + " is used as background on Renderer " + renderer->name() + " but it's never been used before.";
            if ( !std::get<1>(t))
                throw "Image " + back + " is used as background on Renderer " + renderer->name() + " but it's never been used as a destination before.";
        }


        if (dest.empty()) {
            throw "Renderer " + renderer->name() + " has no destination image defined";
        }

        auto i3 = __imDesc.find(dest);
        if (i3 == __imDesc.end()) {
            throw "Renderer " + renderer->name() + " references " + dest + " as its destination image but it's undefined";
        }
        std::tuple<bool,bool> tOk(true,true);
        imageUsage[dest] = tOk;
    }

    //If we get here Renderers are OK, let's see if there are unused images;
    for (auto it = imageUsage.begin(); it != imageUsage.end(); ++it) {
        auto t = it->second;
        if (!std::get<0>(t))
            throw "Image " + it->first + " is not referenced by any Renderer";
        if (!std::get<1>(t))
            throw "Image " + it->first + " is first referenced as a Background image. Fix this. ";
    }

    //NOW we're done.
    return;
}

void TextureBuilder::prepareTextureForRendering() {
    try {
        auto jsonData = this->toJsonString();
        loadTextureFromJsonString(jsonData);
        connectDescriptorsAndPrepare();
    }
    catch (noise::ExceptionInvalidParam err) {
        cerr << "Error in prepareTextureForRendering";
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule err) {
        cerr << "Error in prepareTextureForRendering";
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "Error in prepareTextureForRendering";
        throw "Out of memory!";
    }
    catch (noise::Exception err) {
        cerr << "Error in prepareTextureForRendering";
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "Error in prepareTextureForRendering";
        throw err;
    }
    catch (...) {
        cerr << "Error in prepareTextureForRendering";
        throw "Undefined error building texture from string data";
    }
}

void TextureBuilder::prepareObjectFromJsonString(const string &jsonData) {
    try {
        loadTextureFromJsonString(jsonData);
        this->connectDescriptorsAndPrepare();
    }
    catch (noise::ExceptionInvalidParam err) {
        cerr << "Error in prepareObjectFromJsonString";
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule err) {
        cerr << "Error in prepareObjectFromJsonString";
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "Error in prepareObjectFromJsonString";
        throw "Out of memory!";
    }
    catch (noise::Exception err) {
        cerr << "Error in prepareObjectFromJsonString";
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "Error in prepareObjectFromJsonString";
        throw err;
    }
    catch (...) {
        cerr << "Error in prepareObjectFromJsonString";
        throw "Undefined error building texture from string data";
    }
}

void TextureBuilder::saveRenderedImageToFile(const string &imageName, const string &destFileName) {
    auto imgIt = _images.find(imageName);

    if (_images.size() > 0 &&  imgIt != _images.end() ) {
        auto ImgPtr = _images[imageName].get();
        // qDebug() << "Saving image..." + destFileName;
        utils::WriterBMP writer;
        writer.SetSourceImage (*ImgPtr);
        writer.SetDestFilename(destFileName);
        writer.WritePngFile();
    }
    else
        throw "image " + imageName + " undefined or not present, or no images have been defined";
}

void TextureBuilder::connectDescriptorsAndPrepare() {
    try {
        cerr << "Creating libnoise stuff\n";
        this->createAll();

        cerr << "Connecting modules and stuff\n";
        this->connectAll();

        cerr  << "All done, let's perform a sanity check\n";
        this->textureSanityCheck();
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "connectDescriptorsAndPrepare" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "connectDescriptorsAndPrepare" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "connectDescriptorsAndPrepare" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "connectDescriptorsAndPrepare" << endl;
        throw err;
    }
    catch (...) {
        cerr << "connectDescriptorsAndPrepare" << endl;
        throw "Undefined error building texture in connectDescriptorsAndPrepare " ;
    }    
}

void TextureBuilder::buildTextureFromJsonString(const string &jsonData, string path) {
    try {
        this->loadTextureFromJsonString(jsonData);
        this->connectDescriptorsAndPrepare();
        this->writeTextureToPath(path);
    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw err;
    }
    catch (...) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Undefined error building texture " + jsonData;
    }



}

void TextureBuilder::loadTextureFromJsonString(const string &jsonData) {
    try {
        json j = json::parse(jsonData);
        this->fromJson(j);
    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "buildTextureFromJsonString" << endl;
        throw err;
    }
    catch (...) {
        cerr << "buildTextureFromJsonString" << endl;
        throw "Undefined error building texture " + jsonData;
    }



}


void TextureBuilder::buildTextureFromJson(const string &filename, string path) {
    try {
        this->loadTextureFromJson(filename);
        this->connectDescriptorsAndPrepare();
        this->writeTextureToPath(path);
    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "buildTextureFromJson 1" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "buildTextureFromJson 2" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "buildTextureFromJson 3" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "buildTextureFromJson 4" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "buildTextureFromJson 5" << endl;
        throw err;
    }
    catch (...) {
        cerr << "buildTextureFromJson 6" << endl;
        cerr << filename << endl;
        cerr << path << endl;
        throw "Undefined error building texture " + filename;
    }


}

void TextureBuilder::saveImageDescriptorToFile(string image_name, string filename) {
    try {
        cerr  << "Saving  images...from " << image_name << " to "  << filename << "\n";
        utils::WriterBMP writer;
        auto imagePtr = _images[image_name].get();
        writer.SetSourceImage (*imagePtr);
        writer.SetDestFilename(filename);
        writer.WritePngFile();
        cerr << "wrote image " << filename << endl;

    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "saveImageDescriptorToFile 1" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "saveImageDescriptorToFile 2" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "saveImageDescriptorToFile 3" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "saveImageDescriptorToFile 4" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "saveImageDescriptorToFile 5" << endl;
        throw err;
    }
    catch (...) {
        cerr << "saveImageDescriptorToFile 6" << endl;
        cerr << filename << endl;
        throw "Undefined error building texture " + _outputFileName;
    }

}

void TextureBuilder::saveImageDescriptorToFile(shared_ptr<ImageDescriptor> image, string filename) {
    try {
        cerr  << "Saving  images...from " << image->name() << " to "  << filename << "\n";
        utils::WriterBMP writer;
        auto imagePtr = _images[image->name()].get();
        writer.SetSourceImage (*imagePtr);
        writer.SetDestFilename(filename);
        writer.WritePngFile();
        cerr << "wrote image " << filename << endl;

    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "saveImageDescriptorToFile 1" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "saveImageDescriptorToFile 2" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "saveImageDescriptorToFile 3" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "saveImageDescriptorToFile 4" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "saveImageDescriptorToFile 5" << endl;
        throw err;
    }
    catch (...) {
        cerr << "saveImageDescriptorToFile 6" << endl;
        cerr << filename << endl;
        throw "Undefined error building texture " + _outputFileName;
    }

}

void TextureBuilder::writeTextureToPath(string path) {
    try {
        cerr  << "Building images...\n";
        string outFile = "";
        shared_ptr<utils::Image> imgPtr = this->buildImages();
        int h = 0;
        _generatedMaps.clear();
        for (auto iter = __imDesc.begin(); iter != __imDesc.end(); ++iter) {
            auto name = iter->first;
            cerr << "processing image " << name << endl;
            auto ImgPtr = _images[name].get();
            string tmpFile = path+"/"+ _outputFileName +"_"+ name+".png";
            if (h == 0)
                outFile = tmpFile;
            h++;

            cerr << "Saving image to..." << tmpFile << endl;
            utils::WriterBMP writer;
            writer.SetSourceImage (*ImgPtr);
            writer.SetDestFilename(tmpFile);
            writer.WritePngFile();
            cerr << "wrote image " << tmpFile << endl;

        }    
    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "writeTextureToPath 1" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "writeTextureToPath 2" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "writeTextureToPath 3" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "writeTextureToPath 4" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "writeTextureToPath 5" << endl;
        throw err;
    }
    catch (...) {
        cerr << "writeTextureToPath 6" << endl;
        cerr << path << endl;
        throw "Undefined error building texture " + _outputFileName;
    }

}

void TextureBuilder::loadTextureFromJson(const string &filename) {
    try {
        _textureFile = filename;
        cerr << "opening filename " << filename << endl;
        std::ifstream i(_textureFile);
        json j;
        cerr << "loading file to json " << endl;
        i >> j;
        cerr << "json loaded: " << endl;
        cerr << j.dump(4);
        this->fromJson(j);

    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "loadTextureFromJson 1" << endl;
        throw "Invalid param error";
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "loadTextureFromJson 2" << endl;
        throw "No module defined as source or control";
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "loadTextureFromJson 3" << endl;
        throw "Out of memory!";
    }
    catch (noise::Exception &err) {
        cerr << "loadTextureFromJson 4" << endl;
        throw "Generic noise::exception";
    }
    catch (std::string &err) {
        cerr << "loadTextureFromJson 5" << endl;
        throw err;
    }
    catch (...) {
        cerr << "loadTextureFromJson 6" << endl;
        cerr << filename << endl;
        throw "Undefined error building texture " + filename;
    }


}



void TextureBuilder::raiseRenderer(const string& rendererName) {
    auto it = std::find(_rndNames.begin(), _rndNames.end(), rendererName);
    if (it == _rndNames.end())
    {
      // name not in vector
    } else
    {
      auto i = std::distance(_rndNames.begin(), it);
      if (i > 0) {
          auto iTmp = _rndNames[i];
          _rndNames[i] = _rndNames[i-1];
          _rndNames[i-1] = iTmp;
      }
    }
}

void TextureBuilder::lowerRenderer(const string& rendererName) {
    auto it = std::find(_rndNames.begin(), _rndNames.end(), rendererName);
    if (it == _rndNames.end())
    {
      // name not in vector
    } else
    {
      auto i = std::distance(_rndNames.begin(), it);
      auto i1 = static_cast<long long>(_rndNames.size());
      if (i < i1-1) {
          auto iTmp = _rndNames[i];
          _rndNames[i] = _rndNames[i+1];
          _rndNames[i+1] = iTmp;
      }
    }

 }
