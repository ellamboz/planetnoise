/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/


#include "moduledescriptor.h"
#include <string>
#include <iostream>

using namespace std;


ModuleDescriptor::ModuleDescriptor()
{
    this->_src1="";
    this->_src2="";
    this->_src3="";
    this->_src4="";
}

#ifdef JSON
void ModuleDescriptor::fromJson(const QJsonObject& json) {
    if (!j["type"].isNull() && !j["type"].isUndefined())
        _moduleType = j["type"].toString();
    if (!j["name"].isNull() && !j["name"].isUndefined())
        _name = j["name"].toString();
    if (!j["seed"].isNull() && !j["seed"].isUndefined())
        _seed = j["seed"].toInt();
    if (!j["freq"].isNull() && !j["freq"].isUndefined())
        _freq = j["freq"].toDouble();
    if (!j["lac"].isNull() && !j["lac"].isUndefined())
        _lac = j["lac"].toDouble();
    if (!j["pers"].isNull() && !j["pers"].isUndefined())
        _pers = j["pers"].toDouble();

    if (!j["offset"].isNull() && !j["offset"].isUndefined())
        _offset = j["offset"].toDouble();
    if (!j["gain"].isNull() && !j["gain"].isUndefined())
        _gain = j["gain"].toDouble();


    if (!j["oct"].isNull() && !j["oct"].isUndefined())
        _oct = j["oct"].toInt();
    if (!j["disp"].isNull() && !j["disp"].isUndefined())
        _disp = j["disp"].toDouble();
    if (!j["enableDist"].isNull() && !j["enableDist"].isUndefined())
        _enableDist = j["enableDist"].toBool();
    if (!j["src1"].isNull() && !j["src1"].isUndefined())
        _src1 = j["src1"].toString();
    if (!j["src2"].isNull() && !j["src2"].isUndefined())
        _src2 = j["src2"].toString();
    if (!j["src3"].isNull() && !j["src3"].isUndefined())
        _src3 = j["src3"].toString();
    if (!j["src4"].isNull() && !j["src4"].isUndefined())
        _src4 = j["src4"].toString();
    if (!j["ctl"].isNull() && !j["ctl"].isUndefined())
        _ctl = j["ctl"].toString();
    if (!j["lbound"].isNull() && !j["lbound"].isUndefined())
        _lBound = j["lbound"].toDouble();
    if (!j["ubound"].isNull() && !j["ubound"].isUndefined())
        _uBound = j["ubound"].toDouble();
    if (!j["cpoints"].isNull() && !j["cpoints"].isUndefined()) {
        QJsonArray ja = j["cpoints"].toArray();
        for (auto x = 0; x < ja.size(); ++x ) {
            QJsonArray ox = ja[x].toArray();
            _cPoints.append(std::tuple<double,double>(ox[0].toDouble(),ox[1].toDouble())  );
        }
    }
    //    _cPoints = j["cPoints"].toQVector<std::tuple<double,double>>();
    if (!j["exp"].isNull() && !j["exp"].isUndefined())
        _exp = j["exp"].toDouble();
    if (!j["bias"].isNull() && !j["bias"].isUndefined())
        _bias = j["bias"].toDouble();
    if (!j["scale"].isNull() && !j["scale"].isUndefined())
        _scale = j["scale"].toDouble();
    if (!j["invert"].isNull() && !j["invert"].isUndefined())
        _invert = j["invert"].toBool();
    if (!j["x"].isNull() && !j["x"].isUndefined())
        _x = j["x"].toDouble();
    if (!j["y"].isNull() && !j["y"].isUndefined())
        _y = j["y"].toDouble();
    if (!j["z"].isNull() && !j["z"].isUndefined())
        _z = j["z"].toDouble();
    if (!j["pow"].isNull() && !j["pow"].isUndefined())
        _pow = j["pow"].toDouble();
    if (!j["rough"].isNull() && !j["rough"].isUndefined())
        _rough = j["rough"].toDouble();
    if (!j["value"].isNull() && !j["value"].isUndefined())
        _value = j["value"].toDouble();
    if (!j["enableRandom"].isNull() && !j["enableRandom"].isUndefined())
        _enableRandom = j["enableRandom"].toBool();
    this->setupPropertiesToExport(this->_moduleType);

}

void ModuleDescriptor::toJson(QJsonObject& json) {
    j["type"] = _moduleType;
    j["name"] = _name;
    if (propContains("seed")) j["seed"] = _seed;
    if (propContains("freq")) j["freq"] = _freq;
    if (propContains("offset")) j["offset"] = _offset;
    if (propContains("gain")) j["gain"] = _gain;
    if (propContains("lac")) j["lac"] = _lac;
    if (propContains("pers")) j["pers"] = _pers;
    if (propContains("oct")) j["oct"] = _oct;
    if (propContains("displ") || propContains("disp")) j["disp"] = _disp;
    if (propContains("enableDist")) j["enableDist"] = _enableDist;
    if (propContains("src1")) j["src1"] = _src1;
    if (propContains("src2")) j["src2"] = _src2;
    if (propContains("src3")) j["src3"] = _src3;
    if (propContains("src4")) j["src4"] = _src4;
    if (propContains("ctl")) j["ctl"] = _ctl;
    if (propContains("lBound")) j["lbound"] = _lBound;
    if (propContains("uBound")) j["ubound"] = _uBound;
    if (propContains("enableRandom")) j["enableRandom"] = _enableRandom;

    QJsonArray a;
    std::tuple<double,double> tp;
    foreach (tp, _cPoints) {
        QJsonArray o;
        o.append(std::get<0>(tp));
        o.append(std::get<1>(tp));
        a.append(o);
    }
    if (propContains("controlPoints")) j["cpoints"] = a;
    //j["cPoints"] = _cPoints;
    if (propContains("exp")) j["exp"] = _exp;
    if (propContains("bias")) j["bias"] = _bias;
    if (propContains("scale")) j["scale"] = _scale;
    if (propContains("invert")) j["invert"] = _invert;
    if (propContains("x")) j["x"] = _x;
    if (propContains("y")) j["y"] = _y;
    if (propContains("z")) j["z"] = _z;
    if (propContains("pow")) j["pow"] = _pow;
    if (propContains("rough")) j["rough"] = _rough;
    if (propContains("value")) j["value"] = _value;
}
#else

#define DEFINEDNOTNULL(j,k) ( (j).find((k)) != (j).end() && !(j)[(k)].is_null()  )

void ModuleDescriptor::fromJson(const json& j) {
    if (DEFINEDNOTNULL(j,"type"))
        _moduleType = j["type"].get<string>();
    if (DEFINEDNOTNULL(j,"name"))
        _name = j["name"].get<string>();
    if (DEFINEDNOTNULL(j,"seed") && j["seed"].is_number())
        _seed = j["seed"].get<int>();
    if (DEFINEDNOTNULL(j,"freq") && j["freq"].is_number())
        _freq = j["freq"].get<double>();
    if (DEFINEDNOTNULL(j,"lac") && j["lac"].is_number())
        _lac = j["lac"].get<double>();
    if (DEFINEDNOTNULL(j,"pers") && j["pers"].is_number())
        _pers = j["pers"].get<double>();

    if (DEFINEDNOTNULL(j,"offset") && j["offset"].is_number())
        _offset = j["offset"].get<double>();
    if (DEFINEDNOTNULL(j,"gain") && j["gain"].is_number())
        _gain = j["gain"].get<double>();


    if (DEFINEDNOTNULL(j,"oct")&& j["oct"].is_number())
        _oct = j["oct"].get<int>();
    if (DEFINEDNOTNULL(j,"disp")&& j["disp"].is_number())
        _disp = j["disp"].get<double>();
    if (DEFINEDNOTNULL(j,"enableDist") && j["enableDist"].is_boolean())
        _enableDist = j["enableDist"].get<bool>();
    if (DEFINEDNOTNULL(j,"src1") && j["src1"].is_string())
        _src1 = j["src1"].get<string>();
    if (DEFINEDNOTNULL(j,"src2") && j["src2"].is_string())
        _src2 = j["src2"].get<string>();
    if (DEFINEDNOTNULL(j,"src3")&& j["src3"].is_string())
        _src3 = j["src3"].get<string>();
    if (DEFINEDNOTNULL(j,"src4")&& j["src4"].is_string())
        _src4 = j["src4"].get<string>();
    if (DEFINEDNOTNULL(j,"ctl")&& j["ctl"].is_string())
        _ctl = j["ctl"].get<string>();
    if (DEFINEDNOTNULL(j,"lbound") && j["lbound"].is_number())
        _lBound = j["lbound"].get<double>();
    if (DEFINEDNOTNULL(j,"ubound") && j["ubound"].is_number())
        _uBound = j["ubound"].get<double>();
    if (DEFINEDNOTNULL(j,"cpoints")) {
        auto ja = j["cpoints"];
        for (json::iterator it = ja.begin(); it != ja.end(); ++it) {
            auto ox = it.value();
            _cPoints.push_back(std::tuple<double,double>(ox[0].get<double>(),ox[1].get<double>())  );
        }
    }
    //    _cPoints = j["cPoints"].toQVector<std::tuple<double,double>>();
    if (DEFINEDNOTNULL(j,"exp")&& j["exp"].is_number())
        _exp = j["exp"].get<double>();
    if (DEFINEDNOTNULL(j,"bias") && j["bias"].is_number())
        _bias = j["bias"].get<double>();
    if (DEFINEDNOTNULL(j,"scale") && j["scale"].is_number())
        _scale = j["scale"].get<double>();
    if (DEFINEDNOTNULL(j,"invert")&& j["invert"].is_boolean())
        _invert = j["invert"].get<bool>();
    if (DEFINEDNOTNULL(j,"x") && j["x"].is_number())
        _x = j["x"].get<double>();
    if (DEFINEDNOTNULL(j,"y")  && j["y"].is_number())
        _y = j["y"].get<double>();
    if (DEFINEDNOTNULL(j,"z") && j["z"].is_number())
        _z = j["z"].get<double>();
    if (DEFINEDNOTNULL(j,"pow")  && j["pow"].is_number())
        _pow = j["pow"].get<double>();
    if (DEFINEDNOTNULL(j,"rough") && j["rough"].is_number())
        _rough = j["rough"].get<double>();
    if (DEFINEDNOTNULL(j,"value") && j["value"].is_number())
        _value = j["value"].get<double>();
    if (DEFINEDNOTNULL(j,"enableRandom")  && j["enableRandom"].is_boolean())
        _enableRandom = j["enableRandom"].get<bool>();
    this->setupPropertiesToExport(this->_moduleType);
}

void ModuleDescriptor::toJson(json& j) {
    j["type"] = _moduleType;
    j["name"] = _name;
    cerr << "exporting " << _name << " to json " << endl;
    for (auto pte : _propertiesToExport) {
        cerr << "prop: " << pte << endl;
        cerr << _name << ", " << _freq << ", " << _lac << ", " << _pers << ", " << _oct << endl;
    }
    if (propContains("seed")) j["seed"] = _seed;
    if (propContains("freq")) j["freq"] = _freq;
    if (propContains("offset")) j["offset"] = _offset;
    if (propContains("gain")) j["gain"] = _gain;
    if (propContains("lac")) j["lac"] = _lac;
    if (propContains("pers")) j["pers"] = _pers;
    if (propContains("oct")) j["oct"] = _oct;
    if (propContains("displ") || propContains("disp")) j["disp"] = _disp;
    if (propContains("enableDist")) j["enableDist"] = _enableDist;
    if (propContains("src1")) j["src1"] = _src1;
    if (propContains("src2")) j["src2"] = _src2;
    if (propContains("src3")) j["src3"] = _src3;
    if (propContains("src4")) j["src4"] = _src4;
    if (propContains("ctl")) j["ctl"] = _ctl;
    if (propContains("lBound")) j["lbound"] = _lBound;
    if (propContains("uBound")) j["ubound"] = _uBound;
    if (propContains("enableRandom")) j["enableRandom"] = _enableRandom;

    auto a = json::array();
    std::tuple<double,double> tp;
    for (auto& tp :  _cPoints) {
        auto o = json::array();
        o.push_back(std::get<0>(tp));
        o.push_back(std::get<1>(tp));
        a.push_back(o);
    }
    if (propContains("controlPoints")) j["cpoints"] = a;
    //j["cPoints"] = _cPoints;
    if (propContains("exp")) j["exp"] = _exp;
    if (propContains("bias")) j["bias"] = _bias;
    if (propContains("scale")) j["scale"] = _scale;
    if (propContains("invert")) j["invert"] = _invert;
    if (propContains("x")) j["x"] = _x;
    if (propContains("y")) j["y"] = _y;
    if (propContains("z")) j["z"] = _z;
    if (propContains("pow")) j["pow"] = _pow;
    if (propContains("rough")) j["rough"] = _rough;
    if (propContains("value")) j["value"] = _value;
}

#endif

//static ModuleDescriptor createRandom();



ModuleDescriptor& ModuleDescriptor::connectModules()
{
    shared_ptr<Module> currentMod;

    //referenced modules sanity check
    if (propContains("src1") && _src1 == "") throw "You must define src1 in module "+_name;
    if (propContains("src2") && _src2 == "") throw "You must define src2 in module "+_name;
    if (propContains("src3") && _src3 == "") throw "You must define src3 in module "+_name;
    if (propContains("src4") && _src4 == "") throw "You must define src4 in module "+_name;
    if (propContains("ctl") && _ctl =="") throw "You must define ctl in module "+_name;
    if (propContains("src1") && !moduleContains(_src1)) throw _src1
            + " is referenced as src1, but there is no module defined in  "+_name;
    if (propContains("src2") && !moduleContains(_src2)) throw _src2
            + " is referenced as src1, but there is no module defined in  "+_name;
    if (propContains("src3") && !moduleContains(_src3)) throw _src3
            + " is referenced as src1, but there is no module defined in  "+_name;
    if (propContains("src4") && !moduleContains(_src4)) throw _src4
            + " is referenced as src1, but there is no module defined in  "+_name;
    if (propContains("ctl") && !moduleContains(_ctl)) throw _ctl
            + " is referenced as src1, but there is no module defined in  "+_name;

    Module * pModule = nullptr;
    if (_name != "" && moduleContains(_name)) {
        currentMod =_modules[_name];
        pModule = currentMod.get();
    }
    else
        throw "Module named " + _name + " not defined.";
    if (_moduleType != "Displace") {
        if (_src1 != "") {
            if (moduleContains(_src1))
                currentMod.get()->SetSourceModule(0,*_modules[_src1].get());
            else
                throw "Source Module src1 '" + _src1 + " referenced, but no module has been defined with this name";
        }
        if (_src2 != "" ) {
            if (moduleContains(_src2))
                currentMod.get()->SetSourceModule(1,*_modules[_src2].get());
            else
                throw "Source Module src2 '" + _src2 + " referenced, but no module has been defined with this name";
        }
        if (_src3 != "") {
            if (moduleContains(_src3))
                currentMod.get()->SetSourceModule(2,*_modules[_src3].get());
            else
                throw "Source Module src3 '" + _src3 + " referenced, but no module has been defined with this name";
        }
        if (_src4 != "") {
            if (moduleContains(_src4))
                currentMod.get()->SetSourceModule(3,*_modules[_src4].get());
            else
                throw "Source Module src4 '" + _src4 + " referenced, but no module has been defined with this name";
        }
    }
    if (this->_moduleType == "Displace") {
        if (_src1 == "" || _src2 == ""  || _src3 == "" || _src4 == "")
            throw "Displace modules require that all modules src1, src2, src3 and src4 be referenced";
        if (!moduleContains(_src1) || !moduleContains(_src2)  || !moduleContains(_src3) || !moduleContains(_src4))
            throw "Displace modules require that all modules src1, src2, src3 and src4 be defined in your Texture representation";
        auto mod = static_cast<Displace*>(pModule);
        mod->SetSourceModule(0,*_modules[_src1].get());
        mod->SetDisplaceModules(
            *_modules[_src2].get(),
            *_modules[_src3].get(),
            *_modules[_src4].get()
        );
    }
    if (_ctl != "" ) {
        if (moduleContains(_ctl)) {
            if (this->_moduleType == "Blend") {
                auto mod = static_cast<Blend*>(pModule);
                mod->SetControlModule(*_modules[_ctl].get());
            }
            if (this->_moduleType == "Select") {
                auto mod = static_cast<Select*>(pModule);
                mod->SetControlModule(*_modules[_ctl].get());
            }
       }
       else {
            throw "Control module " + _ctl + " must be defined";
        }

    }
    //this->c_currentModule = currentMod.get();
    return *this;
}

std::string ModuleDescriptor::luaInitialization() {
    std::string data = "";
    if (propContains("src1"))  data = data + _name + ".src1=" + this->_src1 + "\n";
    if (propContains("src2"))  data = data + _name + ".src2=" + this->_src2 + "\n";
    if (propContains("src3"))  data = data + _name + ".src3=" + this->_src3 + "\n";
    if (propContains("src4"))  data = data + _name + ".src4=" + this->_src4 + "\n";
    if (propContains("ctl"))   data = data + _name + ".ctl=" + this->_ctl + "\n";

    if (propContains("oct"))    data = data + _name + ".oct=" + to_string(this->_oct) + "\n";
    if (propContains("seed"))   data = data + _name + ".seed=" + to_string(this->_seed) + "\n";

    if (propContains("freq"))  data = data + _name + ".freq=" + to_string(this->_freq) + "\n";
    if (propContains("lac"))   data = data + _name + ".lac=" + to_string(this->_lac) + "\n";
    if (propContains("pers"))  data = data + _name + ".pers=" + to_string(this->_pers) + "\n";
    if (propContains("exp"))   data = data + _name + ".exp=" + to_string(this->_exp) + "\n";
    if (propContains("value")) data = data + _name + ".value=" + to_string(this->_value) + "\n";
    if (propContains("pow"))   data = data + _name + ".pow=" + to_string(this->_pow) + "\n";
    if (propContains("rough")) data = data + _name + ".rough=" + to_string(this->_rough) + "\n";

    if (propContains("uBound")) data = data + _name + ".uBound=" + to_string(this->_uBound) + "\n";
    if (propContains("lBound")) data = data + _name + ".lBound=" + to_string(this->_lBound) + "\n";

    if (propContains("x")) data = data + _name + ".x=" + to_string(this->_x) + "\n";
    if (propContains("y"))   data = data + _name + ".y=" + to_string(this->_y) + "\n";
    if (propContains("z")) data = data + _name + ".z=" + to_string(this->_z) + "\n";

    if (propContains("displ"))   data = data + _name + ".displ=" + to_string(this->_y) + "\n";
    if (propContains("enableDist")) data = data + _name + ".enableDist=" + (this->_enableDist ? std::string("true") : std::string("false")) +  "\n";

    if (propContains("scale")) data = data + _name + ".bias=" + to_string(this->_bias) + "\n";
    if (propContains("bias"))   data = data + _name + ".scale=" + to_string(this->_scale) + "\n";

    if (_moduleType == "Curve") {
        //control points
        for (auto cp = this->cPoints().begin(); cp != this->cPoints().end(); ++cp) {
            data = data + _name + std::string(":addControlPoint("
                                              +to_string(std::get<0>(*cp))
                                              +","
                                              +to_string(std::get<1>(*cp))
                                              +")\n");
        }
    }

    if (_moduleType == "Terrace") {
        //control points
        for (auto cp = this->cPoints().begin(); cp != this->cPoints().end(); ++cp) {
            data = data + _name + std::string(":addControlPoints("+ to_string(std::get<0>(*cp)) + ")\n");


        }

    }
    return data;
}

void ModuleDescriptor::setupPropertiesToExport(std::string& _m_moduleType) {
    _propertiesToExport.clear();
    if (_m_moduleType=="Abs") appendToProperties( { "name" ,  "src1" , "enableRandom"} );
    if (_m_moduleType=="Add") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    if (_m_moduleType=="Avg") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    if (_m_moduleType=="Avg4") appendToProperties( { "name" ,  "src1" , "src2" , "src3" , "src4" , "enableRandom"} );
    if (_m_moduleType=="Billow")  appendToProperties( { "name" , "seed" , "freq" , "lac" , "pers" , "oct"  , "enableRandom"} );
    if (_m_moduleType=="Blend") appendToProperties( { "name" ,  "src1" , "src2" , "ctl" , "enableRandom"} );
    if (_m_moduleType=="Clamp") appendToProperties( { "name" , "lBound" , "uBound" , "src1" , "enableRandom"} );
    if (_m_moduleType=="Const") appendToProperties( { "name" , "value" , "enableRandom"} );
    if (_m_moduleType=="Cache") appendToProperties( { "name" ,  "src1" } );
    if (_m_moduleType=="Cos") appendToProperties( { "name" ,  "freq" , "exp" , "value" , "src1" , "enableRandom"} );
    if (_m_moduleType=="Curve") appendToProperties( { "name" , "controlPoints" , "src1" , "enableRandom"} );
    if (_m_moduleType=="Cylinders")  appendToProperties( { "name" , "freq" , "enableRandom"} );
    if (_m_moduleType=="Displace") appendToProperties( { "name" ,    "src1" , "src2" , "src3" , "src4" , "ctl" , "enableRandom"} );
    if (_m_moduleType=="Exponent") appendToProperties( { "name" , "exp" , "src1" , "enableRandom"} );
    if (_m_moduleType=="Exp") appendToProperties( { "name" , "exp" , "src1" , "enableRandom"} );
    if (_m_moduleType=="Invert") appendToProperties( { "name" ,  "src1" , "enableRandom"} );
    if (_m_moduleType=="Max") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    if (_m_moduleType=="Min") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    if (_m_moduleType=="Multiply") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    if (_m_moduleType=="Perlin")  appendToProperties( { "name" , "seed" , "freq" , "lac" , "pers" , "oct" , "enableRandom"} );
    if (_m_moduleType=="Power") appendToProperties( { "name" ,  "src1" , "src2" , "enableRandom"} );
    if (_m_moduleType=="RidgedMulti")  appendToProperties( { "name", "seed" , "freq" , "lac" , "oct" , "enableRandom"} );
    if (_m_moduleType=="RidgedMulti2")  appendToProperties( { "name" , "offset" , "gain" , "exp" , "seed" , "freq" , "lac" , "oct" , "enableRandom"} );
    if (_m_moduleType=="RotatePoint") appendToProperties( { "name" , "src1" , "x" , "y" , "z" , "enableRandom"} );
    if (_m_moduleType=="ScaleBias") appendToProperties( { "name" , "bias" , "scale" , "src1" , "enableRandom"} );
    if (_m_moduleType=="ScalePoint") appendToProperties( { "name" , "src1" , "x" , "y" , "z" , "enableRandom"} );
    if (_m_moduleType=="Select") appendToProperties( { "name" ,  "src1" , "src2" , "ctl" , "uBound" , "lBound" , "value" , "enableRandom"} );
    if (_m_moduleType=="Sin") appendToProperties( { "name" ,  "freq" , "exp" , "value" ,  "src1" , "enableRandom"} );
    if (_m_moduleType=="Spheres") appendToProperties( { "name" , "freq" , "enableRandom"} );
    if (_m_moduleType=="Terrace") appendToProperties( { "name" , "controlPoints" , "invert" , "src1" , "enableRandom"} );
    if (_m_moduleType=="TranslatePoint") appendToProperties( { "name" , "src1" , "x" , "y" , "z" , "enableRandom"} );
    if (_m_moduleType=="Turbulence")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    if (_m_moduleType=="Turbulence2")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    if (_m_moduleType=="TurbulenceBillow")  appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    if (_m_moduleType=="TurbulenceRidged")  {
        cerr << "Appending to props to Turbulence Ridged " << endl;
        appendToProperties( { "name" , "seed" , "freq" , "pow" , "rough" , "src1" , "enableRandom"} );
    }
    if (_m_moduleType=="Voronoi") appendToProperties( { "name" , "freq" , "displ" , "seed" , "enableDist" , "enableRandom"} );

}

//from descriptor to actual object
shared_ptr<Module> ModuleDescriptor::makeModule() {
    try {
        //"Oct": 0.0 },
        if (_moduleType=="Billow") return makeBillow();
        if (_moduleType=="Checkerboard") return makeCheckerboard();
        if (_moduleType=="Const") return makeConst();
        if (_moduleType=="Cylinders") return makeCylinders();
        if (_moduleType=="Perlin") return makePerlin();
        if (_moduleType=="RidgedMulti") return makeRidgedMulti();
        if (_moduleType=="RidgedMulti2") return makeRidgedMulti2();
        if (_moduleType=="Spheres") return makeSpheres();
        if (_moduleType=="Voronoi") return makeVoronoi();
        if (_moduleType=="Abs") return makeAbs();
        if (_moduleType=="Clamp") return makeClamp();
        if (_moduleType=="Curve") return makeCurve();
        if (_moduleType=="Exponent") return makeExponent();
        if (_moduleType=="Exp") return makeExponent();
        if (_moduleType=="Invert") return makeInvert();
        if (_moduleType=="ScaleBias") return makeScaleBias();
        if (_moduleType=="Terrace") return makeTerrace();
        if (_moduleType=="Turbulence") return makeTurbulence();
        if (_moduleType=="Turbulence2") return makeTurbulence2();
        if (_moduleType=="TurbulenceBillow") return makeTurbulenceBillow();
        if (_moduleType=="TurbulenceRidged") return makeTurbulenceRidged();
        if (_moduleType=="Add") return makeAdd();
        if (_moduleType=="Max") return makeMax();
        if (_moduleType=="Avg") return makeAvg();
        if (_moduleType=="Avg4") return makeAvg4();
        if (_moduleType=="Cos") return makeCos();
        if (_moduleType=="Sin") return makeSin();
        if (_moduleType=="Min") return makeMin();
        if (_moduleType=="Multiply") return makeMultiply();
        if (_moduleType=="Power") return makePower();
        if (_moduleType=="Blend") return makeBlend();
        if (_moduleType=="Select") return makeSelect();
        if (_moduleType=="Displace") return makeDisplace();
        if (_moduleType=="RotatePoint") return makeRotatePoint();
        if (_moduleType=="ScalePoint") return makeScalePoint();
        if (_moduleType=="TranslatePoint") return makeTranslatePoint();
        return makePerlin();
    }
    catch (noise::ExceptionInvalidParam &exc) {
        throw exc;
    }
    catch (noise::ExceptionNoModule &exc) {
        throw exc;
    }
    catch (noise::ExceptionUnknown &exc) {
        throw exc;
    }
    catch (std::string& exc) {
        throw exc;
    }
    catch (...) {
        std::string err = "Undefined error";
        throw err;
    }
}

//{ "module":"Billow" , "mame": "mod_name" , "seed": 0.0 , "freq": 0.0 , "lac": 0.0 , "pers": 0.0 ,
shared_ptr<Module> ModuleDescriptor::makeBillow() {
    Billow* m = new Billow();
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->SetFrequency(_freq);
    m->SetLacunarity(_lac);
    m->SetPersistence(_pers);

    m->SetOctaveCount(_oct);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "module":"Checkerboard" , "name": "mod_name" },
shared_ptr<Module> ModuleDescriptor::makeCheckerboard() {
    Checkerboard* m = new Checkerboard();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "module":"Const" , "name": "mod_name", "value" : 0.0 },
shared_ptr<Module> ModuleDescriptor::makeConst() {
    Const* m = new Const();
    m->SetConstValue(this->_value);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "module":"Cylinders" , "name": "mod_name" , "Freq": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeCylinders() {
    Cylinders* m = new Cylinders();
    m->SetFrequency(_freq);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Perlin" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Lac": 0.0 , "Pers": 0.0 , "Oct": 0.0 },
shared_ptr<Module> ModuleDescriptor::makePerlin() {
    Perlin* m = new Perlin();
    m->SetLacunarity(_lac);
    m->SetFrequency(_freq);
    m->SetPersistence(_pers);
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->SetOctaveCount(_oct);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"RidgedMulti" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Lac": 0.0 , "Oct": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeRidgedMulti() {
    RidgedMulti* m = new RidgedMulti();
    m->SetLacunarity(_lac);
    m->SetFrequency(_freq);
    m->SetOctaveCount(_oct);
    //m->SetExponent(_exp);
    //m->SetGain(_gain);
    //m->SetOffset(_offset);
    //m->SetWeight(_x);
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"RidgedMulti2" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Lac": 0.0 , "Oct": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeRidgedMulti2() {
    RidgedMulti2* m = new RidgedMulti2();
    m->SetLacunarity(_lac);
    m->SetFrequency(_freq);
    m->SetOctaveCount(_oct);
    m->SetExponent(_exp);
    m->SetGain(_gain);
    m->SetOffset(_offset);
    //m->SetWeight(_x);
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Spheres" , "Name": "mod_name" , "Freq": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeSpheres() {
    //qDebug() << " -- Making sphere module.... ";
    Spheres* m = new Spheres();
    //qDebug() << " made sphere module ";
    m->SetFrequency(_freq);
    auto freq = m->GetFrequency();
    //qDebug() << " newly created sphere has freq " << freq;
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Voronoi" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Displ": 0.0 , "EnableDist": true },
shared_ptr<Module> ModuleDescriptor::makeVoronoi() {
    Voronoi* m = new Voronoi();
    m->SetFrequency(_freq);
    m->SetDisplacement(_disp);
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->EnableDistance(_enableDist);
    shared_ptr<Module>p;
    p.reset(m);
    return p;
}

//{ "Module":"Abs" , "Name": "mod_name" , "Src1": "mod_name" },
shared_ptr<Module> ModuleDescriptor::makeAbs() {
    Abs* m = new Abs();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Clamp" , "Name": "mod_name" , "Src1": "mod_name" , "Lbound": 0.0 , "Ubound": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeClamp() {
    Clamp* m = new Clamp();
    m->SetBounds(_lBound, _uBound);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Curve" , "Name": "mod_name" , "Src1": "mod_name" , "Cpoints": [[0.0,1.0],[1.0,1.1]] },
shared_ptr<Module> ModuleDescriptor::makeCurve() {
    Curve* m = new Curve();
    std::tuple<double,double> t;
    for (auto t :  _cPoints) {
        m->AddControlPoint(std::get<0>(t),std::get<1>(t));
    }
    shared_ptr<Module> p; p.reset(m);
    return p;
}


//{ "Module":"Exponent" , "Name": "mod_name" , "Src1": "mod_name" , "Exp": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeExponent() {
    Exponent* m = new Exponent();
    m->SetExponent(this->_exp);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
shared_ptr<Module> ModuleDescriptor::makeInvert() {
    Invert* m = new Invert();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
shared_ptr<Module> ModuleDescriptor::makeCache() {
    Cache* m = new Cache();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
shared_ptr<Module> ModuleDescriptor::makeCos() {
    Cos* m = new Cos();
    m->SetDelta(_value);
    m->SetFrequency(_freq);
    m->SetExponent(_exp);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Invert" , "Name": "mod_name" , "Src1": "mod_name" },
shared_ptr<Module> ModuleDescriptor::makeSin() {
    Sin* m = new Sin();
    m->SetDelta(_value);
    m->SetFrequency(_freq);
    m->SetExponent(_exp);
    shared_ptr<Module> p; p.reset(m);
    return p;
}




//{ "Module":"ScaleBias" , "Name": "mod_name" , "Src1": "mod_name" , "Bias": 0.0 , "Scale": 0.0 },
shared_ptr<Module> ModuleDescriptor::makeScaleBias() {
    ScaleBias* m = new ScaleBias();
    m->SetBias(_bias);
    m->SetScale(_scale);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Terrace" , "Name": "mod_name" , "Src1": "mod_name" , "Cpoints": [[0.0,1.0],[1.0,1.1]] , "Invert": true },
shared_ptr<Module> ModuleDescriptor::makeTerrace() {
    Terrace* m = new Terrace();
    std::tuple<double,double> t;
    for (auto t:  _cPoints) {
        m->AddControlPoint(std::get<0>(t));
        //m->AddControlPoint<(std::get<0>(t),std::get<1>(t));

    }
    m->InvertTerraces(this->_invert);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Turbulence" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Src1": "mod_name" , "Pow": 0.0 , "Rough": 0.0 }]
shared_ptr<Module> ModuleDescriptor::makeTurbulence() {
    Turbulence* m = new Turbulence();
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->SetFrequency(_freq);
    m->SetPower(_pow);
    m->SetRoughness(_rough);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Turbulence" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Src1": "mod_name" , "Pow": 0.0 , "Rough": 0.0 }]
shared_ptr<Module> ModuleDescriptor::makeTurbulence2() {
    Turbulence2* m = new Turbulence2();
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->SetFrequency(_freq);
    m->SetPower(_pow);
    m->SetRoughness(_rough);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Turbulence" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Src1": "mod_name" , "Pow": 0.0 , "Rough": 0.0 }]
shared_ptr<Module> ModuleDescriptor::makeTurbulenceBillow() {
    TurbulenceBillow * m = new TurbulenceBillow();
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->SetFrequency(_freq);
    m->SetPower(_pow);
    m->SetRoughness(_rough);
    shared_ptr<Module> p; p.reset(m);
    return p;
}

//{ "Module":"Turbulence" , "Name": "mod_name" , "Seed": 0.0 , "Freq": 0.0 , "Src1": "mod_name" , "Pow": 0.0 , "Rough": 0.0 }]
shared_ptr<Module> ModuleDescriptor::makeTurbulenceRidged() {
    TurbulenceRidged * m = new TurbulenceRidged();
    m->SetSeed(_seed != 0 ? _seed : SSGX::dx(999999));
    _actualSeed = m->GetSeed();
    m->SetFrequency(_freq);
    m->SetPower(_pow);
    m->SetRoughness(_rough);
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makeAdd() {
    Add* m = new Add();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

shared_ptr<Module> ModuleDescriptor::makeMax() {
    Max* m = new Max();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

shared_ptr<Module> ModuleDescriptor::makeMin() {
    Min* m = new Min();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

shared_ptr<Module> ModuleDescriptor::makeAvg() {
    Avg* m = new Avg();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

shared_ptr<Module> ModuleDescriptor::makeAvg4() {
    Avg4* m = new Avg4();
    shared_ptr<Module> p; p.reset(m);
    return p;
}

shared_ptr<Module> ModuleDescriptor::makeMultiply (){
    Multiply* m = new Multiply();
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makePower (){
    Power* m = new Power();
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makeBlend (){
    Blend* m = new Blend();
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makeSelect (){
    Select* m = new Select();
    if ( _lBound > _uBound)
    {
        std::string a("Module " + this->_name+ " cannot have its lower bound higher than upper bound");
        throw a;
    }
    m->SetBounds(_lBound,_uBound);
    m->SetEdgeFalloff(_value);
    shared_ptr<Module> p; p.reset(m);

    return p;
}

shared_ptr<Module> ModuleDescriptor::makeDisplace() {
    Displace* m = new Displace();
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makeRotatePoint() {
    RotatePoint* m = new RotatePoint();
    m->SetAngles(_x,_y,_z);
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makeScalePoint() {
    ScalePoint* m = new ScalePoint();
    m->SetXScale(_x);
    m->SetYScale(_y);
    m->SetZScale(_z);
    shared_ptr<Module> p; p.reset(m);
    return p;
}


shared_ptr<Module> ModuleDescriptor::makeTranslatePoint() {
    TranslatePoint* m = new TranslatePoint();
    m->SetXTranslation(_x);
    m->SetYTranslation(_y);
    m->SetZTranslation(_z);
    shared_ptr<Module> p; p.reset(m);
    return p;
}



