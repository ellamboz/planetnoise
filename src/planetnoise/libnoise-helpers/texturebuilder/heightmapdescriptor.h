/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#ifndef HEIGHTMAPDESCRIPTOR_H
#define HEIGHTMAPDESCRIPTOR_H

#include <tuple>
#include <noiseutils.h>
#include <ssg_structures.h>
#include "libnoise-helpers_global.h"
#include <memory>

using namespace std;


class LIBNOISEHELPERSSHARED_EXPORT HeightMapDescriptor
{
    std::string _name = "heightmap1";
public:
    std::string& name() { return _name; }
    void setName(const std::string& v) { _name = v; }
    HeightMapDescriptor();

    std::string pythonDeclaration() {
        return _name + "=ID.Create('"+_name+"');";
    }


    //from descriptor to actual object
    std::shared_ptr<utils::NoiseMap> makeNoiseMap() {
        std::shared_ptr<utils::NoiseMap> sp = make_shared<utils::NoiseMap>();
        return sp;
    }
    static std::shared_ptr<HeightMapDescriptor> create(std::string name) {
        auto p = std::make_shared<HeightMapDescriptor>();
        p->setName(name);
        return p;
    }
};

#endif // HEIGHTMAPDESCRIPTOR_H
