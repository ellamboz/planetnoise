#include <iostream>
#include <texturebuilder/texturebuilder.h>
#include <memory>
#include <fstream>
#include <ssg_structures.h>
#include <algorithm>
#include <getopt.h>
#include "nomen.h"
#include "presets.h"
#include <map>


using namespace std;
using namespace nlohmann;


void usage() {
    cerr << "planetnoise -t NEW|<preset_name>|.texjson file (default NEW             )" << endl;
    cerr << "            -p output_path                     (default .               )" << endl;
    cerr << "            -f image filename                  (default random-generated)" << endl;
    cerr << "            -n num (generate r images)         (default 1)" << endl;
    cerr << "            -w set width in pixels (height is width / 2 ) " << endl;
    cerr << "            -x Perlin|RidgedMulti|RidgedMulti2|Billow|Voronoi algorithm for first layer when random  " << endl;
    cerr << "            -y Perlin|RidgedMulti|RidgedMulti2|Billow|Voronoi algorithm for second layer when random  " << endl;
    cerr << "            -e Generate a pseudo-earthlike planet instead " << endl;
    cerr << "            -l <float> Set sea level between -0.9 (almost no sea) to 0.9 (waterworld)" << endl;
    cerr << "            -i <float> Set ice level between -0.9 (glacier world) to 0.9 (almost no ice)" << endl;
    cerr << "            -h this screen " << endl << endl;
    cerr << "Available presets: \n\n";
    int h = 1;
    for (auto& tpl : TEMPLATES) {
        cerr << tpl.first << "  ";
        h++;
        if (h % 6 == 0)
            cerr << endl;
    }
    cerr << endl;

    cerr << R"TX(

Sample usage:

# create a fully random planet texture and generates json file
$ planetnoise

# creates a planet texture using EARTHLIKE_REALISTIC template
$ planetnoise -t EARTHLIKE_REALISTIC

# creates a planet texture using an external json file, specifying output path and prefix name
$ planetnoise -t my_texture.texjson -p ./destination -n myprefix


)TX" << endl;

}

/// libnoise -t NEW|.texjson file (default NEW             )
///          -p output_path       (default .               )
///          -n filename          (default random-generated)
///          -h this screen
int main(int argc, char** argv) {
    srand(time(NULL));  // initialize clock
    string tpl= "NEW";
    string path = ".";
    string filename = Nomen::createWord();
    bool earthlike = false;
    string texture1 = "";
    string texture2 = "";
    double sealevel = 0.4;
    double icelevel = 0.8;
    int width = 2000;

    bool customFilename = false;
    vector<string> allowedItems = {"Perlin","RidgedMulti","RidgedMulti2","Billow","Voronoi"} ;
    int iterations = 1;
    char c;
    while ( (c = getopt (argc, argv, "t:p:n:f:x:y:l:i:w:he")) != -1) {
        switch(c) {
        case 't':
            if (optarg != nullptr) {
                string tmp(optarg);
                tpl = tmp;
            }
            break;
        case 'p':
            if (optarg != nullptr) {
                string tmp(optarg);
                path = tmp;
            }
            break;
        case 'n':
            if (optarg != nullptr) {
                string tmp(optarg);
                iterations = std::stoi(tmp);
                cerr  << "Passed " << iterations << endl;
            }
            break;
        case 'i':
            if (optarg != nullptr) {
                string tmp(optarg);
                icelevel = std::stod(tmp);
                cerr  << "Passed Ice Level " << icelevel << endl;
            }
            break;
        case 'w':
            if (optarg != nullptr) {
                string tmp(optarg);
                width = std::stoi(tmp);
                cerr << width << endl;
            }
            break;
        case 'l':
            if (optarg != nullptr) {
                string tmp(optarg);
                sealevel = std::stod(tmp);
                cerr  << "Passed Sealevel " << sealevel << endl;
            }
            break;
        case 'f':
            if (optarg != nullptr) {
                string tmp(optarg);
                filename = tmp;
		customFilename = true;
            }
            break;
        case 'x':
            if (optarg != nullptr) {
                string tmp(optarg);
                texture1 = tmp;
                if (std::find(allowedItems.begin(), allowedItems.end(), texture1) == allowedItems.end())
                {
                    cerr << "Texture1 type " << texture1 << " is not allowed, defaulting to random. " << endl;
                    texture1 = "";
                }                
            }
            break;
        case 'y':
            if (optarg != nullptr) {
                string tmp(optarg);
                texture2 = tmp;
                if (std::find(allowedItems.begin(), allowedItems.end(), texture1) == allowedItems.end())
                {
                    cerr << "Texture2 type " << texture2 << " is not allowed, defaulting to random. " << endl;
                    texture2 = "";
                }                
            }
            break;
        case 'e':
            earthlike = true;
            break;
        case 'h':
            usage();
            exit(0);
        }
    }

    cerr << "Creating " << iterations << " textures " << endl;

    srand(time(NULL));  // initialize clock
    for (auto its = 1; its <= iterations; its++ ) {
        cerr << "-------------------------------------------" << endl;
        cerr << "TEXTURE " << its << endl;
        cerr << "-------------------------------------------" << endl;
        if (tpl != "NEW") {
            if (TEMPLATES.find(tpl) != TEMPLATES.end()) {
                cerr << "----- predefined template ------" << endl;
                TextureBuilder tb = TextureBuilder{};
                tb.initialize();
                string data = TEMPLATES[tpl];
                tb.setOutputFileName(tpl+"_"+filename);
                tb.setOutputFolder(path);
                cerr << "Processing preset " << tpl << " to " << path << endl;
                tb.buildTextureFromJsonString(data, path);
                tb.setSize(width, width/2);
                cerr << "Built using template " << tpl << endl;
                cout << filename << "|" << tb.colorMap() << "|" << tb.bumpMap() << "|" << tb.reflectionMap() << "|" << tb.cloudMap() << endl;
            }
            else {
                cerr << "----- custom template ------" << endl;
                TextureBuilder tb = TextureBuilder{};
                tb.initialize();
                tb.setOutputFileName(filename);
                cerr << "Processing... " << tpl << " to " << path << endl;
                tb.buildTextureFromJson(tpl, path);
                tb.setSize(width, width/2);
                cerr << "That's all!" << endl;
                cout << filename << "|" << tb.colorMap() << "|" << tb.bumpMap() << "|" << tb.reflectionMap() << "|" << tb.cloudMap() << endl;
            }
        } else {
            try {
                if (!customFilename) { 
                            tpl = Nomen::createWord();
                            filename = tpl;
                } else {
                    tpl=filename;
                }
                if (iterations > 1) {
                    filename = filename + std::to_string(its);
                    tpl = filename;
                }
                cerr << "----- new template " << tpl << " ------" << endl;
                TextureBuilder tb = TextureBuilder::create(earthlike,texture1,texture2, sealevel,icelevel);
                tb.setSize(width, width/2);
                json j;
                tb.toJson(j);
                string textureFile = path+"/"+tpl+".texjson";
                ofstream o(textureFile);
                o << setw(4) << j << endl;
                tb.setOutputFolder(path);
                tb.setOutputFileName(filename);
                tb.buildTextureFromJson(textureFile,path);
                cerr << "That's all!" << endl;
                tpl = "NEW";
                cout << filename << "|" << tb.colorMap() << "|" << tb.bumpMap() << "|"  << tb.reflectionMap() << "|" << tb.cloudMap() << endl;
            }
            catch (const char* _data) {
                cerr << "ERROR! ERROR! ERROR! " << endl;
                cerr << _data;
            }
        }
    }
}

