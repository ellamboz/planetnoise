#include <windows.h>
#include <texturebuilder/texturebuilder.h>
#include <iostream>
#include <fstream>


using namespace std;
using namespace nlohmann;

extern "C" // only required if using g++
{
void __declspec(dllexport) __stdcall createParamTexture(
    bool bEarthlike, 
    const char * pszTexture1,
    const char * pszTexture2,
    double seaLevel,
    double iceLevel,
    int width,
    const char * pszPath, 
    const char *pszFilename, 
    char *szRes)
{
    srand (time(NULL));
    auto tex1 = std::string(pszTexture1);
    auto tex2 = std::string(pszTexture2);
    auto path = std::string(pszPath);
    auto filename = std::string(pszFilename);
    TextureBuilder tb;
    tb.initialize(bEarthlike, tex1, tex2, seaLevel, iceLevel);
    tb.setSize(width, width/2);
    json j;
    tb.toJson(j);
    string tpl = path+"/"+filename+".texjson";
    ofstream o(tpl);
    o << setw(4) << j << endl;
    tb.setOutputFolder(path);
    tb.setOutputFileName(filename);    
    tb.buildTextureFromJson(tpl, path);
    std::string result = filename + "|" + tb.colorMap() + "|" + tb.bumpMap() + "|"  + tb.reflectionMap() + "|" + tb.cloudMap() + "|";
    result.copy(szRes, result.length(), 0);
    return;
}

void __declspec(dllexport) __stdcall createTexture(const char* pszJsonFile, const char * pszPath, const char *pszFilename, char *szRes)
{
    srand (time(NULL));
    auto tpl = std::string(pszJsonFile);
    auto path = std::string(pszPath);
    auto filename = std::string(pszFilename);
    TextureBuilder tb = TextureBuilder{};
    tb.setOutputFileName(filename);    
    tb.buildTextureFromJson(tpl, path);
    std::string result = filename + "|" + tb.colorMap() + "|" + tb.bumpMap() + "|"  + tb.reflectionMap() + "|" + tb.cloudMap() + "|";
    result.copy(szRes, result.length(), 0);
    return;
}


BOOL __declspec (dllexport)  __stdcall DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

typedef void* CTextureBuilder;

CTextureBuilder __declspec(dllexport) __stdcall tb_new() 
{
    return reinterpret_cast<CTextureBuilder>(new TextureBuilder{});
}

CTextureBuilder __declspec(dllexport) __stdcall tb_newWithParam( bool bEarthlike, 
    const char * pszTexture1,
    const char * pszTexture2,
    double seaLevel,
    double iceLevel) 
{
    auto tex1 = std::string(pszTexture1);
    auto tex2 = std::string(pszTexture2);
    TextureBuilder* tb = new TextureBuilder();
    tb->initialize(bEarthlike, tex1, tex2, seaLevel, iceLevel);    
    return reinterpret_cast<CTextureBuilder>(tb);
}


void __declspec(dllexport) __stdcall tb_free(CTextureBuilder ptr) 
{
    delete reinterpret_cast<TextureBuilder *>(ptr);
}

void __declspec(dllexport) __stdcall tb_setOutputFileName(CTextureBuilder ptr, const char* pszFilename) 
{
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto filename = std::string(pszFilename);
    tb->setOutputFileName(filename);
     
}

void __declspec(dllexport) __stdcall tb_buildTextureFromJson(CTextureBuilder ptr, const char* pszTpl, const char* pszPath ) 
{
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto tpl = std::string(pszTpl);
    auto path = std::string(pszPath);
    tb->buildTextureFromJson(tpl, path);
}

void __declspec(dllexport) __stdcall tb_buildTextureFromJsonString(CTextureBuilder ptr, const char* pszData, const char* pszPath ) 
{
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto data = std::string(pszData);
    auto path = std::string(pszPath);
    tb->buildTextureFromJsonString(data, path);
}

//
void __declspec(dllexport) __stdcall tb_setSize(CTextureBuilder ptr, int xSize, int ySize)  {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setSize(xSize, ySize);
}


} // extern "C"
