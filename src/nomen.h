#ifndef NOMEN_H_INCLUDED
#define NOMEN_H_INCLUDED

#include <memory>
#include <map>
#include <vector>

using namespace std;


namespace Nomen {

    const string VOWELS="aeiouy";
    const string CONS="bcdfghjklmnprstvwxz";

    string v() {
        string res = "";
        res += VOWELS[rand() % VOWELS.length()];
        return res;
    }
    string c() {
        string res = "";
        res += CONS[rand() % CONS.length()];
        return res;
    }

    vector<string> dyp = {
        "ea","ea","ea","ea",
        "ei","ei","ai","ae",
        "ae","ai","ao","au",
        "ea","ei","eo","eu",
        "ia","ie","io","iu",
        "oa","oe","oi","ou",
        "ua","ue","ui","uo"
      };

    string dypt() {  return dyp[rand() % dyp.size()]; }

    string cv() { return  c()+ v(); }
    string vc() { return  v()+c(); }
    string vcv() { return  v()+c()+v(); }
    string cvc() { return c()+v()+c(); }

    vector<vector<int>> patterns = {
        {1,3},
        {1,3},
        {1,3},
        {2,4,6},
        {2,4,6},
        {2,4,-6},
        {1,1,6},
        {1,1,6},
        {1,1,-6},
        {1,1,-6,-7},
        {1,-3,-7},
        {1,-3,2},
        {1,-3,2},
        {1,-3,-2,-7},
        {1,5,2},
        {1,5,2},
        {1,5,-2,-7},
      };

	vector<string> cl = {
        "br","bb","bbl","bbr",
        "cr","cl","ccr",
        "dr","dd","dl",
        "fr","fl","ff","ffr","ffl",
        "gr","gg","ggr","ggl",
        "kr","ks",
        "lg","lf","lt","ll","lf","lp",
        "mb","mf","mm",
        "nd","nf","nt","ntr","ndr","ntl","ndl",
        "pp","pr","pl",
        "rt","rd","rs","rr","rg","rf","rp","rv",
        "ss","sr","sl",
        "tt","tr",
        "vr"};

    string getCl() {  return cl[rand() % cl.size()]; }

    bool roll (int odd, int res) { return rand() % odd == res; }
    string rollString (int odd, int res, string resTrue, string resFalse) { return roll(odd, res) ? resTrue : resFalse ; }


    string createWord() {
        string res = "";
        int pIdx = rand() % patterns.size();
        //print (pIdx);
        auto pattern = patterns[pIdx];
        for (int i : pattern ) {
          switch (i) {
            case -1: res += c()+ rollString (4, 2, dypt() , v()) ; break;
            case -2: res +=  rollString (4, 2, dypt() , v()) +c(); break;
            case -3: res += c() + rollString (4, 2, dypt() , v())  + c(); break;
            case -4: res += rollString (4, 2, dypt() , v())  + c() + rollString (6, 2, dypt() , v()) ; break;
            case -6: res += rollString(3,1,c(), getCl()); break;
            case -7: res += rollString (3, 2, dypt() , v()) ; break;
            case 1: res += cv(); break;
            case 2: res += vc(); break;
            case 3: res += cvc(); break;
            case 4: res += vcv(); break;
            case 5: res += getCl(); break;
            case 6: res += c(); break;
            case 7: res += v(); break;
            default: break;
          }
        };
        return res;
  }

}



#endif // NOMEN_H_INCLUDED
