# planetnoise

`planetnoise` is a library to generate planetary-like textures that can be wrapped around a sphere. Currently, 
it comes in two flavors:

* A Python module
* A standalone executable. 

While its earlier versions were written as part of a QT application, it's been extensively refactored and 
the standalone app needs no external libraries apart from those included in the project. 

These external libraries are included in the source tree:

* [Libnoise C++](http://libnoise.sourceforge.net/), _libnoise is a portable C++ library 
that is used to generate coherent noise, a type of smoothly-changing noise. libnoise can 
generate Perlin noise, ridged multifractal noise, and other types of coherent-noise._ 
The library is licensed under LGPL 2.1

* [Json C++](https://github.com/nlohmann/json), a JSON processing library, licensed under MIT License

* [STB](https://github.com/nothings/stb), a multipurpose, public domain library. 

**planetnoise** is licensed under LGPL 2.1 as well, since I modified `libnoise` a little.

To build the python module, you also need the Python development libraries
and [PyBind11](https://github.com/pybind/pybind11), a great python module building helper library. 


## Building `planetnoise`, the executable version. 

### Linux

Make sure you've got CMake and GCC/G++ installed. 

Then, all you've got to do is:

```
$ cd <source_root>/builds/exe
$ mkdir _build
$ cd _build
$ cmake .. -G "Unix Makefiles"
$ make
```

If you want to access `planetnoise` from anywhere in your system you must also

```
$ sudo cp planetnoise /usr/bin
```

### Windows (64-bit)

Make sure you've CMake and MinGW C/C++ installed. If you don't have MinGW C++, a good distro to download [can be found here.](https://nuwen.net/mingw.html) 

Then, all you've got to do is:

```
c:\ cd <source_root>
c:\ mkdir _build
c:\ cd _build
c:\ cmake .. -G "MinGW Makefiles"
c:\ make
```

In your build directory you'll find `planetnoise.exe`.


### Win32, MacOS

You're on your own, but I'm quite sure instruction for Linux will be OK for macOS folks, while Win32 people
will need to scavenge a MingW version for 32-bit and follow Windows building instructions. 

Since I'm not a MacOS expert, you might want to check notarization issues, although it seems that if you have 
access to a C++ compiler, building a command-line exe should be a non-issue. 

### Sample Usage

```
planetnoise -t NEW|<preset_name>|.texjson file (default NEW             )
            -p output_path                     (default .               )
            -f image filename                  (default random-generated)
            -n num (generate r images)         (default 1)
            -w set width in pixels (height is width / 2 )
            -x Perlin|RidgedMulti|RidgedMulti2|Billow|Voronoi algorithm for first layer when random
            -y Perlin|RidgedMulti|RidgedMulti2|Billow|Voronoi algorithm for second layer when random
            -e Generate a pseudo-earthlike planet instead
            -l <float> Set sea level between -0.9 (almost no sea) to 0.9 (waterworld)
            -i <float> Set ice level between -0.9 (glacier world) to 0.9 (almost no ice)
            -h this screen

Available presets:

ALIENPEAKSVORONOI  CANYON_01  CHUNK  CLOUD  DESERT
DESERT2  DESERT_CREAM  DESERT_REDDISH  EARTHLIKE_01  EARTHLIKE_01_1  EARTHLIKE_02
EARTHLIKE_03  EARTHLIKE_04  EARTHLIKE_10  EARTHLIKE_11  EARTHLIKE_12  EARTHLIKE_ISLAND
EARTHLIKE_REALISTIC  FUNKYCLOUD  GASGIANT21  GASGIANT22  GASGIANT2OK  GASGIANT3
GASGIANT4  GASGIANT5  GASGIANTOK  GASGIANTORIGINALOK  GLACIER  HOTHOUSE
ICEBALL  MULTILAYERED_EARTHLIKERND  OUTRE  OUTRE2  OUTRE4  POSTGARDEN
POSTGARDEN_3  PREGARDEN  PREGARDEN2  PREGARDEN3  ROCKBALL  ROCKBALL_02
ROCKBALL_03


Sample usage:

# create a fully random planet texture and generates json file
$ planetnoise

# creates a planet texture using EARTHLIKE_REALISTIC template
$ planetnoise -t EARTHLIKE_REALISTIC

# creates a planet texture using an external json file, specifying output path and prefix name
$ planetnoise -t my_texture.texjson -p ./destination -n myprefix

# create an earthlike planet, with about 30% land and poles, 2400x1200 pixel
$ planetnoise -e -l 0.2 -i 0.6 -w 2400


```

## Building the Python Module

Before building the python module, you must make sure that:

* You have a C++14-compliant compiler installed and available. 
* Since this module uses `pybind11` -- the high-level wizardry that makes building Python modules out of C++ classes easy -- 
and installing it using `pip` does not always work, I included it as a submodule, so you must checkout it this way:

After your first checkout:

~~~
git submodule update --init --recursive
~~~

If you want to update it later:

~~~
git submodule update --recursive
~~~


Once pybind has been included, you can build and install the module by launching these commands:

~~~
python setup.py build
python setup.py install
~~~

If you -- like me -- when on windows you're using MingW C++ compiler, you should build the module by running this command:

~~~
python setup.py build -cmingw32
~~~



After installing the module, you can go into the `examples/python` subdirectory and test the module on one of the examples included. 

## Status

Definitely Alpha-transitioning-to-Beta code. 

* **Executable** -- quite mature and usable
* **Python Module** -- working nicely, but lacking docs and maybe there are too much innards exposed in the Python layer. 

## Some FAQ

### Q: What kind of planetary map can I expect? 

Barring the old, bathymetric-style equirectangular map where a clever use of color palettes simulates a 
garden planet as seen from space (it's [python example 01](examples/python/example_01.py) if you WANT such a map), `planetnoise` can churn out this sort of thing ( [python example 03](examples/python/example_03.py), which uses three textures: one -- shaded -- for land area, one -- flat -- for the sea and lastly one for generating mountain ranges and snowcaps )

![Sample Planetary Map](images/Planet_03_Image.png)

Other examples will teach you how to produce not only color maps, but specular map and bump map. 
Check [this site](https://tilde.club/~maxlamboz/planets/) for details. 

### Q: Is the python module fast? 

It is **fast**. On an Ubuntu 18.04 machine, with an Intel i7-7500U your usual one-texture, bathymetric, 1024x512 map
gets generated in < 1sec. More complex and bigger textures need more time, but unless you want to generate A0-sized maps
(14000x7000 pixel) layered textures, 2048x1024 requires few seconds. This is thanks to `libnoise` being a full, C++ library.

There _are_ similar libraries written in python that take advantage of OpenCL, but if you don't have a high-end graphics
card to take advantage of, your source of number-crunching power is your main CPU, and you want to write planetary texture
generators in Python, **planetnoise** might just be one of the fastest choice out there. 





