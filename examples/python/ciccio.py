import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',   
	enableRandom=True, 
	freq=2.6493499279022217, 
	lac=3.3790719509124756, 
	oct=6, 
	pers=0.291269987821579, 
	seed=0)
CylinderPole=MD.Cylinders ( name='CylinderPole',   
	enableRandom=False, 
	freq=0.5)
ExtraLayer_0_Module=MD.Perlin ( name='ExtraLayer_0_Module',   
	enableRandom=True, 
	freq=2.6115000247955322, 
	lac=3.210239887237549, 
	oct=6, 
	pers=0.27015000581741333, 
	seed=0)
ExtraLayer_1_Module=MD.RidgedMulti2 ( name='ExtraLayer_1_Module',   
	enableRandom=True, 
	exp=2.0, 
	freq=2.284450054168701, 
	gain=2.0, 
	lac=2.9972479343414307, 
	oct=6, 
	seed=0)
MiddleLayer_Module=MD.RidgedMulti2 ( name='MiddleLayer_Module',   
	enableRandom=True, 
	exp=2.0, 
	freq=2.375349998474121, 
	gain=2.0, 
	lac=3.333695888519287, 
	oct=6, 
	seed=0)
RidgedBumpMapModule=MD.Perlin ( name='RidgedBumpMapModule',   
	enableRandom=False, 
	freq=6.3, 
	lac=3.6, 
	oct=8, 
	pers=0.3, 
	seed=0)
SeaModule=MD.Perlin ( name='SeaModule',   
	enableRandom=True, 
	freq=1.2750400000000002, 
	lac=3.5325, 
	oct=8, 
	pers=0.34334400000000004, 
	seed=0)
AATurboPoles=MD.Turbulence ( name='AATurboPoles',   
	enableRandom=False, 
	freq=4.1202, 
	pow=0.25317, 
	rough=3.2048500000000004, 
	seed=0, 
	src1=CylinderPole)
AATurboSea=MD.Turbulence ( name='AATurboSea',   
	enableRandom=False, 
	freq=0.6106, 
	pow=0.25284, 
	rough=1.1314, 
	seed=0, 
	src1=SeaModule)
RidgedBumpMapModule_e=MD.Exponent ( name='RidgedBumpMapModule_e',   
	enableRandom=False, 
	exp=1.25, 
	src1=RidgedBumpMapModule)
RidgedBumpMapModule_e_t=MD.Turbulence ( name='RidgedBumpMapModule_e_t',   
	enableRandom=False, 
	freq=0.75, 
	pow=0.05, 
	rough=1.25, 
	seed=0, 
	src1=RidgedBumpMapModule_e)

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_geo=ID.create('img_geo')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
ExtraLayer_0_heightMap=HMD.create('ExtraLayer_0_heightMap')
ExtraLayer_1_heightMap=HMD.create('ExtraLayer_1_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
heightMapPole=HMD.create('heightMapPole')
heightMapSea=HMD.create('heightMapSea')
bump_heightMap=HMD.create('bump_heightMap')
geo_heightMap=HMD.create('geo_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
ExtraLayer_0_noiseMapBuilder1 = NMB.create('ExtraLayer_0_noiseMapBuilder1', module=ExtraLayer_0_Module, heightMap=ExtraLayer_0_heightMap)
ExtraLayer_1_noiseMapBuilder1 = NMB.create('ExtraLayer_1_noiseMapBuilder1', module=ExtraLayer_1_Module, heightMap=ExtraLayer_1_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
heightMapBuilderSea = NMB.create('heightMapBuilderSea', module=AATurboSea, heightMap=heightMapSea)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=RidgedBumpMapModule_e_t, heightMap=bump_heightMap)
noiseMapBuilderGeo = NMB.create('noiseMapBuilderGeo', module=MiddleLayer_Module, heightMap=geo_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=MiddleLayer_Module, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (name='BeginLayer_renderer',  
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 57, 55, 54, 255 ), 
		( -0.33333333333333337, 50, 52, 49, 255 ), 
		( 0.33333333333333326, 56, 53, 52, 255 ), 
		( 0.99, 51, 52, 50, 255 ), 
		( 1.0, 120, 198, 94, 255 )], 
	heightMap=BeginLayer_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

MiddleLayer_renderer=RD.create (name='MiddleLayer_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 52, 51, 49, 16 ), 
		( -0.6666666666666667, 52, 50, 48, 16 ), 
		( -0.3333333333333334, 55, 53, 51, 10 ), 
		( -1.1102230246251565e-16, 55, 53, 51, 1 ), 
		( 0.3333333333333332, 52, 50, 48, 14 ), 
		( 0.6666666666666665, 54, 52, 50, 32 ), 
		( 0.9999999999999998, 57, 54, 52, 192 ), 
		( 1.0, 126, 210, 31, 255 )], 
	heightMap=MiddleLayer_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

ExtraLayer_0_renderer=RD.create (name='ExtraLayer_0_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 96, 94, 55, 10 ), 
		( -0.6666666666666667, 98, 94, 57, 19 ), 
		( -0.3333333333333334, 97, 91, 56, 19 ), 
		( -1.1102230246251565e-16, 94, 86, 55, 22 ), 
		( 0.3333333333333332, 94, 85, 54, 23 ), 
		( 0.6666666666666665, 93, 82, 52, 30 ), 
		( 0.9999999999999998, 92, 79, 50, 21 ), 
		( 1.0, 228, 114, 222, 255 )], 
	heightMap=ExtraLayer_0_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

ExtraLayer_1_renderer=RD.create (name='ExtraLayer_1_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 55, 56, 51, 2 ), 
		( -0.6666666666666667, 58, 60, 54, 4 ), 
		( -0.3333333333333334, 59, 62, 56, 25 ), 
		( -1.1102230246251565e-16, 55, 58, 52, 192 ), 
		( 0.3333333333333332, 57, 61, 53, 20 ), 
		( 0.6666666666666665, 57, 62, 53, 18 ), 
		( 0.9999999999999998, 56, 62, 53, 26 ), 
		( 1.0, 224, 10, 223, 255 )], 
	heightMap=ExtraLayer_1_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

snow_renderer=RD.create (name='snow_renderer',  
	backgroundImage=BaseImage, 
	destImage=BaseImage, 
	enabledLight=True, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( -0.4, 0, 0, 0, 0 ), 
		( -0.23, 255, 255, 255, 0 ), 
		( 0.0, 255, 255, 255, 128 ), 
		( 0.23, 255, 255, 255, 0 ), 
		( 0.6, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	heightMap=bump_heightMap, 
	lightBrightness=2.1, 
	lightContrast=2.1, 
	randomGradient=False)

GrassLayer_renderer=RD.create (name='GrassLayer_renderer',  
	backgroundImage=BaseImage, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 114, 111, 68, 0 ), 
		( -0.71, 93, 105, 53, 0 ), 
		( -0.57, 74, 75, 65, 0 ), 
		( -0.42, 76, 84, 47, 0 ), 
		( -0.28, 46, 51, 19, 141 ), 
		( -0.039, 46, 51, 19, 140 ), 
		( 0.21, 76, 84, 47, 140 ), 
		( 0.57, 103, 131, 34, 9 ), 
		( 0.6, 73, 73, 48, 0 ), 
		( 1.0, 99, 168, 165, 0 )], 
	heightMap=BeginLayer_heightMap, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

DesertLayer_renderer=RD.create (name='DesertLayer_renderer',  
	backgroundImage=BaseImage, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 254, 238, 197, 0 ), 
		( -0.93, 237, 235, 212, 0 ), 
		( -0.87, 223, 214, 183, 30 ), 
		( -0.81, 228, 168, 134, 75 ), 
		( -0.75, 237, 235, 212, 45 ), 
		( -0.68, 224, 220, 56, 0 ), 
		( 1.0, 99, 168, 165, 0 )], 
	heightMap=heightMapPole, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

RendererSea=RD.create (name='RendererSea',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 20, 36, 101, 255 ), 
		( 0.23357000000000003, 23, 50, 115, 255 ), 
		( 0.24857, 26, 58, 123, 255 ), 
		( 0.25857, 34, 62, 145, 255 ), 
		( 0.25867, 201, 206, 196, 128 ), 
		( 0.36617, 201, 206, 196, 64 ), 
		( 0.46617, 123, 173, 114, 32 ), 
		( 0.71617, 221, 216, 209, 0 ), 
		( 1.0, 255, 255, 255, 0 )], 
	heightMap=heightMapSea, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

RendererPole=RD.create (name='RendererPole',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( 0.214768, 230, 240, 250, 0 ), 
		( 0.2168095, 248, 254, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	heightMap=heightMapPole, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

bumpmap_renderer=RD.create (name='bumpmap_renderer',  
	destImage=img_bump, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( -0.4, 0, 0, 0, 255 ), 
		( 0.0, 255, 255, 255, 255 ), 
		( 0.6, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	heightMap=bump_heightMap, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

bumpmap_renderer_mask=RD.create (name='bumpmap_renderer_mask',  
	backgroundImage=img_bump, 
	destImage=img_bump, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( 0.36667, 0, 0, 0, 255 ), 
		( 0.51617, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	heightMap=heightMapSea, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

specmap_renderer=RD.create (name='specmap_renderer',  
	destImage=img_spec, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	heightMap=spec_heightMap, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

specmap_renderer_spec=RD.create (name='specmap_renderer_spec',  
	backgroundImage=img_spec, 
	destImage=img_spec, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.25856999, 255, 255, 255, 255 ), 
		( 0.25857, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	heightMap=heightMapSea, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

geo_renderer=RD.create (name='geo_renderer',  
	destImage=img_geo, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	heightMap=geo_heightMap, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

geomap_renderer_geo=RD.create (name='geomap_renderer_geo',  
	backgroundImage=img_geo, 
	destImage=img_geo, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.25856999, 255, 255, 255, 255 ), 
		( 0.25857, 0, 0, 0, 255 ), 
		( 0.25907, 224, 224, 224, 255 ), 
		( 0.30957, 224, 224, 224, 255 ), 
		( 1.0, 224, 224, 224, 255 )], 
	heightMap=heightMapSea, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)

ExtraLayer_0_renderer.setRandomFactor(20,40,60)

ExtraLayer_1_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([AATurboPoles,AATurboSea,BeginLayer_Module,CylinderPole,ExtraLayer_0_Module,ExtraLayer_1_Module,MiddleLayer_Module,RidgedBumpMapModule,RidgedBumpMapModule_e,RidgedBumpMapModule_e_t,SeaModule])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_geo,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,ExtraLayer_0_heightMap,ExtraLayer_1_heightMap,MiddleLayer_heightMap,heightMapPole,heightMapSea,bump_heightMap,geo_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,ExtraLayer_0_noiseMapBuilder1,ExtraLayer_1_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,heightMapBuilderPole,heightMapBuilderSea,noiseMapBuilderBump,noiseMapBuilderGeo,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,ExtraLayer_0_renderer,ExtraLayer_1_renderer,snow_renderer,GrassLayer_renderer,DesertLayer_renderer,RendererSea,RendererPole,bumpmap_renderer,bumpmap_renderer_mask,specmap_renderer,specmap_renderer_spec,geo_renderer,geomap_renderer_geo])     # finally, lets add the renderer

texture.outputFileName = "Texture_ciccio"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


