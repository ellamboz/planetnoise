import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB
import utils.gradients

# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()
texture.setSize(2048,1024)

# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = texture.Perlin( name="Perlin", seed=102, freq=1.2,lac=1.8, pers=0.6,oct=8)

# let's build another Perlin Descriptor for sea:
# take care to use a different name from other modules!
perlin_sea = texture.Perlin( name="PerlinSea", seed=202, freq=1.2,lac=1.8, pers=0.6,oct=8)

# Let's also build a Ridged Multifractal descriptor. Its jagged appearance will be useful 
# to create mountain ranges. 
rmf_mountains = texture.RidgedMulti( name="Ridged", seed=302, freq=3.9, lac=1.9, oct=8)

# ...since RidgedMultiFractal sometimes produces ranges that follow unnaturally neat curves, 
# let's perturbate it with a Turbulence module. src1 parameter is the module to be perturbated.
perturbed_rmf = texture.Turbulence(name="RidgedTurbo", src1=rmf_mountains, seed=401, freq=1.0, pow=0.125, rough=5.1)

pole = texture.Cylinders("Pole", 0.5)
perturbed_pole = texture.Turbulence(name="PoleTurbo", src1=pole, seed=501, freq=4.0, pow=0.25, rough=5.1)

billow = texture.Billow( name="BillowCloud", seed=401, freq=1.35,lac=3.7,pers=0.34,oct=5)
voronoi = texture.Voronoi(name="VoronoiCloud", seed=401, freq=1.36, disp=0.0, enableDispl = False)


clouds = texture.Displace("Clouds", billow, perlin_sea, voronoi, perlin, perlin)

# then the image descriptor, which will hold rendered image data
# we STILL need one image. 
image = texture.Image("Image")

# we also need two images, one for the bump, one for the specular map
imageBump = texture.Image("Bump")
imageSpec = texture.Image("Spec")

# then for the clouds
imageCloud = texture.Image("Clouds")

# then the heightmap, which will hold heightmap data
heightmap = texture.HeightMap("Heightmap")

# we need an heightmap for sea, too. 
heightmapSea = texture.HeightMap("HeightmapSea")

#and one for the mountains
heightmapRange = texture.HeightMap("HeightmapRange")

#and one for the poles
heightmapPoles = texture.HeightMap("HeightmapPoles")

#and one for the clouds
heightmapClouds = texture.HeightMap("HeightmapClouds")



# connect the noise module with heightmap
heightmapBuilder = texture.NoiseMapBuilder("hmbuilder", module=perlin, heightMap=heightmap)

# we also need a hm builder for sea. 
heightmapBuilderSea = texture.NoiseMapBuilder("hmbuilderSea", module=perlin_sea, heightMap=heightmapSea)

# ... and one for mountain ranges. Notice we used the PERTURBED ridged multi module.
heightmapBuilderRanges = texture.NoiseMapBuilder("hmbuilderRanges", module=perturbed_rmf, heightMap=heightmapRange)

# ... and one for poles. Make sure to bind the perturbed_pole module!
heightmapBuilderPoles = texture.NoiseMapBuilder("hmbuilderPoles", module=perturbed_pole, heightMap=heightmapPoles)

# ... and one for poles. Make sure to bind the perturbed_pole module!
heightmapBuilderCloud = texture.NoiseMapBuilder("hmbuilderClouds", module=clouds, heightMap=heightmapClouds)



# this is a simple gradient we'll use to render land areas.
# we remove ice, since we'll use rmf_mountains ridged multifractal
# noise as a basis for our mountain ranges. 
land_gradient = utils.gradients.evolve_planet_gradient(5)

# this is a sea gradient. It's sea blue until 55%, then there's a little sand strip
# that becomes transparent. We'll use it to generate a blue map with a cutout area that makes for 
# the continents' profiles. 
sea_gradient = [
    (-1.0  , 20, 20, 80,255),    
    (-0.1  , 30, 30,128,255),
    ( 0.250  , 40, 40,144,255),
    ( 0.251,255,255,192,255),
    ( 0.410,255,255,192,  0),
    ( 1.0  ,255,255,192,  0),
]

# this is the gradient we're using for ice. Only higest peak will be white
ice_gradient = [
    (-1.0  ,255,255,255,   0),
    ( 0.4  ,255,255,255,   0),
    ( 0.42 ,255,255,255,  0),
    ( 0.78 ,255,255,255, 48),
    ( 0.85 ,255,255,255, 128),
    ( 0.95 ,255,255,255, 192),
    ( 1.00 ,255,255,255, 225),
]


pole_gradient =   [
    (-1.0  ,255,255,255,   0),
    ( 0.58 ,255,255,255,   0),
    ( 0.59 ,255,255,255, 255),
    ( 1.00 ,255,255,255, 225),
]

# this is the gradient we're using as a basis for our bump map
bump_gradient = [
    (-1.0  ,  0,  0,  0, 255),
    #( 0.252,  0,  0,  0, 255),
    ( 1.00 ,255,255,255, 225)
]

blacksea_gradient = [
    (-1.0  ,   0,  0,  0,255),    
    ( 0.251,   0,  0,  0,255),
    ( 0.410,   0,  0,  0,  0),
    ( 1.0  ,255,255,192,  0),
]

spec_gradient = [
    ( -1.0  , 255,255,255,255),    
    (  0.251, 255,255,255,255),
    (  0.252,   0,  0,  0,255),
    (    1.0  ,   0,  0,  0,255),
]


white_gradient = [
     (-1.00 ,255,255,255, 255),
     ( 1.00 ,255,255,255, 255)
]

cloud_gradient = [
    ( -1, 0, 0, 0, 255 ),
    ( -0.95, 0, 0, 0, 255 ),
    ( -0.7429111531190926, 128, 128, 128, 255 ),
    ( -0.3534971644612477, 0, 0, 0, 255 ),
    ( 0.3253308128544424, 215, 218, 218, 255 ),
    ( 0.6483931947069943, 232, 232, 232, 255 ),
    ( 0.8525519848771266, 240, 240, 240, 255 ),
    ( 1, 240, 240, 240, 255 )
]


# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = texture.Renderer(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo = land_gradient,
    bumpMap = heightmapRange,   # we're using another heightmap as a bumpmap
    destImage=image,
    lightContrast = 1.0,
    enabledLight= False)

# after the land, snow
renderer_mnt  = texture.Renderer(name="renderer_mnt", 
    heightMap=heightmapRange,       # name of the heightmap you want to render
    gradientInfo = ice_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 1.4,
    enabledLight= False)


# then sea covers all: we'll use the sea gradient to create a "sea overlay" on the map. 
# this will make our world map slightly more natural, since land and sea have their own generator.
renderer_sea  = texture.Renderer(name="renderer_sea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = sea_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)

renderer_pole  = texture.Renderer(name="renderer_pole", 
    heightMap=heightmapPoles,       # name of the heightmap you want to render
    gradientInfo = pole_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)

# now let's render the bumpmap
renderer_bump_1  = texture.Renderer(name="renderer_bump", 
    heightMap=heightmapRange,       # name of the heightmap you want to render
    gradientInfo = bump_gradient,
    destImage=imageBump,
    lightContrast = 1.4,
    enabledLight= False)

renderer_bump_2  = texture.Renderer(name="renderer_bump_sea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = blacksea_gradient,
    destImage=imageBump,
    backgroundImage= imageBump,
    lightContrast = 2.0,
    enabledLight= False)


renderer_spec  = texture.Renderer(name="renderer_specsea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = spec_gradient,
    destImage=imageSpec,
    lightContrast = 2.0,
    enabledLight= False)


renderer_cloud  = texture.Renderer(name="renderer_cloud", 
    heightMap=heightmapClouds,       # name of the heightmap you want to render
    gradientInfo = cloud_gradient,
    destImage=imageCloud,
    lightContrast = 2.0,
    enabledLight= False)

renderer_cloud_2  = texture.Renderer(name="renderer_cloud_2", 
    heightMap=heightmapClouds,       # name of the heightmap you want to render
    gradientInfo = white_gradient,
    alphaImage= imageCloud,
    destImage=imageCloud,
    lightContrast = 2.0,
    enabledLight= False)    






jsonTexture = texture.jsonString()
with open ("Planet_09.texjson","w") as f:
    f.write(jsonTexture)

texture.outputFileName = "Planet_09"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

