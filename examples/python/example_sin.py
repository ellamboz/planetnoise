# How to build a texture:

# To build a texture you operate this way:

# First you build and connect one or more modules. 

# Modules can be:
# - Noise generators: these generate the fractals that will make up your world
# - Transformers: these modules transform the output of another module (for instance, Turbulence)
# - Combiners: these modules process the input of two or more modules ( for instance, min, max, average)

# Then you create the images descriptors, that will store binary image data. You can create and render 
# more than one image with a single TextureBuilder

# Then you create the heightmaps, that store modules' output in terms of height level, and range from
# -1.0 to 1.0

# After that, you connect heightmaps to modules by creating heightmap builders.

# You then must render heightmaps to images: to do this, you create Renderers, that connect heightmap 
# to images and render them using custom gradients. This way, you can render not only eartlike maps, but
# marslike maps, jupiterlike maps.

# Moreover, renderers are stackable: you can have a renderer write its heightmap data on a background image
# descriptor. This is the key to get realistic planetary maps. 

# After you are done creating these descriptor, you create a TextureBuilder. It is the object that binds all 
# this stuff together and outputs planetary maps. 

# You append these objects to TextureBuilder, and tell it to generate the textures. 

# Follow these examples to get a feel of planetnoise lib. 

import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

# let's build our Texture Object
texture = planetnoise.TextureBuilder()


# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = texture.RidgedMulti2( name="Perlin", 
    seed=110, 
    freq=0.85,
    lac=3.8, 
    oct=8,
    gain=0.9,
    offset=-10.066, 
    exp=0.3  
)
perlin_sin = texture.Exp(name="PerlinSin", src1=perlin, exp=3.0)
curve = MD.Curve(name="curve1", src1=perlin_sin, cpoints=[
    (-1.0, 1.0),
    (-0.5, -0.5),
    (0.5, 0.5),
    (0.95, 1.0),
    (1.0, -1.0)
])

# then the image descriptor, which will hold rendered image data
image = texture.Image("Image")

# then the heightmap, which will hold heightmap data
heightmap = texture.HeightMap("Heightmap")


# connect the noise module with heightmap
heightmapBuilder = texture.NoiseMapBuilder("hmbuilder", module=perlin_sin, heightMap=heightmap)

# this is a simple gradient we'll use to render land areas:
planet_gradient = [                
    (-1.0  , 20, 20, 80,255),    
    (-0.7  , 30, 30,128,255),
    (-0.01  , 40, 40,144,255),
    ( 0.0,255,255,192,255),
    ( 0.31,  0,128,  0,255),
    ( 0.99999,100, 50,  0,255),
    ( 1.0 ,255,255,255,255)
]


# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = texture.Renderer(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo=planet_gradient,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= True)


texture.outputFileName = "Planet_sin"
texture.appendModuleDescriptor(curve)

print (texture.jsonString()) 
texture.buildTextureFromJsonString(texture.jsonString(), ".")

