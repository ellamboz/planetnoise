import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',  
	enableRandom=True, 
	lac=3.43072009087, 
	pers=0.304758012295, 
	seed=0, 
	freq=2.37814998627, 
	oct=6)
CylinderPole=MD.Cylinders ( name='CylinderPole',  
	freq=0.5, 
	enableRandom=False)
EndLayer_Module=MD.RidgedMulti2 ( name='EndLayer_Module',  
	enableRandom=True, 
	lac=3.43167996407, 
	seed=0, 
	gain=2.0, 
	exp=2.0, 
	offset=0.0, 
	freq=2.29839992523, 
	oct=6)
ExtraLayer_0_Module=MD.Billow ( name='ExtraLayer_0_Module',  
	enableRandom=True, 
	lac=3.27660799026, 
	pers=0.324665993452, 
	seed=0, 
	freq=2.72239995003, 
	oct=6)
MiddleLayer_Module=MD.Perlin ( name='MiddleLayer_Module',  
	enableRandom=True, 
	lac=3.44192004204, 
	pers=0.321101993322, 
	seed=0, 
	freq=2.41199994087, 
	oct=6)
PenLayer_Module=MD.Billow ( name='PenLayer_Module',  
	enableRandom=True, 
	lac=3.38303995132, 
	pers=0.322403997183, 
	seed=0, 
	freq=2.64404988289, 
	oct=6)
AATurboPoles=MD.Turbulence ( name='AATurboPoles',  
	src1=CylinderPole, 
	pow=0.32169, 
	enableRandom=False, 
	seed=0, 
	freq=5.3394, 
	rough=3.4211)
AvgBumpMapModule=MD.Avg4 ( name='AvgBumpMapModule',  
	enableRandom=False, 
	src2=MiddleLayer_Module, 
	src3=PenLayer_Module, 
	src1=BeginLayer_Module, 
	src4=EndLayer_Module)
AvgBumpMapModule_e=MD.Exponent ( name='AvgBumpMapModule_e',  
	src1=AvgBumpMapModule, 
	enableRandom=False, 
	exp=1.25)
AvgBumpMapModule_e_t=MD.Turbulence ( name='AvgBumpMapModule_e_t',  
	src1=AvgBumpMapModule_e, 
	pow=0.1783, 
	enableRandom=False, 
	seed=0, 
	freq=5.7238, 
	rough=4.4832)

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
EndLayer_heightMap=HMD.create('EndLayer_heightMap')
ExtraLayer_0_heightMap=HMD.create('ExtraLayer_0_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
PenLayer_heightMap=HMD.create('PenLayer_heightMap')
heightMapPole=HMD.create('heightMapPole')
bump_heightMap=HMD.create('bump_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
EndLayer_noiseMapBuilder1 = NMB.create('EndLayer_noiseMapBuilder1', module=EndLayer_Module, heightMap=EndLayer_heightMap)
ExtraLayer_0_noiseMapBuilder1 = NMB.create('ExtraLayer_0_noiseMapBuilder1', module=ExtraLayer_0_Module, heightMap=ExtraLayer_0_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
PenLayer_noiseMapBuilder1 = NMB.create('PenLayer_noiseMapBuilder1', module=PenLayer_Module, heightMap=PenLayer_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=AvgBumpMapModule_e_t, heightMap=bump_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=EndLayer_Module, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 57, 97, 98, 255 ), 
		( -0.5, 99, 52, 52, 255 ), 
		( 0.0, 56, 57, 53, 255 ), 
		( 0.5, 101, 99, 98, 255 ), 
		( 1.0, 111, 168, 188, 255 )], 
	lightContrast=1.4, 
	heightMap=BeginLayer_heightMap, 
	enabledLight=False)

MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 58, 55, 54, 2 ), 
		( -0.714285714286, 58, 54, 53, 19 ), 
		( -0.428571428571, 61, 57, 56, 14 ), 
		( -0.142857142857, 61, 57, 56, 192 ), 
		( 0.142857142857, 58, 54, 53, 17 ), 
		( 0.428571428571, 60, 56, 55, 6 ), 
		( 0.714285714286, 63, 58, 57, 192 ), 
		( 1.0, 124, 131, 139, 255 )], 
	lightContrast=1.4, 
	heightMap=MiddleLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

PenLayer_renderer=RD.create (  name='PenLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 52, 49, 48, 16 ), 
		( -0.714285714286, 54, 51, 50, 192 ), 
		( -0.428571428571, 53, 50, 49, 24 ), 
		( -0.142857142857, 50, 47, 46, 4 ), 
		( 0.142857142857, 50, 46, 45, 4 ), 
		( 0.428571428571, 49, 44, 43, 14 ), 
		( 0.714285714286, 48, 42, 41, 12 ), 
		( 1.0, 139, 15, 124, 255 )], 
	lightContrast=1.4, 
	heightMap=PenLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

ExtraLayer_0_renderer=RD.create (  name='ExtraLayer_0_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 51, 52, 49, 1 ), 
		( -0.714285714286, 54, 56, 52, 192 ), 
		( -0.428571428571, 55, 58, 54, 7 ), 
		( -0.142857142857, 51, 54, 49, 13 ), 
		( 0.142857142857, 53, 57, 50, 12 ), 
		( 0.428571428571, 53, 58, 50, 32 ), 
		( 0.714285714286, 53, 58, 50, 192 ), 
		( 1.0, 54, 182, 223, 255 )], 
	lightContrast=1.4, 
	heightMap=ExtraLayer_0_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

EndLayer_renderer=RD.create (  name='EndLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 51, 50, 49, 255 ), 
		( -0.714285714286, 54, 52, 51, 255 ), 
		( -0.428571428571, 58, 55, 54, 255 ), 
		( -0.142857142857, 59, 55, 54, 0 ), 
		( 0.142857142857, 58, 54, 53, 0 ), 
		( 0.428571428571, 58, 54, 53, 0 ), 
		( 0.714285714286, 55, 51, 50, 0 ), 
		( 1.0, 198, 122, 109, 0 )], 
	lightContrast=1.4, 
	heightMap=EndLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=bump_heightMap, 
	enabledLight=False)

specmap_renderer=RD.create (  name='specmap_renderer',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=spec_heightMap, 
	enabledLight=False)

RendererPole=RD.create (  name='RendererPole',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( 0.918822, 230, 240, 250, 0 ), 
		( 0.919233, 248, 254, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)

PenLayer_renderer.setRandomFactor(20,40,60)

ExtraLayer_0_renderer.setRandomFactor(20,40,60)

EndLayer_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([AATurboPoles,AvgBumpMapModule,AvgBumpMapModule_e,AvgBumpMapModule_e_t,BeginLayer_Module,CylinderPole,EndLayer_Module,ExtraLayer_0_Module,MiddleLayer_Module,PenLayer_Module])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,EndLayer_heightMap,ExtraLayer_0_heightMap,MiddleLayer_heightMap,PenLayer_heightMap,heightMapPole,bump_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,EndLayer_noiseMapBuilder1,ExtraLayer_0_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,PenLayer_noiseMapBuilder1,heightMapBuilderPole,noiseMapBuilderBump,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,PenLayer_renderer,ExtraLayer_0_renderer,EndLayer_renderer,bumpmap_renderer,specmap_renderer,RendererPole])     # finally, lets add the renderer

texture.outputFileName = "Texture_planet03"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


