import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.Billow ( name='BeginLayer_Module',   
	enableRandom=True, 
	lac=2.88774394989, 
	pers=0.305759996176, 
	seed=0, 
	freq=2.38464999199, 
	oct=6)
CylinderPole=MD.Cylinders ( name='CylinderPole',   
	freq=0.5, 
	enableRandom=False)
ExtraLayer_0_Module=MD.Perlin ( name='ExtraLayer_0_Module',   
	enableRandom=True, 
	lac=3.20435190201, 
	pers=0.284357994795, 
	seed=0, 
	freq=2.53570008278, 
	oct=6)
MiddleLayer_Module=MD.Billow ( name='MiddleLayer_Module',   
	enableRandom=True, 
	lac=2.88287997246, 
	pers=0.270720005035, 
	seed=0, 
	freq=2.71574997902, 
	oct=6)
RidgedBumpMapModule=MD.Perlin ( name='RidgedBumpMapModule',   
	enableRandom=False, 
	lac=3.6, 
	pers=0.3, 
	seed=0, 
	freq=6.3, 
	oct=8)
SeaModule=MD.Perlin ( name='SeaModule',   
	enableRandom=True, 
	lac=3.87075, 
	pers=0.316152, 
	seed=0, 
	freq=1.21124, 
	oct=8)
AATurboPoles=MD.Turbulence ( name='AATurboPoles',   
	src1=CylinderPole, 
	pow=0.2685, 
	enableRandom=False, 
	seed=0, 
	freq=5.3162, 
	rough=2.9572)
AATurboSea=MD.Turbulence ( name='AATurboSea',   
	src1=SeaModule, 
	pow=0.21232, 
	enableRandom=False, 
	seed=0, 
	freq=0.5264, 
	rough=1.1976)
RidgedBumpMapModule_e=MD.Exponent ( name='RidgedBumpMapModule_e',   
	src1=RidgedBumpMapModule, 
	exp=1.4)
RidgedBumpMapModule_e_t=MD.Turbulence ( name='RidgedBumpMapModule_e_t',   
	src1=RidgedBumpMapModule_e, 
	pow=0.05, 
	enableRandom=False, 
	seed=0, 
	freq=0.75, 
	rough=1.25)

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_geo=ID.create('img_geo')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
ExtraLayer_0_heightMap=HMD.create('ExtraLayer_0_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
heightMapPole=HMD.create('heightMapPole')
heightMapSea=HMD.create('heightMapSea')
bump_heightMap=HMD.create('bump_heightMap')
geo_heightMap=HMD.create('geo_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
ExtraLayer_0_noiseMapBuilder1 = NMB.create('ExtraLayer_0_noiseMapBuilder1', module=ExtraLayer_0_Module, heightMap=ExtraLayer_0_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
heightMapBuilderSea = NMB.create('heightMapBuilderSea', module=AATurboSea, heightMap=heightMapSea)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=RidgedBumpMapModule_e_t, heightMap=bump_heightMap)
noiseMapBuilderGeo = NMB.create('noiseMapBuilderGeo', module=MiddleLayer_Module, heightMap=geo_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=MiddleLayer_Module, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (name='BeginLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 50, 51, 49, 255 ), 
		( -0.333333333333, 52, 51, 49, 255 ), 
		( 0.333333333333, 57, 53, 52, 255 ), 
		( 0.99999999, 51, 52, 50, 255 ), 
		( 1.0, 114, 146, 22, 255 )], 
	lightContrast=1.4, 
	heightMap=BeginLayer_heightMap, 
	enabledLight=False)

MiddleLayer_renderer=RD.create (name='MiddleLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 99, 52, 52, 19 ), 
		( -0.666666666667, 99, 51, 50, 32 ), 
		( -0.333333333333, 102, 53, 52, 7 ), 
		( -1.11022302463e-16, 102, 54, 52, 32 ), 
		( 0.333333333333, 99, 53, 51, 9 ), 
		( 0.666666666667, 101, 53, 52, 192 ), 
		( 0.99999999, 104, 54, 53, 12 ), 
		( 1.0, 139, 112, 212, 255 )], 
	lightContrast=1.4, 
	heightMap=MiddleLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

ExtraLayer_0_renderer=RD.create (name='ExtraLayer_0_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 96, 94, 55, 192 ), 
		( -0.666666666667, 98, 94, 57, 13 ), 
		( -0.333333333333, 97, 91, 56, 8 ), 
		( -1.11022302463e-16, 94, 86, 55, 26 ), 
		( 0.333333333333, 94, 85, 54, 22 ), 
		( 0.666666666667, 93, 82, 52, 192 ), 
		( 0.9999999, 92, 79, 50, 192 ), 
		( 1.0, 153, 65, 71, 255 )], 
	lightContrast=1.4, 
	heightMap=ExtraLayer_0_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

snow_renderer=RD.create (name='snow_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.1, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( -0.4, 0, 0, 0, 0 ), 
		( -0.23, 255, 255, 255, 0 ), 
		( 0.0, 255, 255, 255, 128 ), 
		( 0.23, 255, 255, 255, 0 ), 
		( 0.6, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	lightContrast=2.1, 
	heightMap=bump_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=True)

RendererSea=RD.create (name='RendererSea',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 20, 36, 101, 255 ), 
		( -0.42858, 23, 50, 115, 255 ), 
		( -0.41358, 26, 58, 123, 255 ), 
		( -0.40358, 34, 62, 145, 255 ), 
		( -0.40348, 140, 175, 157, 128 ), 
		( -0.25586, 140, 175, 157, 64 ), 
		( -0.15586, 244, 239, 223, 32 ), 
		( 0.09414, 149, 146, 142, 0 ), 
		( 1.0, 255, 255, 255, 0 )], 
	lightContrast=1.4, 
	heightMap=heightMapSea, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

RendererPole=RD.create (name='RendererPole',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( 0.456526, 230, 240, 250, 0 ), 
		( 0.4593835, 248, 254, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

bumpmap_renderer=RD.create (name='bumpmap_renderer',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( -0.4, 0, 0, 0, 255 ), 
		( 0.0, 255, 255, 255, 255 ), 
		( 0.6, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	lightContrast=1.0, 
	heightMap=bump_heightMap, 
	enabledLight=False)

bumpmap_renderer_mask=RD.create (name='bumpmap_renderer_mask',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( -0.25536, 0, 0, 0, 255 ), 
		( -0.10586, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_bump, 
	enabledLight=False)

specmap_renderer=RD.create (name='specmap_renderer',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=spec_heightMap, 
	enabledLight=False)

specmap_renderer_spec=RD.create (name='specmap_renderer_spec',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( -0.40358001, 255, 255, 255, 255 ), 
		( -0.40358, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_spec, 
	enabledLight=False)

geo_renderer=RD.create (name='geo_renderer',  
	destImage=img_geo, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=geo_heightMap, 
	enabledLight=False)

geomap_renderer_geo=RD.create (name='geomap_renderer_geo',  
	destImage=img_geo, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( -0.40358001, 255, 255, 255, 255 ), 
		( -0.40358, 0, 0, 0, 255 ), 
		( -0.40308, 224, 224, 224, 255 ), 
		( -0.35258, 224, 224, 224, 255 ), 
		( 1.0, 224, 224, 224, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_geo, 
	enabledLight=False)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)

ExtraLayer_0_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.addRandomFactors([
        0.0,
        0.1,
        0.12,
        0.13,
        0.15,
        0.18,
        0.23,
        0.31
    ])
texture.appendModuleDescriptor([AATurboPoles,AATurboSea,BeginLayer_Module,CylinderPole,ExtraLayer_0_Module,MiddleLayer_Module,RidgedBumpMapModule,RidgedBumpMapModule_e,RidgedBumpMapModule_e_t,SeaModule])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_geo,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,ExtraLayer_0_heightMap,MiddleLayer_heightMap,heightMapPole,heightMapSea,bump_heightMap,geo_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,ExtraLayer_0_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,heightMapBuilderPole,heightMapBuilderSea,noiseMapBuilderBump,noiseMapBuilderGeo,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,ExtraLayer_0_renderer,snow_renderer,RendererSea,RendererPole,bumpmap_renderer,bumpmap_renderer_mask,specmap_renderer,specmap_renderer_spec,geo_renderer,geomap_renderer_geo])     # finally, lets add the renderer

json = texture.jsonString()
with open("Pippo.texjson","w") as f:
	f.write(json)


texture.outputFileName = "Texture_planet"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


