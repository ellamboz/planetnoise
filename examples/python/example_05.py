import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = MD.Perlin( name="Perlin", seed=101, freq=1.2,lac=1.8, pers=0.6,oct=8)

# let's build another Perlin Descriptor for sea:
# take care to use a different name from other modules!
perlin_sea = MD.Perlin( name="PerlinSea", seed=201, freq=1.2,lac=1.8, pers=0.6,oct=8)

# Let's also build a Ridged Multifractal descriptor. Its jagged appearance will be useful 
# to create mountain ranges. 
rmf_mountains = MD.RidgedMulti( name="Ridged", seed=301, freq=3.9, lac=1.9, oct=8)

# ...since RidgedMultiFractal sometimes produces ranges that follow unnaturally neat curves, 
# let's perturbate it with a Turbulence module. src1 parameter is the module to be perturbated.
perturbed_rmf = MD.Turbulence(name="RidgedTurbo", src1=rmf_mountains, seed=401, freq=1.0, pow=0.125, rough=5.1)

# then the image descriptor, which will hold rendered image data
# we STILL need one image. 
image = ID.create("Image")

# then the heightmap, which will hold heightmap data
heightmap = HMD.create("Heightmap")

# we need an heightmap for sea, too. 
heightmapSea = HMD.create("HeightmapSea")

#and one for the mountains
heightmapRange = HMD.create("HeightmapRange")

# connect the noise module with heightmap
heightmapBuilder = NMB.create("hmbuilder", module=perlin, heightMap=heightmap)

# we also need a hm builder for sea. 
heightmapBuilderSea = NMB.create("hmbuilderSea", module=perlin_sea, heightMap=heightmapSea)

# ... and one for mountain ranges. Notice we used the PERTURBED ridged multi module.
heightmapBuilderRanges = NMB.create("hmbuilderRanges", module=perturbed_rmf, heightMap=heightmapRange)




# this is a simple gradient we'll use to render land areas.
# we remove ice, since we'll use rmf_mountains ridged multifractal
# noise as a basis for our mountain ranges. 
land_gradient = [                
    ( -1.0,255,255,192,255),
    ( 0.21,  0,128,  0,255),
    ( 1.0,100, 50,  0,255)
]

# this is a sea gradient. It's sea blue until 55%, then there's a little sand strip
# that becomes transparent. We'll use it to generate a blue map with a cutout area that makes for 
# the continents' profiles. 
sea_gradient = [
    (-1.0  , 20, 20, 80,255),    
    (-0.1  , 30, 30,128,255),
    ( 0.250  , 40, 40,144,255),
    ( 0.251,255,255,192,255),
    ( 0.410,255,255,192,  0),
    ( 1.0  ,255,255,192,  0),
]

# this is the gradient we're using for ice. Only higest peak will be white
ice_gradient = [
    (-1.0  ,255,255,255,   0),
    ( 0.4  ,255,255,255,   0),
    ( 0.42 ,255,255,255,  0),
    ( 0.78 ,255,255,255, 48),
    ( 0.85 ,255,255,255, 128),
    ( 0.95 ,255,255,255, 192),
    ( 1.00 ,255,255,255, 225),
]

# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = RD.create(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo = land_gradient,
    bumpMap = heightmapRange,   # we're using another heightmap as a bumpmap
    destImage=image,
    lightContrast = 1.0,
    enabledLight= True)

# after the land, snow
renderer_mnt  = RD.create(name="renderer_mnt", 
    heightMap=heightmapRange,       # name of the heightmap you want to render
    gradientInfo = ice_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 1.4,
    enabledLight= True)


# then sea covers all: we'll use the sea gradient to create a "sea overlay" on the map. 
# this will make our world map slightly more natural, since land and sea have their own generator.
renderer_sea  = RD.create(name="renderer_sea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = sea_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)



# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()
texture.setSize(2400,1200)
# texture.setBounds(0,10,35,45)


texture.appendModuleDescriptor([perlin,  perlin_sea, rmf_mountains, perturbed_rmf ])
texture.appendImageDescriptor(image)           # add images to builder
texture.appendHeightMapDescriptor([heightmap, heightmapSea, heightmapRange])
texture.appendNoiseMapBuilderDescriptor([heightmapBuilder,heightmapBuilderSea, heightmapBuilderRanges]) # add heightmapbuilder

# very important, renderers perform their job according to their insertion order. 
texture.appendRendererDescriptor([renderer, renderer_mnt, renderer_sea])     # finally, lets add the renderer

jsonTexture = texture.jsonString()
with open ("Planet_05.texjson","w") as f:
    f.write(jsonTexture)

texture.outputFileName = "Planet_05"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

