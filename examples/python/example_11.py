# How to build a texture:

# To build a texture you operate this way:

# First you build and connect one or more modules. 

# Modules can be:
# - Noise generators: these generate the fractals that will make up your world
# - Transformers: these modules transform the output of another module (for instance, Turbulence)
# - Combiners: these modules process the input of two or more modules ( for instance, min, max, average)

# Then you create the images descriptors, that will store binary image data. You can create and render 
# more than one image with a single TextureBuilder

# Then you create the heightmaps, that store modules' output in terms of height level, and range from
# -1.0 to 1.0

# After that, you connect heightmaps to modules by creating heightmap builders.

# You then must render heightmaps to images: to do this, you create Renderers, that connect heightmap 
# to images and render them using custom gradients. This way, you can render not only eartlike maps, but
# marslike maps, jupiterlike maps.

# Moreover, renderers are stackable: you can have a renderer write its heightmap data on a background image
# descriptor. This is the key to get realistic planetary maps. 

# After you are done creating these descriptor, you create a TextureBuilder. It is the object that binds all 
# this stuff together and outputs planetary maps. 

# You append these objects to TextureBuilder, and tell it to generate the textures. 

# Follow these examples to get a feel of planetnoise lib. 

import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB
import utils.gradients as GD
import random

def _r(fmin, fmax):
    return fmin + random.random()*(fmax-fmin)

# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = MD.Perlin( name="Perlin", 
    seed=0, 
    freq=_r(1.0,1.5),
    lac=_r(1.0,3.0), 
    pers=_r(0.4,0.7),
    oct=10
)
rotator = MD.RotatePoint("rotate", perlin, 15,0,-15)
stretcher = MD.ScalePoint("stretcher", perlin, 1.0,4.0,1.0)

# then the image descriptor, which will hold rendered image data
image = ID.create("Image")
image2 = ID.create("Image2")
image3 = ID.create("Image3")

# then the heightmap, which will hold heightmap data
heightmap = HMD.create("Heightmap")
heightmap2 = HMD.create("Heightmap2")
heightmap3= HMD.create("Heightmap3")


# connect the noise module with heightmap
heightmapBuilder = NMB.create("hmbuilder", module=perlin, heightMap=heightmap)
heightmapBuilder2 = NMB.create("hmbuilder2", module=rotator, heightMap=heightmap2)
heightmapBuilder3 = NMB.create("hmbuilder3", module=stretcher, heightMap=heightmap3)




# this is a simple gradient we'll use to render land areas:
planet_gradient = GD.evolve_gradient (7, dh=9, ds=20, dv=100)
rot_gradient = GD.random_gradient(7, True, 32, 160)


# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = RD.create(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo=planet_gradient,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= False)

renderer2  = RD.create(name="renderer2", 
    heightMap=heightmap2,       # name of the heightmap you want to render
    gradientInfo=planet_gradient,
    destImage=image2,
    lightContrast = 2.0,
    enabledLight= False)

renderer3  = RD.create(name="renderer3", 
    heightMap=heightmap3,       # name of the heightmap you want to render
    gradientInfo=planet_gradient,
    destImage=image3,
    lightContrast = 2.0,
    enabledLight= False)



# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([perlin,rotator,stretcher])        # add modules to builder
texture.appendImageDescriptor([image,image2,image3])           # add images to builder
texture.appendHeightMapDescriptor([heightmap, heightmap2, heightmap3])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([heightmapBuilder,heightmapBuilder2, heightmapBuilder3]) # add heightmapbuilder
texture.appendRendererDescriptor([renderer,renderer2, renderer3])     # finally, lets add the renderer

print (texture.jsonString())

texture.outputFileName = "Planet_11"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

