import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


Module1=MD.Perlin ( 
	enableRandom=True, 
	freq=4.2, 
	lac=2.2, 
	name='Module1', 
	oct=6, 
	pers=0.24, 
	seed=0)
ModuleDesert=MD.Billow ( 
	enableRandom=True, 
	freq=4.5, 
	lac=4.3, 
	name='ModuleDesert', 
	oct=6, 
	pers=0.35, 
	seed=0)
ModuleSea=MD.Perlin ( 
	enableRandom=True, 
	freq=1.8, 
	lac=5.3, 
	name='ModuleSea', 
	oct=6, 
	pers=0.23, 
	seed=0)
TextureFlow_Module=MD.RidgedMulti ( 
	enableRandom=True, 
	freq=6.6, 
	lac=1.66912, 
	name='TextureFlow_Module', 
	oct=6, 
	seed=0)
cylinderPole=MD.Cylinders ( 
	enableRandom=False, 
	freq=0.5, 
	name='cylinderPole')
TurboDesert=MD.Turbulence ( 
	enableRandom=True, 
	freq=2.5, 
	name='TurboDesert', 
	pow=0.55, 
	rough=1.2, 
	seed=0, 
	src1=ModuleDesert)
turboPoles=MD.Turbulence ( 
	enableRandom=True, 
	freq=4.5, 
	name='turboPoles', 
	pow=0.2, 
	rough=3.2, 
	seed=0, 
	src1=cylinderPole)

ImageSea=ID.create('ImageSea')
image1=ID.create('image1')
imageBump=ID.create('imageBump')
imageNormal=ID.create('imageNormal')


TextureFlow_heightMap=HMD.create('TextureFlow_heightMap')
heightMapPole=HMD.create('heightMapPole')
heightMapDesert=HMD.create('heightMapDesert')
heightMap=HMD.create('heightMap')
HeightMapSea=HMD.create('HeightMapSea')

TextureFlow_noiseMapBuilder1 = NMB.create('TextureFlow_noiseMapBuilder1', module=TextureFlow_Module, heightMap=TextureFlow_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=turboPoles, heightMap=heightMapPole)
noiseMapBDesert = NMB.create('noiseMapBDesert', module=ModuleDesert, heightMap=heightMapDesert)
noiseMapBuilder1 = NMB.create('noiseMapBuilder1', module=Module1, heightMap=heightMap)
noiseMapSea = NMB.create('noiseMapSea', module=ModuleSea, heightMap=HeightMapSea)

renderer1=RD.create ( 
	destImage=image1, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 12, 97, 0, 255 ), 
		( -0.05604719764011801, 115, 146, 0, 255 ), 
		( 0.9156626506024097, 0, 195, 0, 255 ), 
		( 1, 255, 255, 255, 255 )], 
	heightMap=heightMap, 
	lightBrightness=2, 
	lightContrast=1.5, 
	name='renderer1', 
	randomGradient=False)

renderer2=RD.create ( 
	backgroundImage=image1, 
	destImage=image1, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 251, 247, 244, 0 ), 
		( -0.7991967871485943, 7, 0, 0, 0 ), 
		( -0.028112449799196804, 208, 209, 95, 65 ), 
		( 1, 232, 212, 117, 135 )], 
	heightMap=heightMapDesert, 
	lightBrightness=2, 
	lightContrast=1.4, 
	name='renderer2', 
	randomGradient=False)

renderer3=RD.create ( 
	backgroundImage=image1, 
	destImage=ImageSea, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 0, 31, 174, 255 ), 
		( -0.5903614457831325, 0, 14, 239, 255 ), 
		( 0.024096385542168752, 0, 80, 235, 255 ), 
		( 0.16867469879518082, 85, 226, 234, 255 ), 
		( 0.18338108882521498, 109, 185, 114, 255 ), 
		( 0.2530120481927711, 120, 166, 59, 255 ), 
		( 0.3373493975903614, 214, 176, 146, 0 ), 
		( 0.5060240963855422, 127, 207, 251, 0 ), 
		( 0.9036144578313252, 129, 167, 59, 255 ), 
		( 0.9397590361445782, 117, 225, 251, 255 ), 
		( 1, 0, 203, 224, 255 )], 
	heightMap=HeightMapSea, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer3', 
	randomGradient=False)

renderer4=RD.create ( 
	backgroundImage=ImageSea, 
	destImage=ImageSea, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 0, 0, 0, 0 ), 
		( 0.3293172690763053, 254, 251, 248, 0 ), 
		( 0.37349397590361444, 255, 253, 251, 255 ), 
		( 1, 255, 254, 252, 255 )], 
	heightMap=heightMapPole, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer4', 
	randomGradient=False)

renderer5=RD.create ( 
	destImage=imageBump, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 255, 255, 255, 255 ), 
		( 0.167, 255, 255, 255, 255 ), 
		( 0.168, 0, 0, 0, 255 ), 
		( 0.91, 0, 0, 0, 255 ), 
		( 0.911, 255, 255, 255, 255 ), 
		( 1, 255, 255, 255, 255 )], 
	heightMap=HeightMapSea, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer5', 
	randomGradient=False)

renderer0006=RD.create ( 
	destImage=imageNormal, 
	enabledLight=False, 
	gradientInfo=[
		( 0.012844036697247763, 0, 0, 0, 255 ), 
		( 0.8560000000000001, 255, 255, 255, 255 ), 
		( 1, 255, 255, 255, 255 )], 
	heightMap=TextureFlow_heightMap, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer0006', 
	randomGradient=False)

renderer0007=RD.create ( 
	backgroundImage=imageNormal, 
	destImage=imageNormal, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 0, 0, 0, 255 ), 
		( 0.2550458715596331, 0, 0, 0, 255 ), 
		( 0.5743119266055046, 0, 0, 0, 0 ), 
		( 0.8445595854922279, 0, 0, 0, 0 ), 
		( 0.9039999999999999, 255, 255, 255, 0 ), 
		( 1, 255, 255, 255, 255 )], 
	heightMap=HeightMapSea, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer0007', 
	randomGradient=False)



texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([Module1,ModuleDesert,ModuleSea,TextureFlow_Module,TurboDesert,cylinderPole,turboPoles])        # add modules to builder
texture.appendImageDescriptor([ImageSea,image1,imageBump,imageNormal])           # add images to builder
texture.appendHeightMapDescriptor([TextureFlow_heightMap,heightMapPole,heightMapDesert,heightMap,HeightMapSea])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([TextureFlow_noiseMapBuilder1,heightMapBuilderPole,noiseMapBDesert,noiseMapBuilder1,noiseMapSea]) # add heightmapbuilder
texture.appendRendererDescriptor([renderer1,renderer2,renderer3,renderer4,renderer5,renderer0006,renderer0007])     # finally, lets add the renderer

texture.outputFileName = "Texture_Earthlike"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


