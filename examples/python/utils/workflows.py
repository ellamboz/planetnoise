import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

import gradients
import random

class TextureWorkflow(object):
    def __init__ (self, name, bottom_level = False, texturebuilder=None, transTex=False, has_background=False ):
        self.name=name
        self.bottom_level= bottom_level
        self.trans_tex = transTex
        self.has_background=has_background
        if texturebuilder != None:
            self.texturebuilder = texturebuilder
        else:
            self.texturebuilder = TextureBuilder()
    
    def build(self):
        w = random.randrange(0,100)
        mod = None
        mod_name = "{}_{}".format(self.name,"Module")
        if 0 <= w < 50:
            mod=MD.Perlin(mod_name,0)
        elif 50 <= w < 75:
            mod=MD.Billow(mod_name,0)
        elif 75 < w < 92:
            mod=MD.RidgedMulti(mod_name,"Module"),0)
        else:
            mod=MD.Voronoi(mod_name,0,3.5,0.1)
        self.texturebuilder.appendModuleDescriptor(mod)
        mod_hm = "{}_{}".format(self.name,"Heightmap")
        mod_hmb = "{}_{}".format(self.name,"HeightmapBuilder")
        mod_img = "{}_{}".format(self.name,"HeightmapBuilder")
        img = ID.create(mod_img)
        self.texturebuilder.appendHeightMapDescriptor(img)
        hm = HMD.create(mod_hm)
        self.texturebuilder.appendHeightMapDescriptor()

