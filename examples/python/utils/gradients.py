import colorsys
import random

BEACH_COLORS = [
    "EEEEC8FF",
    "DEDEB8FF",
    "F8E0C8FF",
    "E8D0B8FF",
    "00D800FF",
    "009000FF",
    "408000FF"
]


PLANET_COLORS = [
    "9f8454FF",
    "b89d7eFF",
    "875e47FF",
    "de5206FF",
    "728f50FF",
    "728f50FF",
    "728f50FF",
    "724d3dFF",
    "c4b49fFF",
    "6f4634FF",
    "918a6bFF",
    "8e8651FF",
    "75824aFF",
    "737c3eFF",
    "5a9b50FF",
    "5a9b50FF",
    "5a9b50FF",
    "5a9b50FF",
    "35351bFF",
    "b07e5fFF",
    "ae998dFF",
    "d7b6a2FF",
    "c9a38cFF",
    "92aeb0FF",
    "3a3f1bFF",
    "2e311eFF",
    "9d7361FF",
    "805744FF"
]

def htmla_to_rgba(h):
    return tuple(int(h[i:i+2], 16) for i in (0, 2, 4, 6))    

def rgba_to_hsva(color):
    rf = float(color[0])/255.0
    gf = float(color[1])/255.0    
    bf = float(color[2])/255.0    
    hf,sf,vf = colorsys.rgb_to_hsv(rf,gf,bf)
    return ( int(hf*255.0), int(sf*255.0), int(vf*255.0), color[3])

def hsva_to_rgba(color):
    rf = float(color[0])/255.0
    gf = float(color[1])/255.0    
    bf = float(color[2])/255.0    
    hf,sf,vf = colorsys.hsv_to_rgb(rf,gf,bf)
    return ( int(hf*255.0), int(sf*255.0), int(vf*255.0), color[3])

def evolve_hsva(c, dh=15, ds=5, dv=0, da=0):
    (h,s,v,a) = c
    if (dh > 0):
        h = (h + random.randint(-dh,dh)) % 256
    if ds > 0:
        s = (s + random.randint(-ds,ds)) % 256
    if dv > 0: 
        v = (v + random.randint(-dv,dv)) % 256 
        if (v < 96):
            v = random.randrange(96,256)
    if da > 0:
        a = (a + random.randint(-da,da)) % 256
    return (h,s,v,a)

def evolve_rgba (c, dh=15, ds=5, dv=0, da=0):
    c1 = rgba_to_hsva(c)
    cv = evolve_hsva(c1,dh,ds,dv,da)
    return hsva_to_rgba(cv)


def random_color_rgb(randomize_alpha=False, lbound_alpha=0, ubound_alpha=256):
    alpha = 255
    if (randomize_alpha):
        alpha = random.randrange(lbound_alpha,ubound_alpha)
    c = (
        random.randrange(0,256),
        random.randrange(128,256),
        random.randrange(96,256),
        alpha
    )
    return hsva_to_rgba(c)

def random_beach_color():
    return htmla_to_rgba( BEACH_COLORS[random.randrange(0,len(BEACH_COLORS))])

def random_planet_color():
    return htmla_to_rgba(PLANET_COLORS[random.randrange(0,len(PLANET_COLORS))])



def random_color_hsv(randomize_alpha=False, lbound_alpha=0, ubound_alpha=256):
    alpha = 255
    if (randomize_alpha):
        alpha = random.randrange(lbound_alpha,ubound_alpha)
    c = (
        random.randrange(0,256),
        random.randrange(128,256),
        random.randrange(96,256),
        alpha
    )
    return c

def random_gradient (random_steps=3, randomize_alpha=False, lbound_alpha=0, ubound_alpha=256):
    delta = 2.0/float(random_steps+1)
    gradient_steps=[]
    c = random_color_rgb(randomize_alpha, lbound_alpha, ubound_alpha)
    gradient_steps.append((-1.0, c[0], c[1],c[2],c[3]))
    ds = -1.0
    for r in range(random_steps):
        c = random_color_rgb(randomize_alpha, lbound_alpha, ubound_alpha)
        dd = random.random()*delta*0.14-delta*0.07
        ds = ds + (delta+dd)
        gradient_steps.append((ds, c[0], c[1],c[2],c[3]))
    c = random_color_rgb(randomize_alpha, lbound_alpha, ubound_alpha)
    gradient_steps.append((1.0, c[0], c[1],c[2],c[3]))
    return gradient_steps




def evolve_gradient (random_steps=3, randomize_alpha=False, lbound_alpha=0, ubound_alpha=256, dh=15, ds=5, dv=0, da=0):
    delta = 2.0/float(random_steps+1)
    gradient_steps=[]
    c = random_color_rgb(randomize_alpha, lbound_alpha, ubound_alpha)
    gradient_steps.append((-1.0, c[0], c[1],c[2],c[3]))
    dds = -1.0
    for r in range(random_steps):
        c = evolve_rgba(c, dh,ds, dv, da)
        ddd = random.random()*delta*0.14-delta*0.07
        dds = dds + (delta+ddd)
        gradient_steps.append((dds, c[0], c[1],c[2],c[3]))
    c = evolve_rgba(c, dh,ds, dv, da)
    gradient_steps.append((1.0, c[0], c[1],c[2],c[3]))
    return gradient_steps    
    
def evolve_beach_gradient (random_steps=3, randomize_alpha=False, lbound_alpha=0, ubound_alpha=256, dh=15, ds=5, dv=0, da=0):
    delta = 2.0/float(random_steps+1)
    gradient_steps=[]
    c = random_beach_color()
    gradient_steps.append((-1.0, c[0], c[1],c[2],c[3]))
    dds = -1.0
    for r in range(random_steps):
        c = evolve_rgba(c, dh,ds, dv, da)
        ddd = random.random()*delta*0.14-delta*0.07
        dds = dds + (delta+ddd)
        gradient_steps.append((dds, c[0], c[1],c[2],c[3]))
    c = evolve_rgba(c, dh,ds, dv, da)
    gradient_steps.append((1.0, c[0], c[1],c[2],c[3]))
    return gradient_steps        

def evolve_planet_gradient (random_steps=3, randomize_alpha=False, lbound_alpha=0, ubound_alpha=256, dh=15, ds=5, dv=0, da=0):
    random.shuffle(PLANET_COLORS)
    delta = 2.0/float(random_steps+1)
    gradient_steps=[]
    nc = 0
    c = htmla_to_rgba(PLANET_COLORS[nc])
    gradient_steps.append((-1.0, c[0], c[1],c[2],c[3]))
    dds = -1.0
    for r in range(random_steps):
        if r  % 2 == 1:
            nc = nc+1
            c = htmla_to_rgba(PLANET_COLORS[nc])
        else:
            c = evolve_rgba(c, dh,ds, dv, da)
        ddd = random.random()*delta*0.14-delta*0.07
        dds = dds + (delta+ddd)
        gradient_steps.append((dds, c[0], c[1],c[2],c[3]))
    c = evolve_rgba(c, dh,ds, dv, da)
    gradient_steps.append((1.0, c[0], c[1],c[2],c[3]))
    return gradient_steps         

if __name__=="__main__":
    c = random_color_rgb()
    print (c)
    
    c = random_color_rgb(True)
    print (c)
    
    c = random_color_rgb(True,0,160)
    print (c)

    r = random_gradient(6,True,50,100)
    print(r)

    r = evolve_gradient(6)
    print(r)


    