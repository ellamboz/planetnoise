import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.RidgedMulti2 ( name='BeginLayer_Module',  
	enableRandom=True, 
	lac=3.03744006157, 
	seed=0, 
	gain=2.0, 
	exp=2.0, 
	offset=0.0, 
	freq=2.35209989548, 
	oct=6)
CylinderPole=MD.Cylinders ( name='CylinderPole',  
	freq=0.5, 
	enableRandom=False)
EndLayer_Module=MD.Perlin ( name='EndLayer_Module',  
	enableRandom=True, 
	lac=3.26816010475, 
	pers=0.315665990114, 
	seed=0, 
	freq=2.3436999321, 
	oct=6)
MiddleLayer_Module=MD.RidgedMulti2 ( name='MiddleLayer_Module',  
	enableRandom=True, 
	lac=3.21228790283, 
	seed=0, 
	gain=2.0, 
	exp=2.0, 
	offset=0.0, 
	freq=2.37330007553, 
	oct=6)
PenLayer_Module=MD.Perlin ( name='PenLayer_Module',  
	enableRandom=True, 
	lac=3.084608078, 
	pers=0.306522011757, 
	seed=0, 
	freq=2.6671500206, 
	oct=6)
AATurboPoles=MD.Turbulence ( name='AATurboPoles',  
	src1=CylinderPole, 
	pow=0.32152, 
	enableRandom=False, 
	seed=0, 
	freq=4.9884, 
	rough=2.99925)
AvgBumpMapModule=MD.Avg4 ( name='AvgBumpMapModule',  
	enableRandom=False, 
	src2=MiddleLayer_Module, 
	src3=PenLayer_Module, 
	src1=BeginLayer_Module, 
	src4=EndLayer_Module)
AvgBumpMapModule_e=MD.Exponent ( name='AvgBumpMapModule_e',  
	src1=AvgBumpMapModule, 
	enableRandom=False, 
	exp=1.25)
AvgBumpMapModule_e_t=MD.Turbulence ( name='AvgBumpMapModule_e_t',  
	src1=AvgBumpMapModule_e, 
	pow=0.1236, 
	enableRandom=False, 
	seed=0, 
	freq=7.0622, 
	rough=1.8588)

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
EndLayer_heightMap=HMD.create('EndLayer_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
PenLayer_heightMap=HMD.create('PenLayer_heightMap')
heightMapPole=HMD.create('heightMapPole')
bump_heightMap=HMD.create('bump_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
EndLayer_noiseMapBuilder1 = NMB.create('EndLayer_noiseMapBuilder1', module=EndLayer_Module, heightMap=EndLayer_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
PenLayer_noiseMapBuilder1 = NMB.create('PenLayer_noiseMapBuilder1', module=PenLayer_Module, heightMap=PenLayer_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=AvgBumpMapModule_e_t, heightMap=bump_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=EndLayer_Module, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 57, 97, 98, 255 ), 
		( -0.5, 56, 95, 99, 255 ), 
		( 0.0, 99, 55, 52, 255 ), 
		( 0.5, 56, 57, 54, 255 ), 
		( 1.0, 162, 122, 0, 255 )], 
	lightContrast=1.4, 
	heightMap=BeginLayer_heightMap, 
	enabledLight=False)

MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 52, 51, 49, 4 ), 
		( -0.714285714286, 52, 50, 48, 23 ), 
		( -0.428571428571, 55, 53, 51, 192 ), 
		( -0.142857142857, 55, 53, 51, 7 ), 
		( 0.142857142857, 52, 50, 48, 16 ), 
		( 0.428571428571, 54, 52, 50, 6 ), 
		( 0.714285714286, 57, 54, 52, 29 ), 
		( 1.0, 140, 66, 175, 255 )], 
	lightContrast=1.4, 
	heightMap=MiddleLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

PenLayer_renderer=RD.create (  name='PenLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 52, 53, 50, 28 ), 
		( -0.714285714286, 54, 55, 52, 5 ), 
		( -0.428571428571, 52, 54, 51, 27 ), 
		( -0.142857142857, 49, 51, 48, 192 ), 
		( 0.142857142857, 48, 51, 47, 12 ), 
		( 0.428571428571, 46, 50, 45, 13 ), 
		( 0.714285714286, 44, 49, 43, 15 ), 
		( 1.0, 157, 194, 169, 255 )], 
	lightContrast=1.4, 
	heightMap=PenLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

EndLayer_renderer=RD.create (  name='EndLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 98, 57, 56, 255 ), 
		( -0.714285714286, 102, 60, 57, 255 ), 
		( -0.428571428571, 104, 62, 58, 255 ), 
		( -0.142857142857, 100, 57, 55, 255 ), 
		( 0.142857142857, 103, 57, 55, 0 ), 
		( 0.428571428571, 104, 55, 55, 0 ), 
		( 0.714285714286, 104, 57, 56, 0 ), 
		( 1.0, 173, 212, 133, 0 )], 
	lightContrast=1.4, 
	heightMap=EndLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=bump_heightMap, 
	enabledLight=False)

specmap_renderer=RD.create (  name='specmap_renderer',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=spec_heightMap, 
	enabledLight=False)

RendererPole=RD.create (  name='RendererPole',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( 0.902268, 230, 240, 250, 0 ), 
		( 0.904738, 248, 254, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)

PenLayer_renderer.setRandomFactor(20,40,60)

EndLayer_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([AATurboPoles,AvgBumpMapModule,AvgBumpMapModule_e,AvgBumpMapModule_e_t,BeginLayer_Module,CylinderPole,EndLayer_Module,MiddleLayer_Module,PenLayer_Module])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,EndLayer_heightMap,MiddleLayer_heightMap,PenLayer_heightMap,heightMapPole,bump_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,EndLayer_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,PenLayer_noiseMapBuilder1,heightMapBuilderPole,noiseMapBuilderBump,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,PenLayer_renderer,EndLayer_renderer,bumpmap_renderer,specmap_renderer,RendererPole])     # finally, lets add the renderer

texture.outputFileName = "Texture_planet04"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


