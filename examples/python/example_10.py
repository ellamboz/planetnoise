# How to build a texture:

# To build a texture you operate this way:

# First you build and connect one or more modules. 

# Modules can be:
# - Noise generators: these generate the fractals that will make up your world
# - Transformers: these modules transform the output of another module (for instance, Turbulence)
# - Combiners: these modules process the input of two or more modules ( for instance, min, max, average)

# Then you create the images descriptors, that will store binary image data. You can create and render 
# more than one image with a single TextureBuilder

# Then you create the heightmaps, that store modules' output in terms of height level, and range from
# -1.0 to 1.0

# After that, you connect heightmaps to modules by creating heightmap builders.

# You then must render heightmaps to images: to do this, you create Renderers, that connect heightmap 
# to images and render them using custom gradients. This way, you can render not only eartlike maps, but
# marslike maps, jupiterlike maps.

# Moreover, renderers are stackable: you can have a renderer write its heightmap data on a background image
# descriptor. This is the key to get realistic planetary maps. 

# After you are done creating these descriptor, you create a TextureBuilder. It is the object that binds all 
# this stuff together and outputs planetary maps. 

# You append these objects to TextureBuilder, and tell it to generate the textures. 

# Follow these examples to get a feel of planetnoise lib. 

import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

# let's create the TextureBuilder
texture = planetnoise.TextureBuilder()


# let's build the perlin noise descriptor
# seed = 0 means "totally random"
voronoi = texture.Voronoi("Voronoi",0,5.5,-0.2, True)
curve = texture.Curve("Curve",voronoi, [
    (-1.0,-1.0),
    (-0.3,-1.0),
    (-0.05, 1.0),
    ( 0.05, 1.0),
    ( 0.3,-1.0),
    ( 1.0,-1.0),
])
turbo = texture.Turbulence("TurboCurve",curve,0, 1.33,0.12,4.42)
ridged = texture.RidgedMulti("Ridge",0,4.2,2.4,8)
mmax = texture.Max("Max",turbo, ridged)
perlin = texture.Perlin("Perlin",0,4.8,2.2,0.3,8)

# then the image descriptor, which will hold rendered image data
image = texture.Image("Image")

# then the heightmap, which will hold heightmap data
heightmap = texture.HeightMap("Heightmap")

# connect the noise module with heightmap
heightmapBuilder = texture.NoiseMapBuilder("hmbuilder", module=mmax, heightMap=heightmap)

hmColor =texture.HeightMap("HMColor")
hmbColor = texture.NoiseMapBuilder("hmbuilderColor", module=perlin, heightMap=hmColor)


# this is a simple gradient we'll use to render land areas:
bummp_gradient = [                
    (-1.0  , 0, 0, 0,255),    
    (-0.9  , 0, 0, 0,255),    
    ( 1.0 ,255,255,255,255)
]


# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer_bump  = texture.Renderer(name="renderer_bump", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo=bummp_gradient,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= False)

renderer  = texture.Renderer(name="renderer", 
    heightMap=hmColor,       # name of the heightmap you want to render
    gradientInfo=[
        (-1.0, 192, 144,32,255),
        (1.0, 255, 162,32,255)
    ],
    bumpMap= heightmap,
    destImage=image,
    lightContrast = 2.0,
    randomGradient = True,
    enabledLight= True)


jsonTexture = texture.jsonString()
with open ("Planet_10.texjson","w") as f:
    f.write(jsonTexture)



texture.outputFileName = "Planet_10"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

