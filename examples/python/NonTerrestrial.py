import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',   
	enableRandom=True, 
	freq=2.7421998977661133, 
	lac=3.0197761058807373, 
	oct=6, 
	pers=0.2843340039253235, 
	seed=0)
EndLayer_Module=MD.Perlin ( name='EndLayer_Module',   
	enableRandom=True, 
	freq=2.683500051498413, 
	lac=3.23635196685791, 
	oct=6, 
	pers=0.31400400400161743, 
	seed=0)
ExtraLayer_0_Module=MD.Voronoi ( name='ExtraLayer_0_Module',   
	disp=0.0, 
	enableDist=True, 
	enableRandom=True, 
	freq=2.491300106048584, 
	seed=0)
ExtraLayer_1_Module=MD.Perlin ( name='ExtraLayer_1_Module',   
	enableRandom=True, 
	freq=2.3055999279022217, 
	lac=3.2023680210113525, 
	oct=6, 
	pers=0.29054999351501465, 
	seed=0)
ExtraLayer_2_Module=MD.Perlin ( name='ExtraLayer_2_Module',   
	enableRandom=True, 
	freq=2.2788500785827637, 
	lac=3.3467519283294678, 
	oct=6, 
	pers=0.2732999920845032, 
	seed=0)
ExtraLayer_3_Module=MD.RidgedMulti2 ( name='ExtraLayer_3_Module',   
	enableRandom=True, 
	exp=2.0, 
	freq=2.660249948501587, 
	gain=2.0, 
	lac=3.070847988128662, 
	oct=6, 
	offset=0.0, 
	seed=0)
MiddleLayer_Module=MD.Perlin ( name='MiddleLayer_Module',   
	enableRandom=True, 
	freq=2.280250072479248, 
	lac=3.395967960357666, 
	oct=6, 
	pers=0.30070799589157104, 
	seed=0)
PenLayer_Module=MD.Perlin ( name='PenLayer_Module',   
	enableRandom=True, 
	freq=2.565850019454956, 
	lac=3.1960320472717285, 
	oct=6, 
	pers=0.3148140013217926, 
	seed=0)
AvgBumpMapModule=MD.Avg4 ( name='AvgBumpMapModule',   
	enableRandom=False, 
	src1=BeginLayer_Module, 
	src2=MiddleLayer_Module, 
	src3=PenLayer_Module, 
	src4=EndLayer_Module)
AvgBumpMapModule_e=MD.Exponent ( name='AvgBumpMapModule_e',   
	enableRandom=False, 
	exp=1.25, 
	src1=AvgBumpMapModule)
AvgBumpMapModule_e_t=MD.Turbulence ( name='AvgBumpMapModule_e_t',   
	enableRandom=False, 
	freq=7.5263, 
	pow=0.4514, 
	rough=1.6552, 
	seed=0, 
	src1=AvgBumpMapModule_e)
AvgBumpMapModule_terr = MD.Terrace ( name="AvgBumpMapModule_terr",
	src1=AvgBumpMapModule_e, cpoints= [
		(-1.0,-1.0),
		(-0.4, 0.4),
		( 0.5, 0.5),
		( 1.0, 1.0)
	])

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
EndLayer_heightMap=HMD.create('EndLayer_heightMap')
ExtraLayer_0_heightMap=HMD.create('ExtraLayer_0_heightMap')
ExtraLayer_1_heightMap=HMD.create('ExtraLayer_1_heightMap')
ExtraLayer_2_heightMap=HMD.create('ExtraLayer_2_heightMap')
ExtraLayer_3_heightMap=HMD.create('ExtraLayer_3_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
PenLayer_heightMap=HMD.create('PenLayer_heightMap')
bump_heightMap=HMD.create('bump_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
EndLayer_noiseMapBuilder1 = NMB.create('EndLayer_noiseMapBuilder1', module=EndLayer_Module, heightMap=EndLayer_heightMap)
ExtraLayer_0_noiseMapBuilder1 = NMB.create('ExtraLayer_0_noiseMapBuilder1', module=ExtraLayer_0_Module, heightMap=ExtraLayer_0_heightMap)
ExtraLayer_1_noiseMapBuilder1 = NMB.create('ExtraLayer_1_noiseMapBuilder1', module=ExtraLayer_1_Module, heightMap=ExtraLayer_1_heightMap)
ExtraLayer_2_noiseMapBuilder1 = NMB.create('ExtraLayer_2_noiseMapBuilder1', module=ExtraLayer_2_Module, heightMap=ExtraLayer_2_heightMap)
ExtraLayer_3_noiseMapBuilder1 = NMB.create('ExtraLayer_3_noiseMapBuilder1', module=ExtraLayer_3_Module, heightMap=ExtraLayer_3_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
PenLayer_noiseMapBuilder1 = NMB.create('PenLayer_noiseMapBuilder1', module=PenLayer_Module, heightMap=PenLayer_heightMap)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=AvgBumpMapModule_terr, heightMap=bump_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=AvgBumpMapModule_terr, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 56, 56, 53, 255 ), 
		( -0.5, 98, 55, 55, 255 ), 
		( 0.0, 101, 98, 97, 255 ), 
		( 0.5, 101, 54, 49, 255 ), 
		( 1.0, 223, 90, 43, 255 )], 
	heightMap=BeginLayer_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 50, 52, 49, 12 ), 
		( -0.7142857142857143, 49, 52, 48, 192 ), 
		( -0.4285714285714286, 52, 55, 51, 14 ), 
		( -0.1428571428571429, 52, 55, 51, 17 ), 
		( 0.1428571428571428, 49, 52, 48, 7 ), 
		( 0.4285714285714285, 51, 54, 50, 192 ), 
		( 0.7142857142857142, 53, 57, 52, 16 ), 
		( 1.0, 234, 51, 20, 255 )], 
	heightMap=MiddleLayer_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

PenLayer_renderer=RD.create (  name='PenLayer_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 51, 53, 49, 29 ), 
		( -0.7142857142857143, 53, 55, 51, 192 ), 
		( -0.4285714285714286, 52, 54, 50, 5 ), 
		( -0.1428571428571429, 49, 51, 47, 14 ), 
		( 0.1428571428571428, 48, 51, 46, 22 ), 
		( 0.4285714285714285, 46, 50, 44, 192 ), 
		( 0.7142857142857142, 44, 49, 42, 27 ), 
		( 1.0, 230, 25, 96, 255 )], 
	heightMap=PenLayer_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

ExtraLayer_0_renderer=RD.create (  name='ExtraLayer_0_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 56, 57, 53, 32 ), 
		( -0.7142857142857143, 59, 61, 56, 8 ), 
		( -0.4285714285714286, 60, 63, 58, 24 ), 
		( -0.1428571428571429, 56, 59, 53, 14 ), 
		( 0.1428571428571428, 58, 62, 54, 192 ), 
		( 0.4285714285714285, 58, 63, 54, 192 ), 
		( 0.7142857142857142, 57, 63, 54, 4 ), 
		( 1.0, 163, 177, 84, 255 )], 
	heightMap=ExtraLayer_0_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

ExtraLayer_1_renderer=RD.create (  name='ExtraLayer_1_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 18 ), 
		( -0.7142857142857143, 3, 2, 2, 25 ), 
		( -0.4285714285714286, 7, 4, 4, 192 ), 
		( -0.1428571428571429, 8, 4, 4, 5 ), 
		( 0.1428571428571428, 7, 3, 3, 3 ), 
		( 0.4285714285714285, 7, 3, 3, 2 ), 
		( 0.7142857142857142, 4, 1, 1, 16 ), 
		( 1.0, 207, 194, 198, 255 )], 
	heightMap=ExtraLayer_1_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

ExtraLayer_2_renderer=RD.create (  name='ExtraLayer_2_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 96, 50, 46, 192 ), 
		( -0.7142857142857143, 97, 51, 47, 28 ), 
		( -0.4285714285714286, 95, 49, 45, 23 ), 
		( -0.1428571428571429, 95, 50, 45, 21 ), 
		( 0.1428571428571428, 98, 49, 45, 26 ), 
		( 0.4285714285714285, 101, 50, 45, 8 ), 
		( 0.7142857142857142, 102, 51, 46, 30 ), 
		( 1.0, 150, 134, 170, 255 )], 
	heightMap=ExtraLayer_2_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

ExtraLayer_3_renderer=RD.create (  name='ExtraLayer_3_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 253, 252, 251, 16 ), 
		( -0.7142857142857143, 255, 252, 249, 32 ), 
		( -0.4285714285714286, 3, 2, 2, 192 ), 
		( -0.1428571428571429, 3, 2, 2, 32 ), 
		( 0.1428571428571428, 3, 2, 2, 5 ), 
		( 0.4285714285714285, 3, 2, 1, 192 ), 
		( 0.7142857142857142, 7, 4, 2, 192 ), 
		( 1.0, 171, 164, 133, 255 )], 
	heightMap=ExtraLayer_3_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

EndLayer_renderer=RD.create (  name='EndLayer_renderer',  
	backgroundImage=BaseImage, 
	bumpMap=BeginLayer_heightMap, 
	destImage=BaseImage, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 47, 48, 45, 255 ), 
		( -0.7142857142857143, 44, 46, 43, 255 ), 
		( -0.4285714285714286, 47, 49, 46, 255 ), 
		( -0.1428571428571429, 45, 47, 44, 0 ), 
		( 0.1428571428571428, 45, 47, 44, 0 ), 
		( 0.4285714285714285, 44, 47, 44, 0 ), 
		( 0.7142857142857142, 45, 49, 45, 0 ), 
		( 1.0, 119, 38, 137, 0 )], 
	heightMap=EndLayer_heightMap, 
	lightBrightness=2.0, 
	lightContrast=1.4, 
	randomGradient=False)

bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
	destImage=img_bump, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	heightMap=bump_heightMap, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)

specmap_renderer=RD.create (  name='specmap_renderer',  
	destImage=img_spec, 
	enabledLight=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.0, 0, 0, 0, 255 ),
		( 1.0, 255, 255, 255, 255 )], 
	heightMap=bump_heightMap, 
	lightBrightness=1.0, 
	lightContrast=1.0, 
	randomGradient=False)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)

PenLayer_renderer.setRandomFactor(20,40,60)

ExtraLayer_0_renderer.setRandomFactor(20,40,60)

ExtraLayer_1_renderer.setRandomFactor(20,40,60)

ExtraLayer_2_renderer.setRandomFactor(20,40,60)

ExtraLayer_3_renderer.setRandomFactor(20,40,60)

EndLayer_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([AvgBumpMapModule,AvgBumpMapModule_e,AvgBumpMapModule_e_t,BeginLayer_Module,EndLayer_Module,ExtraLayer_0_Module,ExtraLayer_1_Module,ExtraLayer_2_Module,ExtraLayer_3_Module,MiddleLayer_Module,PenLayer_Module,AvgBumpMapModule_terr])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,EndLayer_heightMap,ExtraLayer_0_heightMap,ExtraLayer_1_heightMap,ExtraLayer_2_heightMap,ExtraLayer_3_heightMap,MiddleLayer_heightMap,PenLayer_heightMap,bump_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,EndLayer_noiseMapBuilder1,ExtraLayer_0_noiseMapBuilder1,ExtraLayer_1_noiseMapBuilder1,ExtraLayer_2_noiseMapBuilder1,ExtraLayer_3_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,PenLayer_noiseMapBuilder1,noiseMapBuilderBump,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,PenLayer_renderer,ExtraLayer_0_renderer,ExtraLayer_1_renderer,ExtraLayer_2_renderer,ExtraLayer_3_renderer,EndLayer_renderer,bumpmap_renderer,specmap_renderer])     # finally, lets add the renderer

texture.outputFileName = "Texture_NonTerrestrial"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


