import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

class PostGarden02 :
    def __init__(self, textureName='PostGarden02'):
        self.texture = planetnoise.TextureBuilder()
        self.texture.setSize(2400,1200)
        self.textureName = textureName
        self.texture.addRandomFactors([0.0, 0.1, 0.12, 0.13, 0.15, 0.18, 0.23, 0.31, 0.1, 0.12, 0.13, 0.15, 0.18, 0.23, 0.31])


        BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',   
                enableRandom=True, 
                freq=2.3721001148223877, 
                lac=3.1413118839263916, 
                oct=6, 
                pers=0.30285000801086426, 
                seed=0)
        CylinderPole=MD.Cylinders ( name='CylinderPole',   
                enableRandom=False, 
                freq=0.5)
        MiddleLayer_Module=MD.Perlin ( name='MiddleLayer_Module',   
                enableRandom=True, 
                freq=2.735650062561035, 
                lac=3.380160093307495, 
                oct=6, 
                pers=0.3193260133266449, 
                seed=0)
        RidgedBumpMapModule=MD.Perlin ( name='RidgedBumpMapModule',   
                enableRandom=False, 
                freq=6.3, 
                lac=3.6, 
                oct=8, 
                pers=0.3, 
                seed=0)
        SeaModule=MD.Perlin ( name='SeaModule',   )
        AATurboPoles=MD.Turbulence ( name='AATurboPoles',   
                enableRandom=False, 
                freq=5.2156, 
                pow=0.25189, 
                rough=3.4106500000000004, 
                seed=0, 
                src1=CylinderPole)
        AATurboSea=MD.Turbulence ( name='AATurboSea',   
                enableRandom=False, 
                freq=0.2277, 
                pow=0.116, 
                rough=2.2085999999999997, 
                seed=0, 
                src1=SeaModule)


        BaseImage=ID.create('BaseImage')
        img_bump=ID.create('img_bump')
        img_spec=ID.create('img_spec')



        BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
        MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
        heightMapPole=HMD.create('heightMapPole')
        heightMapSea=HMD.create('heightMapSea')
        bump_heightMap=HMD.create('bump_heightMap')
        spec_heightMap=HMD.create('spec_heightMap')

        BeginLayer_noiseMapBuilder1=NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
        MiddleLayer_noiseMapBuilder1=NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
        heightMapBuilderPole=NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
        heightMapBuilderSea=NMB.create('heightMapBuilderSea', module=AATurboSea, heightMap=heightMapSea)
        noiseMapBuilderBump=NMB.create('noiseMapBuilderBump', module=RidgedBumpMapModule, heightMap=bump_heightMap)
        noiseMapBuilderSpec=NMB.create('noiseMapBuilderSpec', module=MiddleLayer_Module, heightMap=spec_heightMap)


        BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 56, 55, 51, 255 ), 
                ( -0.8571428571428572, 57, 55, 51, 255 ), 
                ( -0.7142857142857144, 60, 57, 54, 255 ), 
                ( -0.5714285714285716, 61, 58, 55, 255 ), 
                ( -0.42857142857142877, 62, 58, 54, 255 ), 
                ( -0.2857142857142859, 62, 57, 53, 255 ), 
                ( -0.14285714285714307, 65, 59, 55, 255 ), 
                ( -2.220446049250313e-16, 65, 58, 55, 255 ), 
                ( 0.14285714285714263, 62, 55, 52, 255 ), 
                ( 0.2857142857142855, 64, 56, 54, 255 ), 
                ( 0.4285714285714283, 67, 58, 56, 255 ), 
                ( 0.5714285714285712, 64, 56, 54, 255 ), 
                ( 0.714285714285714, 66, 58, 56, 255 ), 
                ( 0.8571428571428568, 65, 57, 55, 255 ), 
                ( 0.9999999999999996, 62, 54, 52, 255 ), 
                ( 1.0, 141, 71, 80, 255 )], 
            heightMap=BeginLayer_heightMap, 
            lightBrightness=2.0, 
            lightContrast=1.4, 
            randomGradient=False)

        MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
            backgroundImage=BaseImage, 
            bumpMap=BeginLayer_heightMap, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 99, 97, 56, 19 ), 
                ( -0.8571428571428572, 98, 95, 54, 11 ), 
                ( -0.7142857142857144, 97, 93, 52, 192 ), 
                ( -0.5714285714285716, 98, 93, 52, 27 ), 
                ( -0.42857142857142877, 102, 96, 53, 19 ), 
                ( -0.2857142857142859, 104, 97, 54, 192 ), 
                ( -0.14285714285714307, 100, 91, 51, 10 ), 
                ( -2.220446049250313e-16, 103, 92, 51, 14 ), 
                ( 0.14285714285714263, 104, 91, 51, 2 ), 
                ( 0.2857142857142855, 104, 91, 52, 1 ), 
                ( 0.4285714285714283, 104, 88, 52, 5 ), 
                ( 0.5714285714285712, 107, 90, 53, 192 ), 
                ( 0.714285714285714, 111, 91, 55, 3 ), 
                ( 0.8571428571428568, 112, 92, 54, 9 ), 
                ( 0.9999999999999996, 111, 88, 54, 6 ), 
                ( 1.0, 167, 201, 199, 255 )], 
            heightMap=MiddleLayer_heightMap, 
            lightBrightness=2.0, 
            lightContrast=1.4, 
            randomGradient=False)

        RendererSea=RD.create (  name='RendererSea',  
            backgroundImage=BaseImage, 
            bumpMap=BeginLayer_heightMap, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 1, 26, 129, 255 ), 
                ( 0.210268, 4, 43, 192, 255 ), 
                ( 0.274013, 255, 240, 192, 255 ), 
                ( 0.324013, 128, 240, 128, 128 ), 
                ( 0.374013, 128, 240, 128, 0 ), 
                ( 1.0, 255, 255, 255, 0 )], 
            heightMap=heightMapSea, 
            lightBrightness=2.0, 
            lightContrast=1.4, 
            randomGradient=False)

        RendererPole=RD.create (  name='RendererPole',  
            backgroundImage=BaseImage, 
            bumpMap=BeginLayer_heightMap, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 0 ), 
                ( -0.01402, 230, 240, 250, 0 ), 
                ( -0.0097995, 248, 254, 255, 255 ), 
                ( 1.0, 255, 255, 255, 255 )], 
            heightMap=heightMapPole, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
            destImage=img_bump, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 255 ), 
                ( -0.4, 0, 0, 0, 255 ), 
                ( 0.0, 255, 255, 255, 255 ), 
                ( 0.6, 0, 0, 0, 255 ), 
                ( 1.0, 0, 0, 0, 255 )], 
            heightMap=bump_heightMap, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        bumpmap_renderer_mask=RD.create (  name='bumpmap_renderer_mask',  
            backgroundImage=img_bump, 
            destImage=img_bump, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 255 ), 
                ( 0.274013, 0, 0, 0, 255 ), 
                ( 0.5240130000000001, 0, 0, 0, 0 ), 
                ( 1.0, 0, 0, 0, 0 )], 
            heightMap=heightMapSea, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        specmap_renderer=RD.create (  name='specmap_renderer',  
            destImage=img_spec, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 255, 255, 255, 255 ), 
                ( 1.0, 255, 255, 255, 255 )], 
            heightMap=spec_heightMap, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        specmap_renderer_spec=RD.create (  name='specmap_renderer_spec',  
            backgroundImage=img_spec, 
            destImage=img_spec, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 255, 255, 255, 255 ), 
                ( 0.274013, 255, 255, 255, 255 ), 
                ( 0.324013, 0, 0, 0, 255 ), 
                ( 1.0, 0, 0, 0, 255 )], 
            heightMap=heightMapSea, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)
        BeginLayer_renderer.setRandomFactor(20,40,60)

        MiddleLayer_renderer.setRandomFactor(20,40,60)

        RendererSea.setRandomFactor(3,15,20)

    
        self.texture.appendModuleDescriptor([AATurboPoles,AATurboSea,BeginLayer_Module,CylinderPole,MiddleLayer_Module,RidgedBumpMapModule,SeaModule])        # add modules to builder
        self.texture.appendImageDescriptor([BaseImage,img_bump,img_spec])           # add images to builder
        self.texture.appendHeightMapDescriptor([BeginLayer_heightMap,MiddleLayer_heightMap,heightMapPole,heightMapSea,bump_heightMap,spec_heightMap])   # add heightmaps to builder
        self.texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,heightMapBuilderPole,heightMapBuilderSea,noiseMapBuilderBump,noiseMapBuilderSpec]) # add heightmapbuilder
        self.texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,RendererSea,RendererPole,bumpmap_renderer,bumpmap_renderer_mask,specmap_renderer,specmap_renderer_spec])     # finally, lets add the renderer
        self.texture.outputFileName = self.textureName

    def build_texture(self):
        self.texture.buildTextureFromJsonString(self.texture.jsonString(), ".")

if __name__ == "__main__":
    txt = PostGarden02('PostGarden02')
    txt.build_texture()
