import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB
import utils.gradients
import random

def df(d):
    return float(d)*random.random()

def random_curve():
    w = []
    w.append((-1.0,-1.0))
    h = -1.0
    while h < 1.0:
        h = h + df(0.4)
        l = h * (0.5+df(0.4))
        if  h < 1.0:
            w.append ((h, l))
    w.append((1.0,1.0))
    return w

# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = MD.Perlin( 
    name="Perlin", 
    seed=0, 
    freq=1.0+df(0.4),
    lac=1.5+df(0.6),
    pers=0.45+df(0.25),
    oct=8
)

# let's build another Perlin Descriptor for sea:
# take care to use a different name from other modules!
perlin_sea = MD.RidgedMulti2( name="PerlinSea", 
    seed=0, 
    freq=1.15,
    lac=3.75, 
    oct=8,
    gain=2.0,
    offset=-0.55, 
    exp=0.5  
)
# perlin_sin = texture.Exp(name="PerlinSin", src1=perlin, exp=3.0)

rot_sea = MD.RotatePoint(name="RotateSea",src1 = perlin_sea,   x = float(random.randint(0,360)), y = float(random.randint(0,360)), z=float(random.randint(0,360)))

# Let's also build a Ridged Multifractal descriptor. Its jagged appearance will be useful 
# to create mountain ranges. 

rmf_mountains = MD.RidgedMulti( name="Ridged", 
    seed=0, 
    freq=3.5+df(0.7),
    lac=1.5+df(1.0), 
    oct=8)

# rmf_mountains = MD.Billow( 
#     name="Ridged", 
#     seed=0, 
#     freq=2.2,
#     lac=3.8,
#     #pers=0.36,
#     oct=8)

rmf_exp = MD.Curve(name="RidgedCurve",src1=rmf_mountains,cpoints= random_curve() )

# ...since RidgedMultiFractal sometimes produces ranges that follow unnaturally neat curves, 
# let's perturbate it with a Turbulence module. src1 parameter is the module to be perturbated.
perturbed_rmf = MD.Turbulence(name="RidgedTurbo", src1=rmf_exp, seed=0, freq=1.0, pow=0.125, rough=5.1)

rotated_rmf = MD.RotatePoint(name="RTRotate", src1=perturbed_rmf, 
    x = -90.0+float(random.randint(0,180)),
    y = -90.0+float(random.randint(0,180)),
    z = -90.0+float(random.randint(0,180)))

pole = MD.Cylinders("Pole", 0.5)
perturbed_pole = MD.Turbulence(name="PoleTurbo", src1=pole, seed=0, freq=4.0, pow=0.25, rough=5.1)


# then the image descriptor, which will hold rendered image data
# we STILL need one image. 
image = ID.create("Image")

# we also need two images, one for the bump, one for the specular map
imageBump = ID.create("Bump")
imageSpec = ID.create("Spec")


# then the heightmap, which will hold heightmap data
heightmap = HMD.create("Heightmap")

# we need an heightmap for sea, too. 
heightmapSea = HMD.create("HeightmapSea")

#and one for the mountains
heightmapRange = HMD.create("HeightmapRange")

#and one for the mountains
heightmapRivers = HMD.create("HeightmapRivers")

#and one for the poles
heightmapPoles = HMD.create("HeightmapPoles ")

# connect the noise module with heightmap
heightmapBuilder = NMB.create("hmbuilder", module=perlin, heightMap=heightmap)

# we also need a hm builder for sea. 
heightmapBuilderSea = NMB.create("hmbuilderSea", module=rot_sea, heightMap=heightmapSea)

# ... and one for mountain ranges. Notice we used the PERTURBED ridged multi module.
heightmapBuilderRanges = NMB.create("hmbuilderRanges", module=perturbed_rmf, heightMap=heightmapRange)

# ... and one for mountain ranges. Notice we used the PERTURBED ridged multi module.
heightmapBuilderRivers = NMB.create("hmbuilderRivers", module=rotated_rmf, heightMap=heightmapRivers)


# ... and one for poles. Make sure to bind the perturbed_pole module!
heightmapBuilderPoles = NMB.create("hmbuilderPoles", module=perturbed_pole, heightMap=heightmapPoles)




# this is a simple gradient we'll use to render land areas.
# we remove ice, since we'll use rmf_mountains ridged multifractal
# noise as a basis for our mountain ranges. 
land_gradient = utils.gradients.evolve_planet_gradient(2+random.randint(1,6))

# this is a sea gradient. It's sea blue until 55%, then there's a little sand strip
# that becomes transparent. We'll use it to generate a blue map with a cutout area that makes for 
# the continents' profiles. 

dd = float(random.randint(0,1000)-500)/2000.0

ec = utils.gradients.random_beach_color()

sea_gradient = [
    (     -1.0  , 20, 20, 80,255),    
    ( dd - 0.11 , 30, 30,128,255),
    ( dd + 0.15 , 40, 40,144,255),
    ( dd + 0.151,192,192,255,255),
    ( dd + 0.152,ec[0],ec[1],ec[2],255),
    ( dd + 0.360,ec[0],ec[1],ec[2],  0),
    (      1.0  ,ec[0],ec[1],ec[2],  0),
]

sea_gradient_2 = [
    (     -1.0  , 20, 20, 80,255),    
    ( dd + 0.31 , 30, 30,128,255),
    ( dd + 0.55  , 40, 40,144,255),
    ( dd + 0.551,192,192,255,255),
    ( dd + 0.552,ec[0],ec[1],ec[2],0),
    ( dd + 0.660,ec[0],ec[1],ec[2],  0),
    (      1.0  ,ec[0],ec[1],ec[2],  0),
]



# this is the gradient we're using for ice. Only higest peak will be white
ice_gradient = [
    (-1.0  ,255,255,255,   0),
    ( 0.4  ,255,255,255,   0),
    ( 0.42 ,255,255,255,  0),
    ( 0.78 ,255,255,255,  0),
    ( 0.85 ,255,255,255,  48),
    ( 0.95 ,255,255,255, 128),
    ( 1.00 ,255,255,255, 224),
]

# this is the gradient we're using for rivers.
river_gradient = [
    (-1.0  ,255,255,255,   0),
    (-0.021 ,255,255,255,   0),
    (-0.02 , 50, 50,144, 240),
    ( 0.00 , 50, 50,144, 255),
    ( 0.02 , 50, 50,144, 255),
    ( 0.021 ,255,255,255,  0),
    ( 1.00 , 50, 50,144,   0)
]

pole_gradient =   [
    (-1.0  ,255,255,255,   0),
    ( 0.58 ,255,255,255,   0),
    ( 0.59 ,255,255,255, 255),
    ( 1.00 ,255,255,255, 225),
]

# this is the gradient we're using as a basis for our bump map
bump_gradient = [
    (-1.0  ,  0,  0,  0, 255),
    #( 0.252,  0,  0,  0, 255),
    ( 1.00 ,255,255,255, 225)
]

blacksea_gradient = [
    (-1.0  ,   0,  0,  0,255),    
    ( dd+0.551,   0,  0,  0,255),
    ( dd+0.591,   0,  0,  0,  0),
    ( 1.0  ,255,255,192,  0),
]

spec_gradient = [
    ( -1.0  , 255,255,255,255),    
    (  dd+0.551, 255,255,255,255),
    (  dd+0.552,   0,  0,  0,255),
    (    1.0  ,   0,  0,  0,255),
]

# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = RD.create(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo = land_gradient,
    bumpMap = heightmapRange,   # we're using another heightmap as a bumpmap
    destImage=image,
    lightContrast = 1.2,
    enabledLight= True)


# after the land, snow
renderer_mnt  = RD.create(name="renderer_mnt", 
    heightMap=heightmapRange,       # name of the heightmap you want to render
    gradientInfo = ice_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 1.2,
    enabledLight= True)


renderer_rvr  = RD.create(name="renderer_rvr", 
    heightMap=heightmapRivers,       # name of the heightmap you want to render
    gradientInfo = river_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 1.2,
    enabledLight= True)


# then sea covers all: we'll use the sea gradient to create a "sea overlay" on the map. 
# this will make our world map slightly more natural, since land and sea have their own generator.
renderer_sea  = RD.create(name="renderer_sea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = sea_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)

renderer_sea_2  = RD.create(name="renderer_sea_2", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = sea_gradient_2,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)    

renderer_pole  = RD.create(name="renderer_pole", 
    heightMap=heightmapPoles,       # name of the heightmap you want to render
    gradientInfo = pole_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)

# now let's render the bumpmap
renderer_bump_1  = RD.create(name="renderer_bump", 
    heightMap=heightmapRange,       # name of the heightmap you want to render
    gradientInfo = bump_gradient,
    destImage=imageBump,
    lightContrast = 1.4,
    enabledLight= False)

renderer_bump_2  = RD.create(name="renderer_bump_sea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = blacksea_gradient,
    destImage=imageBump,
    backgroundImage= imageBump,
    lightContrast = 2.0,
    enabledLight= False)


renderer_spec  = RD.create(name="renderer_specsea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = spec_gradient,
    destImage=imageSpec,
    lightContrast = 2.0,
    enabledLight= False)


# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()
texture.setSize(2000,1000)
# texture.setBounds(0,10,35,45)


texture.appendModuleDescriptor([perlin,  perlin_sea, rmf_mountains, rmf_exp, perturbed_rmf, rotated_rmf, pole, perturbed_pole, rot_sea ])
texture.appendImageDescriptor([image, imageBump, imageSpec])           # add images to builder
texture.appendHeightMapDescriptor([heightmap, heightmapSea, heightmapRange, heightmapPoles,heightmapRivers])
texture.appendNoiseMapBuilderDescriptor([heightmapBuilder,heightmapBuilderSea, heightmapBuilderRanges, heightmapBuilderRivers, heightmapBuilderPoles]) # add heightmapbuilder

# very important, renderers perform their job according to their insertion order. 
texture.appendRendererDescriptor([
    renderer, renderer_sea, renderer_rvr,
    renderer_mnt, renderer_sea_2,  renderer_pole,
    renderer_bump_1, renderer_bump_2, renderer_spec
    ])     # finally, lets add the renderer

jsonTexture = texture.jsonString()
with open ("Planet_18.texjson","w") as f:
    f.write(jsonTexture)

texture.outputFileName = "Planet_18"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

