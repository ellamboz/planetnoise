from distutils.core import setup, Extension
import sys
import os

if sys.platform == "win32":
    # To build for Windows:
    # 1. Install MingW-W64-builds from https://mingw-w64.org/doku.php/download
    #    It is important to change the default to 64-bit when installing if a
    #    64-bit Python is installed in windows.
    # 2. Put the bin/ folder inside x86_64-8.1.0-posix-seh-rt_v6-rev0 in your
    #    system PATH when compiling.
    # 3. The code below will moneky-patch distutils to work.
    import distutils.cygwinccompiler
    distutils.cygwinccompiler.get_msvcr = lambda: []
    # Escaping works differently.
    CONFIG_VERSION = '\\"2019-07-09\\"'
    # Make sure that pthreads is linked statically, otherwise we run into problems
    # on computers where it is not installed.
    extra_link_args = ["-Wl,-Bstatic", "-lpthread"]

_sources = """src/planetnoise/libnoise/src/noise/model/cylinder.cpp
src/planetnoise/libnoise/src/noise/model/line.cpp
src/planetnoise/libnoise/src/noise/model/plane.cpp
src/planetnoise/libnoise/src/noise/model/sphere.cpp
src/planetnoise/libnoise/src/noise/module/abs.cpp
src/planetnoise/libnoise/src/noise/module/add.cpp
src/planetnoise/libnoise/src/noise/module/avg.cpp
src/planetnoise/libnoise/src/noise/module/avg4.cpp
src/planetnoise/libnoise/src/noise/module/billow.cpp
src/planetnoise/libnoise/src/noise/module/blend.cpp
src/planetnoise/libnoise/src/noise/module/cache.cpp
src/planetnoise/libnoise/src/noise/module/checkerboard.cpp
src/planetnoise/libnoise/src/noise/module/clamp.cpp
src/planetnoise/libnoise/src/noise/module/const.cpp
src/planetnoise/libnoise/src/noise/module/cos.cpp
src/planetnoise/libnoise/src/noise/module/curve.cpp
src/planetnoise/libnoise/src/noise/module/cylinders.cpp
src/planetnoise/libnoise/src/noise/module/displace.cpp
src/planetnoise/libnoise/src/noise/module/exponent.cpp
src/planetnoise/libnoise/src/noise/module/invert.cpp
src/planetnoise/libnoise/src/noise/module/max.cpp
src/planetnoise/libnoise/src/noise/module/min.cpp
src/planetnoise/libnoise/src/noise/module/modulebase.cpp
src/planetnoise/libnoise/src/noise/module/multiply.cpp
src/planetnoise/libnoise/src/noise/module/perlin.cpp
src/planetnoise/libnoise/src/noise/module/power.cpp
src/planetnoise/libnoise/src/noise/module/ridgedmulti.cpp
src/planetnoise/libnoise/src/noise/module/ridgedmulti2.cpp
src/planetnoise/libnoise/src/noise/module/rotatepoint.cpp
src/planetnoise/libnoise/src/noise/module/scalebias.cpp
src/planetnoise/libnoise/src/noise/module/scalepoint.cpp
src/planetnoise/libnoise/src/noise/module/select.cpp
src/planetnoise/libnoise/src/noise/module/spheres.cpp
src/planetnoise/libnoise/src/noise/module/terrace.cpp
src/planetnoise/libnoise/src/noise/module/translatepoint.cpp
src/planetnoise/libnoise/src/noise/module/turbulence.cpp
src/planetnoise/libnoise/src/noise/module/turbulence2.cpp
src/planetnoise/libnoise/src/noise/module/turbulence_billow.cpp
src/planetnoise/libnoise/src/noise/module/turbulence_ridged.cpp
src/planetnoise/libnoise/src/noise/module/voronoi.cpp
src/planetnoise/libnoise/src/noise/latlon.cpp
src/planetnoise/libnoise/src/noise/noisegen.cpp
src/planetnoise/libnoise/src/noiseutils.cpp
src/planetnoise/libnoise/json.cpp
src/planetnoise/libnoise-helpers/texturebuilder/heightmapdescriptor.cpp
src/planetnoise/libnoise-helpers/texturebuilder/imagedescriptor.cpp
src/planetnoise/libnoise-helpers/texturebuilder/moduledescriptor.cpp
src/planetnoise/libnoise-helpers/texturebuilder/noisemapbuilderdescriptor.cpp
src/planetnoise/libnoise-helpers/texturebuilder/rendererdescriptor.cpp
src/planetnoise/libnoise-helpers/texturebuilder/texturebuilder.cpp
src/planetnoise/libnoise-helpers/texturebuilder/textureworkflowcreator.cpp
src/planetnoise/stb/stb_vorbis.c
src/py_planetnoise.cpp""".split("\n")


class get_pybind_include(object):
    def __str__(self):
        try:
            from pip import locations

            pybind_include = os.path.dirname(locations.distutils_scheme('pybind11')['headers'])
            return pybind_include
        except:
            return "."


_include_dirs=[
    "src/planetnoise/libnoise/src",
    "src/planetnoise/json",
    "src/planetnoise/libnoise-helpers",
    "src/planetnoise/stb",
    "src/pybind/include",
    "src/pybind/include/pybind11",
    get_pybind_include()
]



module1 = Extension('planetnoise',
                    define_macros = [('MAJOR_VERSION', '0'),
                                     ('MINOR_VERSION', '1'),
                                     ('MS_WIN64', None)],
                    include_dirs = _include_dirs,
                    language="c++",
                    sources = _sources)

setup (name = 'planetnoise',
       version = '0.1',
       description = 'Wrapper around C++ planetnoise',
       author = 'Massimiliano Lambertini',
       author_email = 'daboss@lamboz.net',
       url = 'https://planetnoise.lamboz.net',
       long_description = '''
A wrapper around libnoise and planetnoise
''',
       ext_modules = [module1])
